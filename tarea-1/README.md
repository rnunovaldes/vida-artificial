# Autómata celular de Una Dimensión

## Descripción
Un autómata celular de una dimensión consiste en una fila
unidimensional de células, cada una de las cuales puede estar en uno de
dos estados posibles: viva (1) o nuerta (0). Las células interactúan
con sus vecinas de acuerdo con un conjunto de reglas predefinidas en
cada paso de tiempo discreto. Cada célula sigue las mismas reglas y se
actualiza simultáneamente con todas las demás células en función de sus
estados y los estados de sus vecinos.

Esta es una implementación del "Autómata Celular Elemental" de Stephen
Wolfram, que utiliza una notación binaria de 8 bits, donde cada bit
corresponde a una configuración específica de estados de vecinos y
determina el estado futuro de la célula central.

## Programa
<div style="display: flex; align-items: center;">
  <a href="https://dev.java">
    <img src="https://dev.java/assets/images/java-affinity-logo-icode-lg.png" alt="Java Affinity Logo" width="70px" />
  </a>
  <p style="margin-left: 10px;">Este trabajo se programó con Java 17.0.7</p>
</div>
Para la ejecución, use alguno de los scripts proporcionados

###### Notas
Debería funcionar con Java 8

### Ejemplo de salida
![Regla 30 con población inicial de uno en el centro](./out/Regla_030.png)
