package main;

import java.io.File;

import mvc.controller.Constants;
import mvc.controller.WindowManager;

/**
 * Clase que inicia la ejecucion del proyecto
 */
public class Principal {

    /**
     * Método principal
     */
    public static void main(String[] args) {
        File outputFolder = new File(Constants.OUT);
        if (!outputFolder.exists()) {
            outputFolder.mkdirs();
        }
        WindowManager wm = new WindowManager();
    }
}
