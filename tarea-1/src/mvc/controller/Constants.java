package mvc.controller;

import java.awt.Dimension;
import java.awt.Toolkit;

/**
 * Contiene valores constantes que son relevantes para varias
 * partes del programa
 */
public final class Constants {
    public static final short MAX_GENERATIONS = 180;
    private static final short BIN = 64;
    public static final short MAX_CELLS = BIN * 6;
    public static final short MID_CELL = BIN * 3 - 1;

    public static final byte CELL_SIZE = 3;
    public static final byte RULE_CELL_SIZE = 14;

    public static final String SEP = System.getProperty("file.separator");
    public static final String RES = "." + Constants.SEP + "res";
    public static final String OUT = "." + Constants.SEP + "out";

    private static final Dimension SCREEN_SIZE = Toolkit.getDefaultToolkit().getScreenSize();
    public static final int SCREEN_WIDTH = (int) SCREEN_SIZE.getWidth();
    public static final int SCREEN_HEIGHT = (int) SCREEN_SIZE.getHeight();
}
