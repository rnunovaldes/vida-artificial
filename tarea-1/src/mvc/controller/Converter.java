package mvc.controller;

import java.util.BitSet;

/**
 * Clase con métodos para convertir números binarios de longitud 8 y 3
 * sus decimales correspondientes
 */
public final class Converter {

    /**
     * Convierte arreglo de booleanos a un número decimal
     *
     * @param binary Un arreglo de booleanos de tamaño 8 donde el primer
     *               elemento es el bit más significativo
     * @return El número en decimal
     */
    public static short binaryToDecimal(BitSet binary) {
        short decimal = (short) (binary.get(0)? 128: 0);
        decimal += binary.get(1)? 64: 0;
        decimal += binary.get(2)? 32: 0;
        decimal += binary.get(3)? 16: 0;
        decimal += binary.get(4)? 8: 0;
        decimal += binary.get(5)? 4: 0;
        decimal += binary.get(6)? 2: 0;
        decimal += binary.get(7)? 1: 0;
        return decimal;
    }

    /**
     * Convierte un número booleano de tamaño 3 a decimal
     *
     * @param fbin El bit más significavo
     * @param sbin El bit de en medio
     * @param tbin El bit menos significativo
     * @return El número en decimal
     */
    public static byte binaryToDecimal(boolean fbin, boolean sbin, boolean tbin) {
        byte decimal = (byte) (fbin? 4: 0);
        decimal += sbin? 2: 0;
        decimal += tbin? 1: 0;
        return decimal;
    }
}
