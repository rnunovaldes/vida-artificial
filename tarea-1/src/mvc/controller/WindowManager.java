package mvc.controller;

import mvc.view.Window;
import mvc.model.Model;
import java.util.BitSet;

/**
 * Clase que coordina las ventanas y la lógica del proyecto
 */
public class WindowManager {
    private Window window;
    private Model model;
    private boolean isSimulationRunning;

    /**
     * Constructor de la clase
     */
    public WindowManager() {
        window = new Window(this);
        model = new Model(this);
    }

    /**
     * Método que envía los valores de una regla para ser guardada
     * en un archivo
     *
     * @param rule Los valores de la regla a guardar
     * @return Si se guardó correctamente
     */
    public boolean sendForSerialization(BitSet rule) {
        return model.serializateRule(rule);
    }

    /**
     * Método que envía la ruta del archivo con la regla al modelo y
     * regresa los valores a mostrar en la IU
     *
     * @param rulPath La ruta del archivo con la regla
     * @return Los valores a mostrar en la IU para la regla
     *         correspondiente
     */
    public BitSet sendForDeserialization(String rulPath) {
        return model.deserializateRule(rulPath);
    }

    /**
     * Método que envía los valores de la primera generación a usar
     * en la simulación al modelo
     *
     * @param fgen Los valores de la primera generación
     */
    public void sendFirstGen(BitSet fgen) {
        model.setFirstGen(fgen);
    }

    /**
     * Método que envía los valores de la regla a usar en la simulación
     * al modelo e indica que se dé comienzo a esta. Usa hilos
     *
     * @param rule Los valores de la regla
     */
    public void sendRule(BitSet rule) {
        window.disableMenuItems();
        if(!isSimulationRunning) {
            isSimulationRunning = true;
            Thread simulationThread = new Thread(() -> {
                model.doSimulation(rule);
                isSimulationRunning = false;
                window.enableMenuItems();
            });
            simulationThread.start();
        }
    }

    /**
     * Envía los valores de una generación resultante de la simulación
     * a la IU
     *
     * @param nextGen La generación nueva que debe ser mostrada
     */
    public void sendNextGen(BitSet nextGen) {
        window.paintNextGen(nextGen);
    }

    public void saveAsImage(String path) {
        model.saveAsImage(path);
    }
}
