package mvc.model;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.BitSet;

import javax.imageio.ImageIO;

import mvc.controller.Constants;
import mvc.controller.WindowManager;

/**
 * Clase que se encarga de pedir la serialización y deserialización de
 * reglas, además de solicitar ejecuciones de las reglas
 */
public class Model {
    private WindowManager wm;
    private BitSet[] generations = new BitSet[Constants.MAX_GENERATIONS];
    private Rule rule;

    /**
     * Constructor de la clase
     *
     * @param wm El coordinador entre las ventanas y la lógica
     */
    public Model(WindowManager wm) {
        this.wm = wm;
        for(int i = 0; i < 130; i++) {
            generations[i] = new BitSet(Constants.MAX_CELLS);
        }
    }

    /**
     * Asigna la primera generación de la simulación
     *
     * @param fgen La generación a asignar como la primera
     */
    public void setFirstGen(BitSet fgen) {
        generations[0] = fgen;
    }

    /**
     * Comienza a gererar las generaciones y enviar los resultados
     *
     * @param rule La regla a usar
     * @return Si se completó
     */
    public boolean doSimulation(BitSet rule) {
        this.rule = new Rule(rule);
        for(short i = 0; i < Constants.MAX_GENERATIONS-1; i++) {
            generations[i+1] = this.rule.nextGen(generations[i]);
            wm.sendNextGen(generations[i + 1]);
            try {
                Thread.sleep(10);
            } catch(InterruptedException e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    /**
     * Método para deserializar una Rule nueva.
     *
     * @param rule Son los valores de la regla que se han puesto en la IU
     * @return Si se pudo guardar la regla correctamente
     */
    public boolean serializateRule(BitSet rule) {
        Rule srule = new Rule(rule);
        String rulPath = Constants.OUT + Constants.SEP + srule.getName() + ".rul";
        try(FileOutputStream fileOut = new FileOutputStream(rulPath);
               ObjectOutputStream objectOut = new ObjectOutputStream(fileOut)) {

            objectOut.writeObject(srule);
            return true;
        } catch(IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Método para deserializar Rule. Luego envía lo necesario para
     * mostrar qué se hizo en la GUI
     *
     * @param rulPath La ruta del archivo
     * @return Los valores a mostrar en la IU para la regla
     *         correspondiente
     */
    public BitSet deserializateRule(String rulPath) {
        try(FileInputStream fileIn = new FileInputStream(rulPath);
               ObjectInputStream objectIn = new ObjectInputStream(fileIn)) {

            Rule srule = (Rule) objectIn.readObject();
            return srule.getRule();
        } catch(IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return new BitSet(8);
    }

    /**
     * Método para crear un archivo en formato png en una ruta dada por un
     * objeto File
     *
     * @param fimg El File con la ruta y nombre de la nueva imagen
     * @param image El BufferedImage de la imagen a guardar
     */
    public void saveAsImage(String fimg) {
        try {
            File file = new File(fimg);
            BufferedImage image = generateBufferedImage(generations);
            ImageIO.write(image, "png", file);
        } catch(IOException e) {
            String workingDir = System.getProperty("user.dir");
            System.out.println("Current working directory : " + workingDir);
            e.printStackTrace();
        }
    }

    /**
     * Método para generar un BufferedImage a partir de una matriz de Color
     *
     * @param pixeles Un arreglo de BitSets donde los trues serán pintados
     *                de negro y false de blanco en  el BufferedImage
     * @return La imagen en formato de BufferedImage
     */
    private static BufferedImage generateBufferedImage(BitSet[] pixeles) {
        BufferedImage nImage = new BufferedImage(Constants.MAX_CELLS * Constants.CELL_SIZE,
            Constants.MAX_GENERATIONS * Constants.CELL_SIZE, BufferedImage.TYPE_INT_RGB);
        // Set each pixel of the BufferedImage to the color from the Color[][].
        for(short i = 0; i < Constants.MAX_GENERATIONS; i++) {
            for(short j = 0; j < Constants.MAX_CELLS; j++) {
                int fixedi = i * Constants.CELL_SIZE;
                int fixedj = j * Constants.CELL_SIZE;
                int color = pixeles[i].get(j)? 0: 0xFFFFFF;
                for (byte y = 0; y < Constants.CELL_SIZE; y++) {
                    for (byte x = 0; x < Constants.CELL_SIZE; x++) {
                        nImage.setRGB(fixedj + x, fixedi + y, color);
                    }
                }
            }
        }
        return nImage;
    }
}
