package mvc.model;

import java.io.Serializable;
import java.util.BitSet;

import mvc.controller.Converter;

/**
 * Clase que modela las reglas a seguir para un autómata celular de una
 * dimensión
 */
public class Rule implements Serializable {
    private BitSet rule;
    private String name = "Regla_%03d";

    /**
     * Constructor de <code>Rule</code>. Cada índice del arreglo
     * representa un número binario. El índice 0 = 111, 1 = 110, ... ,
     * 6 = 001, 7 = 000.
     *
     * @param rule Un arreglo de booleanos que indican los valores
     */
    public Rule(BitSet rule) {
        this.rule = rule;
        name = String.format(name, Converter.binaryToDecimal(rule));
    }

    /**
     * Genera la siguiente generación según la regla y la generación
     * actual indicada
     *
     * @param currentGen La generación actual
     * @return Una nueva generación obtenida al aplicar la regla en la
     *         generación actual
     */
    public BitSet nextGen(BitSet currentGen) {
        int size = currentGen.size();
        BitSet nextGen = new BitSet(size);

        for(short i = 0; i < size; i++) {
            boolean center = currentGen.get(i);
            boolean left = (i-1 == -1)?
                    currentGen.get(currentGen.size()-1): currentGen.get(i-1);
            boolean right = (i+1 == size)?
                    currentGen.get(0): currentGen.get(i+1);
            byte index = Converter.binaryToDecimal(left, center, right);
            if(rule.get(7-index)) nextGen.set(i);
        }

        return nextGen;
    }

    /**
     * Obtiene los valores de la regla (8 valores)
     *
     * @return Un arreglo con los valores
     */
    public BitSet getRule() {
        return rule;
    }

    /**
     * Obtiene el nombre de la regla
     *
     * @return Una cadena con formato "Regla_#decimal"
     */
    public String getName() {
        return name;
    }
}
