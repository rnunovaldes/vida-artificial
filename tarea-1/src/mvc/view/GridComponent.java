package mvc.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.BitSet;

import javax.swing.JComponent;

import mvc.controller.Constants;

/**
 * Clase que modela una rejilla de muchos cuadros que pueden cambiar de
 * color de acuerdo a la simulación. Tamaño dependiente de variables en
 * <code>Constats</code>
 */
public class GridComponent extends JComponent {
    private BitSet[] grid = new BitSet[Constants.MAX_GENERATIONS];

    public GridComponent() {
        for(int i = 0; i < Constants.MAX_GENERATIONS; i++) {
            grid[i] = new BitSet(Constants.MAX_CELLS);
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        for(int row = 0; row < Constants.MAX_GENERATIONS; row++) {
            for(int col = 0; col < Constants.MAX_CELLS; col++) {
                if(grid[row].get(col)) {
                    g.setColor(Color.BLACK);
                } else {
                    g.setColor(Color.WHITE);
                }
                g.fillRect(col * Constants.CELL_SIZE, row * Constants.CELL_SIZE,
                        Constants.CELL_SIZE, Constants.CELL_SIZE);
            }
        }

        int width = getWidth();
        int height = getHeight();
        g.setColor(Color.LIGHT_GRAY); // Color de la línea de la rejilla
        for(int x = 0; x <= width; x += Constants.CELL_SIZE) {
            g.drawLine(x, 0, x, height);
        }
        for(int y = 0; y <= height; y += Constants.CELL_SIZE) {
            g.drawLine(0, y, width, y);
        }
    }

    /**
     * Hace de color negro una celda en la coordenada especificada
     *
     * @param x La coordenada x de izquierda a derecha
     * @param y La coordenada y de arriba a abajo
     */
    public void setCellBlack(short x, short y) {
        if(isValidCell(x, y)) {
            grid[y].set(x);
            int repaintX = x * Constants.CELL_SIZE;
            int repaintY = y * Constants.CELL_SIZE;
            repaint(repaintX, repaintY,
                    Constants.CELL_SIZE, Constants.CELL_SIZE);
        }
    }

    /**
     * Método que hace blancas todas las celdas de la primera fila
     * de la rejilla
     */
    public void resetFirstGen() {
        grid[0] = new BitSet(Constants.MAX_CELLS);
        repaint(0, Constants.CELL_SIZE,
                Constants.MAX_CELLS * Constants.CELL_SIZE, Constants.CELL_SIZE);
    }

    /**
     * Método que hace blancas todas las celdas que no sean la primera
     * fila de la rejilla
     */
    public void restart() {
        for(int i = 1; i < Constants.MAX_GENERATIONS; i++) {
            grid[i] = new BitSet(Constants.MAX_CELLS);
        }
        repaint();
    }

    /**
     * Determina si la celda es válida para ser cambiada de color
     *
     * @param x La posición en el eje de las x de izquierda a derecha
     * @param y La posición en el eje de las y de arriba a abajo
     * @return Si la celda está contenida en la rejilla
     */
    private boolean isValidCell(int x, int y) {
        return x >= 0 && x < Constants.MAX_CELLS
                && y >= 0 && y < Constants.MAX_GENERATIONS;
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(Constants.MAX_CELLS * Constants.CELL_SIZE + 1,
                Constants.MAX_GENERATIONS * Constants.CELL_SIZE + 1);
    }
}
