package mvc.view;

/**
 * Para implementar un listener de los cuadrados clicables
 */
public interface SquarePanelClickListener {
    void onSquarePanelClicked(SquarePanel squarePanel);
}
