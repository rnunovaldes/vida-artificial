package mvc.view;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JPanel;

/**
 * Clase para usar en la IU. Son 3 cuadrados alineados en una fila y un
 * cuarto abajo del segundo cuadro al que se puede clicar y cambiar
 * entre color blanco o negro
 */
public class DisplayRule extends JPanel implements SquarePanelClickListener {

    private SquarePanel rule;
    private DisplayRuleClickListener clickListener;

    /**
     * Contructor de la clase
     *
     * @param fblack Si el cuadro superior izquierdo es negro o no
     * @param sblack Si el cuadro superior central es negro o no
     * @param tblack Si el cuadro superior derecho es negro o no
     */
    public DisplayRule(boolean fblack, boolean sblack, boolean tblack,
                       DisplayRuleClickListener clickListener) {
        boolean[] blacks = {fblack, sblack, tblack};
        this.clickListener = clickListener;
        GridBagLayout gbl = new GridBagLayout();
        setLayout(gbl);
        GridBagConstraints constraints = new GridBagConstraints();
        for(int i = 0; i < 3; i++) {
            constraints.gridx = i;
            constraints.gridy = 0;
            SquarePanel square = new SquarePanel(
                    blacks[i], false, null
                );
            gbl.setConstraints(square, constraints);
            add(square);
        }

        constraints.gridx = 1;
        constraints.gridy = 1;
        rule = new SquarePanel(false, true, this);
        gbl.setConstraints(rule, constraints);
        add(rule);
    }

    @Override
    public void onSquarePanelClicked(SquarePanel squarePanel) {
        clickListener.onDisplayRuleClicked(DisplayRule.this);
    }

    /**
     * Obtiene el color asignado al cuadro inferior
     *
     * @return Si el cuadro es de color negro o no
     */
    public boolean getColor() {
        return rule.getBlack();
    }

    /**
     * Asigna un color al cuadro inferior
     *
     * @param black Si el cuadro debe ser negro o no
     */
    public void setColor(boolean black) {
        rule.setBlack(black);
        rule.repaint();
    }
}
