package mvc.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;
import java.util.Random;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;

import mvc.controller.Constants;
import mvc.controller.Converter;
import mvc.controller.WindowManager;
import utils.fileManager.RuleFilter;

/**
 * Clase para mostrar una ventana de la interfaz grafica
 */
public class Window extends JFrame implements ActionListener,
        DisplayRuleClickListener {

    private final String TITLE = "Autómata Celular de 1Dimensión";
    private final String[] OPTIONS = {
            /* 0 */"Archivo",
            /* 1 */"Reglas",
            /* 2 */"Guardar regla actual",
            /* 3 */"Cargar regla",
            /* 4 */"Guardar imagen",
            /* 5 */"Población",
            /* 6 */"Uno",
            /* 7 */"Al azar",
            /* 8 */"Simulación",
            /* 9 */"Iniciar",
            /* 10 */"Reiniciar",
        };
    private final String[] LABELS = {
            /* 0 */"Regla #%d",
            /* 1 */"Estado",
            /* 2 */"Simulación"
        };

    private volatile short gen = 0;
    private JMenu menu1, smenu1, menu2, menu3;
    private JMenuItem sm1_i1, sm1_i2, sm1_i3, sm2_i1, sm2_i2, sm3_i1, sm3_i2;
    private JFileChooser fcLoad, fcSave;
    private JPanel simZone, sharedZone, rulesZone, statusZone;
    private JLabel checkIconLabel, loadingIconLabel;
    private GridComponent simGrid;
    private List<DisplayRule> drule = new ArrayList<>(8);
    private BitSet vrule = new BitSet(8);

    private final File OUTFILE = new File(Constants.OUT);

    protected WindowManager wm;

    /**
     * Constructor de la clase donde se inicializan todos los componentes
     * de la ventana principal
     *
     * @param wm El coordinador entre las ventanas y la logica
     */
    public Window(WindowManager wm) {
        this.wm = wm;
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        // setSize(480, 480);
        setTitle(TITLE);
        // Centrar horizontalmente
        int centerX = (50);
        setLocation(centerX, 0);
        // setUndecorated(false);
        setResizable(true);
        setLayout(new BorderLayout());

        /* Pestaña de Archivo */
        JMenuBar mb = new JMenuBar();
        menu1 = new JMenu(OPTIONS[0]);
        smenu1 = new JMenu(OPTIONS[1]);
        sm1_i1 = new JMenuItem(OPTIONS[2]);
        sm1_i2 = new JMenuItem(OPTIONS[3]);
        sm1_i3 = new JMenuItem(OPTIONS[4]);
        smenu1.add(sm1_i1);
        smenu1.add(sm1_i2);
        menu1.add(smenu1);
        menu1.add(sm1_i3);

        /* Pestaña de Población */
        menu2 = new JMenu(OPTIONS[5]);
        sm2_i1 = new JMenuItem(OPTIONS[6]);
        sm2_i2 = new JMenuItem(OPTIONS[7]);
        menu2.add(sm2_i1);
        menu2.add(sm2_i2);

        /* Pestaña de Simulación */
        menu3 = new JMenu(OPTIONS[8]);
        sm3_i1 = new JMenuItem(OPTIONS[9]);
        sm3_i2 = new JMenuItem(OPTIONS[10]);
        menu3.add(sm3_i1);
        menu3.add(sm3_i2);

        mb.add(menu1);
        mb.add(menu2);
        mb.add(menu3);
        setJMenuBar(mb);

        /* File choose para pestaña Archivo */
        fcLoad = new JFileChooser();
        fcLoad.setCurrentDirectory(OUTFILE);
        // Add a custom file filter and disable the default
        // (Accept All) file filter.
        fcLoad.addChoosableFileFilter(new RuleFilter());
        fcLoad.setAcceptAllFileFilterUsed(false);
        // Add custom icons for file types.
        // fcEscoger.setFileView(new RuleFileView());
        /* File chooser para pestanya Archivo Guardar */
        fcSave = new JFileChooser();
        fcSave.setCurrentDirectory(OUTFILE);
        fcSave.setDialogType(JFileChooser.SAVE_DIALOG);
        fcSave.setFileFilter(new FileNameExtensionFilter("png file","png"));

        /* Reglas */
        rulesZone = new JPanel(new FlowLayout(FlowLayout.CENTER));
        rulesZone.setBorder(BorderFactory.createTitledBorder(
                String.format(LABELS[0], 0)
            ));
        boolean[] bin = {true, true, true};
        for(int i = 0; i < 8; i++) {
            drule.add(new DisplayRule(bin[2], bin[1], bin[0], this));
            rulesZone.add(drule.get(i));
            if(!bin[0] && !bin[1]) bin[2] = !bin[2];
            if(!bin[0]) bin[1] = !bin[1];
            bin[0] = !bin[0];
        }

        /* Estado */
        statusZone = new JPanel(new FlowLayout(FlowLayout.CENTER));
        statusZone.setBorder(BorderFactory.createTitledBorder(LABELS[1]));
        checkIconLabel = new JLabel(new ImageIcon(
                Constants.RES + Constants.SEP + "checkmark.png"
            ));
        checkIconLabel.setVisible(true);
        loadingIconLabel = new JLabel(new ImageIcon(
                Constants.RES + Constants.SEP + "loading.gif"
            ));
        loadingIconLabel.setVisible(false);
        statusZone.add(checkIconLabel);
        statusZone.add(loadingIconLabel);
        statusZone.setPreferredSize(new Dimension(60, 1));
        sharedZone = new JPanel(new BorderLayout());
        sharedZone.add(rulesZone, BorderLayout.CENTER);
        sharedZone.add(statusZone, BorderLayout.EAST);

        /* Simulación */
        simZone = new JPanel();
        simZone.setBorder(BorderFactory.createTitledBorder(LABELS[2]));
        simGrid = new GridComponent();
        simZone.add(simGrid);

        add(sharedZone, BorderLayout.NORTH);
        add(simZone, BorderLayout.CENTER);

        setMinimumSize(new Dimension(650, 550));
        setPreferredSize(new Dimension(
                Constants.SCREEN_WIDTH - 100, Constants.SCREEN_HEIGHT
            ));
        pack();
        setVisible(true);

        /* Listeners */
        sm1_i1.addActionListener(this);
        sm1_i2.addActionListener(this);
        sm1_i3.addActionListener(this);
        sm2_i1.addActionListener(this);
        sm2_i2.addActionListener(this);
        sm3_i1.addActionListener(this);
        sm3_i2.addActionListener(this);
    }

    /**
     * Hace que su gridComponent pinte en una fila todos los bitSet que
     * se le pasen
     *
     * @param bs Dónde es que deben pintar los cuadrados del
     *           gridComponent para la fila correspondiente (esta fila
     *           es conocida solo internamente por esta clase)
     */
    public void paintNextGen(BitSet bs) {
        for(short i = 0; i < bs.size(); i++) {
            if(bs.get(i)) simGrid.setCellBlack(i, gen);
        }
        gen++;
    }

    /**
     * Activa todas las acciones del usuario. Además indica que está
     * listo para hacer una simulación con una imagen.
     */
    public void enableMenuItems() {
        loadingIconLabel.setVisible(false);
        checkIconLabel.setVisible(true);
        sm1_i2.setEnabled(true);
        sm1_i3.setEnabled(true);
        sm2_i1.setEnabled(true);
        sm2_i2.setEnabled(true);
        sm3_i1.setEnabled(true);
        sm3_i2.setEnabled(true);
    }

    /**
     * Desactiva casi todas las acciones del usuario con excepción de
     * guardar regla actual. Además indica que debe poner un ícono de
     * carga
     */
    public void disableMenuItems() {
        checkIconLabel.setVisible(false);
        loadingIconLabel.setVisible(true);
        sm1_i2.setEnabled(false);
        sm1_i3.setEnabled(false);
        sm2_i1.setEnabled(false);
        sm2_i2.setEnabled(false);
        sm3_i1.setEnabled(false);
        sm3_i2.setEnabled(false);
    }

    /**
     * Genera una lista de npumeros aleatoios que no se repiten
     *
     * @param n cuantos número aleatorios habra en la lista
     * @return Una lista sin repeticiones de numeros que fueron
     *         escogidos con aletoriedad
     */
    private static final List<Short> generateRandomNumbers(short n) {
        List<Short> availableNumbers = new ArrayList<>();
        List<Short> randomNumbers = new ArrayList<>();
        Random random = new Random();

        for(short i = 0; i < n; i++) {
            availableNumbers.add(i);
        }
        for(short i = 0; i < random.nextInt(availableNumbers.size()) + 1; i++) {
            int randomIndex = random.nextInt(availableNumbers.size());
            short randomNumber = availableNumbers.get(randomIndex);
            randomNumbers.add(randomNumber);
            availableNumbers.remove(randomIndex);
        }
        return randomNumbers;
    }

    @Override
    public void onDisplayRuleClicked(DisplayRule displayRule) {
        int index = drule.indexOf(displayRule);
        vrule.set(index, !vrule.get(index));
        rulesZone.setBorder(BorderFactory.createTitledBorder(
                String.format(LABELS[0], Converter.binaryToDecimal(vrule))));
    }

    /**
     * Método que recibe las aciones realizadas en la ventana
     *
     * @param e El <code>ActionEvent</code> registrado
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == sm1_i1) {
            wm.sendForSerialization(vrule);
        }
        if(e.getSource() == sm1_i2) {
            int resp = fcLoad.showOpenDialog(null);

            if(resp == JFileChooser.APPROVE_OPTION) {
                String rulPath = fcLoad.getSelectedFile().getAbsolutePath();
                vrule = wm.sendForDeserialization(rulPath);
                for(int i = 0; i < 8; i++) {
                    drule.get(i).setColor(vrule.get(i));
                }
                rulesZone.setBorder(BorderFactory.createTitledBorder(
                        String.format(LABELS[0], Converter.binaryToDecimal(vrule))
                    ));
            }
        }
        if(e.getSource() == sm1_i3) {
            File defaultName = new File(String.format(
                    "Regla_%03d", Converter.binaryToDecimal(vrule)
                ));
            fcSave.setSelectedFile(defaultName);
            if(fcSave.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                String filename = fcSave.getSelectedFile().getAbsolutePath();
                if(!filename.toLowerCase().endsWith(".png")) filename += ".png";
                wm.saveAsImage(filename);
            }
		}
        if(e.getSource() == sm2_i1) {
            BitSet firstGen = new BitSet(Constants.MAX_CELLS);
            firstGen.set(Constants.MID_CELL);
            simGrid.resetFirstGen();
            simGrid.setCellBlack(Constants.MID_CELL, (short) 0);
            wm.sendFirstGen(firstGen);
        }
        if(e.getSource() == sm2_i2) {
            BitSet firstGen = new BitSet(Constants.MAX_CELLS);
            List<Short> randoms = generateRandomNumbers(Constants.MAX_CELLS);
            simGrid.resetFirstGen();
            for(Short r: randoms) {
                firstGen.set(r);
                simGrid.setCellBlack(r, (short) 0);
            }
            wm.sendFirstGen(firstGen);
        }
        if(e.getSource() == sm3_i1) {
            gen = 0;
            gen++;
            wm.sendRule(vrule);
        }
        if(e.getSource() == sm3_i2) {
            gen = 0;
            simGrid.restart();
        }
    }
}
