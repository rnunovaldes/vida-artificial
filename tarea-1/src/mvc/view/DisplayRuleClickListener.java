package mvc.view;

/**
 * Para implementar un listener de las reglas con cuadrados clicables
 */
public interface DisplayRuleClickListener {
    void onDisplayRuleClicked(DisplayRule displayRule);
}
