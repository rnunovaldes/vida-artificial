package mvc.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import mvc.controller.Constants;

/**
 * Clase que modela un cuadro que puede ser negro o blanco y
 * puede o no ser clicable para cambiar su color
 */
public class SquarePanel extends JPanel {
    private Color color;
    private boolean isBlack;
    private SquarePanelClickListener clickListener;

    /**
     * Contructor de la clase
     *
     * @param black          Si el cuadro es inicial mente negro
     * @param canChangeColor Si puede ser clicado y cambiar su color
     */
    public SquarePanel(boolean black, boolean canChangeColor, SquarePanelClickListener listener) {
        color = black ? Color.BLACK : Color.WHITE;
        this.isBlack = black;
        this.clickListener = listener;
        setBorder(new LineBorder(Color.LIGHT_GRAY));

        if(canChangeColor) {
            addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    isBlack = !isBlack;
                    color = isBlack? Color.BLACK: Color.WHITE;
                    repaint();

                    if(clickListener != null) {
                        clickListener.onSquarePanelClicked(SquarePanel.this);
                    }
                }
            });
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(color);
        g.fillRect(0, 0, Constants.RULE_CELL_SIZE, Constants.RULE_CELL_SIZE);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(Constants.RULE_CELL_SIZE, Constants.RULE_CELL_SIZE);
    }

    /**
     * Obtiene si el cuadro es de color negro
     *
     * @return Si el cuadro es de color negro
     */
    public boolean getBlack() {
        return isBlack;
    }

    /**
     * Asigna al cuadro de color negro o blanco
     *
     * @param isBlack Si se asignará el color negro al cuadro
     */
    public void setBlack(boolean isBlack) {
        this.isBlack = isBlack;
        color = isBlack? Color.BLACK: Color.WHITE;
    }
}
