# La Hormiga de Langton

## Descripción
La "Hormiga de Langton" es un autómata celular bidimensional. Esta
implementación es una generalización que usa a los más ocho estados para las
celdas. Este autómata generalizado se basa en el comportamiento de una
"hormiga" que se mueve en una cuadrícula y cambia el estado de las celdas a
medida que avanza.

Las reglas generales son:

* Hormiga: Una hormiga se coloca en una celda en la cuadrícula bidimensional y
  mira inicialmente en una dirección específica (arriba, abajo, izquierda o
  derecha).
* Cuadrícula: La cuadrícula está compuesta por celdas que pueden estar en uno
  de los varios estados posibles.
* Movimiento de la hormiga: En función del estado de la celda en la que se
  encuentra, la hormiga puede girar en una dirección específica (por ejemplo,
  en sentido horario o antihorario) o permanecer en su dirección actual y
  cambia el estado de la celda en la que se encuentra en función del estado
  actual de la celda y su estado interno. Luego, avanza en la dirección que ve.
* Iteración: El proceso de movimiento de la hormiga se repite en cada paso de
  tiempo. La hormiga sigue moviéndose y cambiando el estado de las celdas
  mientras explora la cuadrícula.

## Programa
<div style="display: flex; align-items: center;">
  <a href="https://dev.java">
    <img src="https://dev.java/assets/images/java-affinity-logo-icode-lg.png" alt="Java Affinity Logo" width="70px" />
  </a>
  <p style="margin-left: 10px;">Este trabajo se programó con Java 17.0.7</p>
</div>
Para la ejecución, use alguno de los scripts proporcionados

###### Notas
Debería funcionar con Java 8

### Ejemplo de salida
![Regla RL en la iteración 11341](./out/RL(Paso%2011341).png)
