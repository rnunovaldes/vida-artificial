package mvc.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Clase dedicada a cargar estados del programa
 */
public final class Loader {

    /**
     * Carga las reglas de un archivo dado. Siempre devuelve un arreglo
     * de tamaño 2.
     * Si el archivo es vacío o no tiene el formato correcto, devuelve
     * dos listas vacías, pero si puede recuperarse algo de información,
     * trata de obtener las listas lo mejor que puede (basandose en las
     * direcciones) y autocompleta lo dañado
     *
     * @param filename El nombre del archivo que tiene las reglas
     * @return Un arreglo de tamaño 2 donde el primer elemento son las
     *         direcciones (lista de booleanos) y el segundo los colores
     *         (lista de bytes)
     */
    public static List[] loadRule(String filename) {
        List[] rules = new ArrayList[2];
        try {
            File file = new File(filename);
            BufferedReader reader = new BufferedReader(new FileReader(file));

            String line;
            byte i = 0;
            List<Boolean> directions = new ArrayList<>();
            List<Byte> colors = new ArrayList<>();
            while((line = reader.readLine()) != null) {
                if(i == 0) {
                    for(int j = 0; j < line.length(); j++) {
                        if(directions.size() == 8) break;

                        if(line.charAt(j) == 'R') directions.add(true);
                        else if(line.charAt(j) == 'L') directions.add(false);
                    }
                } else {
                    byte colorIndex = 0;
                    boolean[] usedColors = new boolean[8];
                    for(int j = 0; j < line.length(); j++) {
                        if(colors.size() == directions.size()) break;
                        colorIndex = (byte)(line.charAt(j)-'0');
                        if (colorIndex > 0 && colorIndex < 9 && !usedColors[colorIndex-1]) {
                            colors.add(colorIndex);
                            usedColors[colorIndex-1] = true;
                        }
                    }
                    if(colors.size() < directions.size()) {
                        for(byte j = 0; j < directions.size() - colors.size(); j++) {
                            for(byte k = 0; k < 8; k++) {
                                if(!usedColors[k]) {
                                    usedColors[k] = true;
                                    colors.add(k);
                                    break;
                                }
                            }
                        }
                    }
                }
                i++;
                if(i == 3) break;
            }
            rules[0] = directions;
            rules[1] = colors;
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return rules;
    }

    /**
     * Carga la información de la hormiga. Si está mal formada, es
     * vacía o incompleta, trata de recuperar lo máximo posible y
     * rellena los campos con información dañana con una por defecto
     *
     * @param filename El nombre del archivo que tiene la información de
     *                 la hormiga en el tablero
     * @return La ubicación de la hormiga y su dirección en un arreglo
     *         {x,y,d}
     */
    public static int[] loadAnt(String filename) {
        int[] ant = new int[3];
        try {
            File file = new File(filename);
            BufferedReader reader = new BufferedReader(new FileReader(file));

            String line;
            int i = 0;
            while((line = reader.readLine()) != null && i == 0) {
                if(i == 0) {
                    String[] sant = line.split("/");
                    int data;
                    try {
                        data = Integer.parseInt(sant[0]);
                        if(data >= Constants.cells_width) throw new Exception();
                        ant[0] = data;
                    } catch(Exception e) {
                        ant[0] = Constants.cells_width/2;
                    }
                    try {
                        data = Integer.parseInt(sant[1]);
                        if(data >= Constants.cells_height) throw new Exception();
                        ant[1] = data;
                    } catch(Exception e) {
                        ant[1] = Constants.cells_height/2;
                    }
                    try {
                        data = Integer.parseInt(sant[2]);
                        if(data > 3) throw new Exception();
                        ant[2] = Integer.parseInt(sant[2]);
                    } catch(Exception e) {
                        ant[2] = 0;
                    }
                }
                i++;
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ant;
    }

    /**
     * Carga la información de los colores del tablero de la simulación.
     * Si está mal formada, vacía o incompleta, recupera lo máximo
     * posible y rellena la información dañada con la casilla vacía (0)
     *
     * @param filename El nombre del archivo que tiene la información de
     *                 los colores en el tablero
     * @return El tablero con números que representan los colores
     *         pintados por la hormiga
     */
    public static byte[][] loadBoard(String filename) {
        byte[][] board = new byte[Constants.cells_width][Constants.cells_height];
        try {
            File file = new File(filename);
            BufferedReader reader = new BufferedReader(new FileReader(file));

            String line;
            int i = 0;
            byte colorIndex;
            while((line = reader.readLine()) != null) {
                if(i != 0) {
                    for(int j = 0; j < line.length(); j++) {
                        if(j == Constants.cells_height) break;
                        colorIndex = (byte)(line.charAt(j)-'0');
                        if (colorIndex > 0 && colorIndex < 9)
                            board[j][i-1] = colorIndex;
                    }
                }
                i++;
                if(i > Constants.cells_height) break;
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return board;
    }
}
