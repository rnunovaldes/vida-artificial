package mvc.controller;

import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;

/**
 * Clase dedicada a guardar estados e imágenes del programa
 */
public final class Saver {

    /**
     * Premite guardar las reglas de la simulación
     *
     * @param filename El nombre del archivo que va a tener las reglas
     * @param directions Si se gira a la derecha o no en ese orden
     * @param colors Los colores a cambiar las casillas en ese orden
     * @return Si se pudo guardar correctamente o no
     */
    public static boolean saveRule(String filename, List<Boolean> directions,
                                   List<Byte> colors) {
        try {
            File file = new File(filename);
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));
            for(boolean d: directions) {
                if(d) writer.write("R");
                else writer.write("L");
            }
            writer.newLine();
            for(Byte c: colors) writer.write(c.toString());
            writer.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Permite guardar el tablero actual de la simulación
     *
     * @param filename El nombre del archivo que va a tener el tablero
     * @param board Una matriz numérica que describe los colores del
     *              tablero
     * @param ant La ubicación y dirección de la hormiga en el tablero
     * @return Si se pudo guardar o no
     */
    public static boolean saveBoard(String filename, byte[][] board, int[] ant) {
        try {
            File file = new File(filename);
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));
            writer.write(String.format("%d/%d/%d", ant[0], ant[1], ant[2]));
            writer.newLine();
            for(int y = 0; y < Constants.cells_height; y++) {
                for(int x = 0; x < Constants.cells_width; x++) {
                    writer.write(Byte.toString(board[x][y]));
                }
                if(y != Constants.cells_height-1) writer.newLine();
            }
            writer.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Método para crear un archivo en formato png del tablero actual de
     * la simulación en una ruta dada
     *
     * @param filename El nombre para la imagen a guardar
     * @param board La matriz numérica a representar como imágen
     */
    public static boolean saveAsImage(String filename, byte[][] board) {
        try {
            File file = new File(filename);
            BufferedImage image = generateBufferedImage(board);
            ImageIO.write(image, "png", file);
        } catch(IOException e) {
            String workingDir = System.getProperty("user.dir");
            System.out.println("Current working directory : " + workingDir);
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Método para generar un BufferedImage a partir de una matriz
     * numérica que representa los distintos colores
     *
     * @param pixeles Una matriz de números donde cada uno representa un
     *                color distinto en el BufferedImage
     * @return La imagen en formato de BufferedImage
     */
    private static BufferedImage generateBufferedImage(byte[][] pixeles) {
        BufferedImage nImage = new BufferedImage(Constants.cells_width * Constants.CELL_SIZE,
            Constants.cells_height * Constants.CELL_SIZE, BufferedImage.TYPE_INT_RGB);
        for(int i = 0; i < Constants.cells_height; i++) {
            for(int j = 0; j < Constants.cells_width; j++) {
                int fixedi = i * Constants.CELL_SIZE;
                int fixedj = j * Constants.CELL_SIZE;
                int color = 0xFFFFFF;
                switch(pixeles[j][i]) {
                    case 1: color = 0xd3d3d3;
                    break;
                    case 2: color = 0xa9a9a9;
                    break;
                    case 3: color = 0xff0000;
                    break;
                    case 4: color = 0x00ffff;
                    break;
                    case 5: color = 0xffff00;
                    break;
                    case 6: color = 0xff00ff;
                    break;
                    case 7: color = 0xffa500;
                    break;
                    case 8: color = 0x00ff00;
                }
                for (byte y = 0; y < Constants.CELL_SIZE; y++) {
                    for (byte x = 0; x < Constants.CELL_SIZE; x++) {
                        nImage.setRGB(fixedj + x, fixedi + y, color);
                    }
                }
            }
        }
        return nImage;
    }
}
