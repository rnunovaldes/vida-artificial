package mvc.controller;

import java.awt.Dimension;
import java.awt.Toolkit;

/**
 * Contiene valores constantes que son relevantes para varias
 * partes del programa
 */
public final class Constants {
    public static final byte CELL_SIZE = 10;

    public static final String SEP = System.getProperty("file.separator");
    public static final String RES = "." + Constants.SEP + "res";
    public static final String OUT = "." + Constants.SEP + "out";

    private static final Dimension SCREEN_SIZE = Toolkit.getDefaultToolkit().getScreenSize();
    public static final int SCREEN_WIDTH = (int) SCREEN_SIZE.getWidth();
    public static final int SCREEN_HEIGHT = (int) SCREEN_SIZE.getHeight();

    public static int cells_width, cells_height = 1;
    public static byte topology = 0;
    public static byte moves_per_second = 5;
}
