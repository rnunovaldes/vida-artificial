package mvc.controller;

import mvc.view.Window;

import java.util.List;

import mvc.model.Model;

/**
 * Clase que coordina las ventanas y la lógica del proyecto
 */
public class WindowManager {
    private Window window;
    private Model model;
    private boolean isSimulationRunning;

    /**
     * Constructor de la clase
     */
    public WindowManager() {
        window = new Window(this);
        model = new Model(this);
    }

    /**
     * Envía los colores y su orden y las direcciónes y su orden
     *
     * @param colors Qué colores y en qué orden se van a usar
     * @param directions Qué direcciones y en qué orden se van a usar
     */
    public void sendRules(List<Byte> colors, List<Boolean> directions) {
        model.setRules(colors, directions);
    }

    /**
     * Método que envía los valores de la primera generación a usar
     * en la simulación al modelo y empieza la simulación
     *
     * @param board Los valores de los colores en el tablero
     * @param ant La ubicaión y dirección refernte a la hormiga
     */
    public void startSimulation(byte[][] board, int[] ant) {
        model.setBoard(board, ant);
        //window.disableMenuItems();
        if(!isSimulationRunning) {
            isSimulationRunning = true;
            Thread simulationThread = new Thread(() -> {
                while(isSimulationRunning) {
                    model.doSimulation();
                //window.enableMenuItems();
                }
            });
            simulationThread.start();
        }
    }

    public void stopSimulation() {
        isSimulationRunning = false;
    }

    /**
     * Envía los valores de una generación resultante de la simulación
     * a la IU
     *
     * @param nextBoard El nuevo tablero que debe ser mostrada
     * @param nextAnt La nueva ubicación y dirección de la hormiga
     * @param stop Si se debe detener la simulación
     */
    public void sendNextGen(byte[][] nextBoard, int[] nextAnt, boolean stop) {
        if(stop) isSimulationRunning = false;
        window.paintNextGen(nextBoard, nextAnt, stop);
    }
}
