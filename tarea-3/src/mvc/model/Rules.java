package mvc.model;

import java.io.Serializable;
import java.util.List;

import mvc.controller.Constants;

/**
 * Clase que modela las reglas a seguir para la simulación de las
 * hormigas de Langton y puede obtener la generación siguiente a una
 * indicada como actual
 */
public class Rules implements Serializable {

    private byte rulesLength;
    private byte[][] board;
    private List<Byte> colors;
    private List<Boolean> directions;
    private int[] ant = new int[3];

    /**
     * Constructor de <code>Rules</code>. Por defecto, inicia un tablero
     * sin color en ninguna celda y la hormiga a la mitad del tablero
     * viendo para arriba
     *
     * @param colors La lista de los colores en orden
     * @param direction La lista de las direcciones (si va a la derecha)
     *                  en orden
     */
    public Rules(List<Byte> colors, List<Boolean> directions) {
        this.colors = colors;
        this.directions = directions;
        rulesLength = (byte)colors.size();
        board = new byte[Constants.cells_width][Constants.cells_height];
        ant[0] = Constants.cells_width/2;
        ant[1] = Constants.cells_height/2;
        ant[2] = 0;
    }

    /**
     * Asigna un tablero y la posición de la hormiga
     *
     * @param currentState El estado de las celdas inicialmente
     * @param antX La ubicación de la hormiga en la coordenada x
     * @param antY La ubicación de la hormiga en la coordenada y
     *             (de arriba a abajo)
     * @param antD Dirección a donde mira la hormiga
     *             0-arriba 1-derecha 2-abajo 3-izquierda
     *             (hace módulo 4 de ser necesario)
     */
    public void setBoard(byte[][] board, int antX, int antY, byte antD) {
        this.board = board;
        this.ant[0] = antX;
        this.ant[1] = antY;
        this.ant[2] = antD;
    }

    /**
     * Genera la siguiente generación según la el estado actual de las
     * casiilas y la ubicación de la hormiga y actualiza este al
     * correspondiente
     *
     * @return Un valor booleano que indica si se pueden obtener más
     *         generaciones después de esta
     */
    public boolean nextGen() {
        byte color = board[ant[0]][ant[1]];
        boolean hasNext = false;
        if(color == 0 || !colors.contains(color)) color = colors.get(0);
        int i = colors.indexOf(color);
        byte rcolor = colors.get((i+1) % rulesLength);
        boolean rdirection = directions.get((i+1) % rulesLength);
        ant[2] = (rdirection)? (ant[2]+1) % 4: (ant[2]+3) % 4;
        board[ant[0]][ant[1]] = rcolor;
        switch(Constants.topology) {
            case 0: hasNext = genBordersJoined();
            break;
            case 1: hasNext = genEndsWithBorder();
            break;
            case 2: hasNext = genBorderReflects();
        }
        return hasNext;
    }

    private boolean genBordersJoined() {
        switch(ant[2]) {
            case 0: ant[1] = ant[1]-1;
                if(ant[1] < 0) ant[1] = Constants.cells_height-1;
            break;
            case 1: ant[0] = ant[0]+1;
                if(ant[0] == Constants.cells_width) ant[0] = 0;
            break;
            case 2: ant[1] = ant[1]+1;
                if(ant[1] == Constants.cells_height) ant[1] = 0;
            break;
            case 3: ant[0] = ant[0]-1;
                if(ant[0] < 0) ant[0] = Constants.cells_width-1;
        }
        return true;
    }

    private boolean genEndsWithBorder() {
        switch(ant[2]) {
            case 0: ant[1] = ant[1]-1;
                if(ant[1] < 0) return false;
            break;
            case 1: ant[0] = ant[0]+1;
                if(ant[0] == Constants.cells_width) return false;
            break;
            case 2: ant[1] = ant[1]+1;
                if(ant[1] == Constants.cells_height) return false;
            break;
            case 3: ant[0] = ant[0]-1;
                if(ant[0] < 0) return false;
        }
        return true;
    }

    private boolean genBorderReflects() {
        switch(ant[2]) {
            case 0: ant[1] = ant[1]-1;
                if(ant[1] < 0) {
                    ant[1] = ant[1]+2;
                    ant[2] = 2;
                }
            break;
            case 1: ant[0] = ant[0]+1;
                if(ant[0] == Constants.cells_width) {
                    ant[0] = ant[0]-2;
                    ant[2] = 3;
                }
            break;
            case 2: ant[1] = ant[1]+1;
                if(ant[1] == Constants.cells_height) {
                    ant[1] = ant[1]-2;
                    ant[2] = 0;
                }
            break;
            case 3: ant[0] = ant[0]-1;
                if(ant[0] < 0) {
                    ant[0] = ant[0]+2;
                    ant[2] = 1;
                }
        }
        return true;
    }

    /*
       topologyOptions = {
            "Bordes contrarios unidos",
            "Bordes terminan simulación",
            "Bordes reflejantes"
        }
     */

    /**
     * Obtiene la matriz con el estado actual del tablero
     *
     * @return Una matriz con valores numéricos que indican colores
     */
    public byte[][] getBoard() {
        return board;
    }

    /**
     * Obtiene lo referente a la ubicación y dirección de la hormiga en
     * el tablero
     *
     * @return Un arreglo donde el 1er elemento es la coordenada x, el
     *         2do la y de arriba a abajo y la 3ra a dónde apunta
     *         (0-arriba 1-derecha 2-abajo 3-izquierda)
     */
    public int[] getAnt() {
        return ant;
    }
}
