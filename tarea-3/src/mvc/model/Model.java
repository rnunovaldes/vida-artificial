package mvc.model;

import java.util.List;

import mvc.controller.Constants;
import mvc.controller.WindowManager;

/**
 * Clase que se encarga de pedir la serialización y deserialización de
 * reglas, además de solicitar ejecuciones de las reglas
 */
public class Model {
    private WindowManager wm;
    private Rules srules;

    /**
     * Constructor de la clase
     *
     * @param wm El coordinador entre las ventanas y la lógica
     */
    public Model(WindowManager wm) {
        this.wm = wm;

    }

    /**
     * Inicia las reglas de la simulación
     *
     * @param colors Los colores en el orden en que deben ser tomados
     * @param directions Las direcciones en el orden en que deben ser
     *                   tomadas
     */
    public void setRules(List<Byte> colors, List<Boolean> directions) {
        srules = new Rules(colors, directions);
    }

    /**
     * Asigna un estado de colores para el teblero y la ubicación de la
     * hormiga y su dirección para la simulación
     *
     * @param board El tablero
     * @param antX Un arreglo que tienen la posición x, la posición y de
     *             arriba a abajo y la dirección de la hormiga en el
     *             tablero
     */
    public void setBoard(byte[][] board, int[] ant) {
        srules.setBoard(board, ant[0], ant[1], (byte)ant[2]);
    }

    /**
     * Genera el siguiente estado correspondiente
     *
     * @return Si se completó
     */
    public boolean doSimulation() {
        boolean stop = !srules.nextGen();
        wm.sendNextGen(srules.getBoard(), srules.getAnt(), stop);
        try {
            Thread.sleep(1000/Constants.moves_per_second);
        } catch(InterruptedException e) {
            e.printStackTrace();
        }
        return true;
    }
}
