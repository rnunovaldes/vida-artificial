package mvc.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Polygon;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

/**
 * Clase que genera un menú que tiene la posibilidad de mostrar 8
 * turmitas de 8 colores distintos y dibuja y administra los espacios
 * para colocar y rdenar las turmitas de la simulación de las hormigas
 * de Langton
 */
public class RulesPanel extends JPanel
    implements RuleComponentListener {

    private RuleComponent[] rules = new RuleComponent[8];

    private volatile List<Point> actualValid = new ArrayList<>(8);

    private Point[] validPositions = new Point[8];
    private int width, height, offheight;
    private RulesPanelListener rpl;

    /**
     * Contructor de la clase
     *
     * @param width Anchura preferida para este menú (si es menor a 200,
     *              200 será el preferido)
     * @param height Largo preferido para este menú (si es menor a 400,
     *               400 será el preferido)
     */
    public RulesPanel(int width, int height) {
        this.width = (width < 200)? 200: width;
        this.height = (height < 400)? 400: height;

        setLayout(null);
        setPreferredSize(new Dimension(this.width, this.height));

        int rheight = 125;
        offheight = (this.height - rheight) / 4;
        for (byte i = 0; i < 4; i++) {
            rules[i] = new RuleComponent(40, 40, (byte)(i+1));
            rules[i+4] = new RuleComponent(40, 40, (byte)(i+5));
            rules[i].addRuleComponentListener(this);
            rules[i+4].addRuleComponentListener(this);
            add(rules[i]);
            add(rules[i+4]);
            rules[i].setBounds(10 + 40*i + i*5, 10, 40, 40);
            rules[i+4].setBounds(10 + 40*i+ i*5, 55, 40, 40);

            validPositions[i] = new Point(35, rheight + offheight * i);
            validPositions[i+4] = new Point(120, rheight + offheight * i);
            actualValid.add(validPositions[i]);
            actualValid.add(validPositions[i+4]);
            rules[i].setValidLocations(actualValid);
            rules[i+4].setValidLocations(actualValid);
        }
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        g.setColor(Color.WHITE);
        g.fillRect(0,0, getWidth(), getHeight());

        g.setColor(Color.LIGHT_GRAY);
        g.fillRect(10, 10, 40, 40);
        g.setColor(Color.DARK_GRAY);
        g.fillRect(55, 10, 40, 40);
        g.setColor(Color.RED);
        g.fillRect(100, 10, 40, 40);
        g.setColor(Color.BLUE);
        g.fillRect(145, 10, 40, 40);
        g.setColor(Color.YELLOW);
        g.fillRect(10, 55, 40, 40);
        g.setColor(Color.MAGENTA);
        g.fillRect(55, 55, 40, 40);
        g.setColor(Color.ORANGE);
        g.fillRect(100, 55, 40, 40);
        g.setColor(Color.GREEN);
        g.fillRect(145, 55, 40, 40);

        g.setColor(Color.BLACK);
        byte count = 0;
        for(Point p: validPositions) {
            byte x = (byte)p.getX();
            int y = (int)p.getY();
            g.fillRect(x, y, 40, 40);

            if(count % 8 > 0) {
                Polygon arrow = new Polygon();
                arrow.addPoint(x+20, y);
                arrow.addPoint(x+10, y-5);
                arrow.addPoint(x+30, y-5);
                g.fillPolygon(arrow);
            }
            if(count % 4 < 3) {
                g.fillRect(x + 19, y+40, 2, offheight);
            } else if(count == 3) {
                g.fillRect(x + 19, y+40, 2, 10);
                g.fillRect(x + 19, y+50, 43, 2);
                g.fillRect(x + 60, y+50, 2, -(offheight*3+65));
                g.fillRect(x + 60, (int)validPositions[4].getY()-15, 44, 2);
                g.fillRect(x + 104, (int)validPositions[4].getY()-15, 2, 15);
            }
            count++;
        }
    }

    /**
     * Activa o desactiva los movimientos de todas las 8 reglas en el
     * planel
     *
     * @param movesAllowed Si True las permite, en caso contrario no
     */
    public void allowMoves(boolean movesAllowed) {
        for(byte i =0; i < rules.length; i++) rules[i].allowMoves(movesAllowed);
    }

    /**
     * Obtiene una lista en el orden que deben ser tomados en cuenta las
     * turmitas, con los colores indicados por la regla
     *
     * @return Una lista de bytes que representan los colores
     */
    public List<Byte> getColors() {
        List<Byte> colors = new ArrayList<>(8);
        for(Point vp :validPositions) {
            if(!actualValid.contains(vp))
                colors.add(((RuleComponent)getComponentAt(vp)).getColor());
        }
        return colors;
    }

    /**
     * Obtiene una lista en el orden que deben ser tomados en cuenta las
     * turmitas, con los las direcciones indicadas por la regla
     *
     * @return Una lista de booleanon que representan la dirección de
     *         cambio de la turmita
     */
    public List<Boolean> getDirections() {
        List<Boolean> direction = new ArrayList<>(8);
        for(Point vp :validPositions) {
            if(!actualValid.contains(vp))
                direction.add(((RuleComponent)getComponentAt(vp)).getDirection());
        }
        return direction;
    }

    /**
     * Asigna las direcciones y colores a usar como reglas de forma que
     * sea visible en la IU
     *
     * @param directions Una lista con las direcciones a girar la
     *                   hormiga (si es a la derecha o no) en ese orden
     * @param colors Una lista con los colores (números interpretados
     *               como colores) a pintar la celda en ese orden
     */
    public void setRules(List<Boolean> directions, List<Byte> colors) {
        if(actualValid.size() != 8) {
            for(byte i = 0; i < 8; i++) {
                rules[i].returnToInitialLocation();
            }
        }
        for(byte i = 0; i < directions.size(); i++) {
            int x = (int)validPositions[i].getX();
            int y = (int)validPositions[i].getY();
            byte color = colors.get(i);
            actualValid.remove(validPositions[i]);
            rules[color-1].setDirection(directions.get(i));
            rules[color-1].customSetLocation(x, y);
        }
    }

    public void addRulesPanelListener(RulesPanelListener rpl) {
        this.rpl = rpl;
    }

    @Override
    public void onRuleMoved(RuleComponent squarePanel) {
        rpl.onRuleMoved(RulesPanel.this);
    }
}
