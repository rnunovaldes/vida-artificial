package mvc.view;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;

import javax.swing.JPanel;

import mvc.controller.Constants;
import utils.graphicInterface.ColorUtil;

/**
 * Componente para mostrar una rejilla interactuable del la simulación
 * de la hormiga de Langton cada celda es un espacio que puede ser
 * pintado de distintos colores por una hormiga de Langton
 */
public class GridComponent extends JPanel implements MouseListener {
    private byte[][] grid = new byte[1][1];
    private int ant[] = { 0, 0, 0 };
    private int width, height;
    private List<Byte> validColors;

    /**
     * Contructor de la clase
     */
    public GridComponent() {
        addMouseListener(this);
    }

    /**
     * Reinicia el tablero a uno vacío
     */
    public void resetBoard() {
        grid = new byte[Constants.cells_width][Constants.cells_height];
        ant[0] = Constants.cells_width/2;
        ant[1] = Constants.cells_height/2;
        ant[2] = 0;
        repaint();
    }

    @Override
    public void setBounds(int x, int y, int width, int height) {
        super.setBounds(x, y, width, height);

        Constants.cells_width = (width/Constants.CELL_SIZE) - 2;
        Constants.cells_height = (height/Constants.CELL_SIZE) - 2;
        grid = new byte[Constants.cells_width][Constants.cells_height];

        this.width = getWidth();
        this.height = getHeight();

        ant[0] = Constants.cells_width/2;
        ant[1] = Constants.cells_height/2;
    }

    /**
     * Guarda una lista con los colores que las reglas permiten
     * actualmente para que sean los únicos disponibles en la rotación
     * de colores al clicar las celdas
     *
     * @param validColors La lista con máximo 8 colores validos en forma
     *                     de números del 1 al 8
     */
    public void setValidColors(List<Byte> validColors) {
        this.validColors = validColors;
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        int offsetw = Constants.CELL_SIZE + (width % Constants.CELL_SIZE);
        int offseth = Constants.CELL_SIZE + (height % Constants.CELL_SIZE);

        Color antColor = Color.BLACK, color = Color.WHITE;
        for(int row = 0; row < Constants.cells_width; row++) {
            for(int col = 0; col < Constants.cells_height; col++) {
                switch(grid[row][col]) {
                    case 1: color = Color.LIGHT_GRAY;
                    break;
                    case 2: color = Color.DARK_GRAY;
                    break;
                    case 3: color = Color.RED;
                    break;
                    case 4: color = Color.BLUE;
                    break;
                    case 5: color = Color.YELLOW;
                    break;
                    case 6: color = Color.MAGENTA;
                    break;
                    case 7: color = Color.ORANGE;
                    break;
                    case 8: color = Color.GREEN;
                    break;
                    default: color = Color.WHITE;
                }
                g.setColor(color);
                g.fillRect(Constants.CELL_SIZE + row * Constants.CELL_SIZE,
                           Constants.CELL_SIZE + col * Constants.CELL_SIZE,
                           Constants.CELL_SIZE, Constants.CELL_SIZE);
                if(ant[0] == row && ant[1] == col)
                    antColor = ColorUtil.getComplementaryColor(color);
            }
        }

        int[] xPoints = new int[3];
        int[] yPoints = new int[3];
        switch (ant[2]) {
            case 0: // Arriba
                xPoints[0] = Constants.CELL_SIZE + ant[0]*Constants.CELL_SIZE + Constants.CELL_SIZE/2;
                xPoints[1] = xPoints[0] - Constants.CELL_SIZE/2;
                xPoints[2] = xPoints[0] + Constants.CELL_SIZE/2;
                yPoints[0] = Constants.CELL_SIZE + ant[1]*Constants.CELL_SIZE;
                yPoints[1] = yPoints[0] + Constants.CELL_SIZE;
                yPoints[2] = yPoints[1];
                break;
            case 1: // Derecha
                xPoints[0] = Constants.CELL_SIZE + ant[0]*Constants.CELL_SIZE;
                xPoints[1] = xPoints[0] + Constants.CELL_SIZE;
                xPoints[2] = xPoints[0];
                yPoints[0] = Constants.CELL_SIZE + ant[1]*Constants.CELL_SIZE;
                yPoints[1] = yPoints[0] + Constants.CELL_SIZE/2;
                yPoints[2] = yPoints[0] + Constants.CELL_SIZE;
                break;
            case 2: // Abajo
                xPoints[0] = Constants.CELL_SIZE + ant[0]*Constants.CELL_SIZE;
                xPoints[1] = xPoints[0] + Constants.CELL_SIZE;
                xPoints[2] = xPoints[0] + Constants.CELL_SIZE/2;
                yPoints[0] = Constants.CELL_SIZE + ant[1]*Constants.CELL_SIZE;
                yPoints[1] = yPoints[0];
                yPoints[2] = yPoints[0] + Constants.CELL_SIZE;
                break;
            case 3: // Izquierda
                xPoints[0] = Constants.CELL_SIZE + ant[0]*Constants.CELL_SIZE;
                xPoints[1] = xPoints[0] + Constants.CELL_SIZE;
                xPoints[2] = xPoints[1];
                yPoints[0] = Constants.CELL_SIZE + ant[1]*Constants.CELL_SIZE + Constants.CELL_SIZE/2;
                yPoints[1] = yPoints[0] - Constants.CELL_SIZE/2;
                yPoints[2] = yPoints[0] + Constants.CELL_SIZE/2;
        }
        Polygon triangle = new Polygon(xPoints, yPoints, 3);
        g.setColor(antColor);
        g.fillPolygon(triangle);

        g.setColor(Color.GRAY);
        for(int i = Constants.CELL_SIZE; i <= width-Constants.CELL_SIZE; i += Constants.CELL_SIZE) {
            g.drawLine(i, Constants.CELL_SIZE, i, height - offseth);
        }
        for(int i = Constants.CELL_SIZE; i <= height-Constants.CELL_SIZE; i += Constants.CELL_SIZE) {
            g.drawLine(Constants.CELL_SIZE, i, width - offsetw, i);
        }
    }

    /**
     * Sirve para cambiar el color de un punto donde se ha clicado al
     * contrario que tenía antes de clicarlo
     *
     * @param me El MouseEvent con las coordenadas de donde se clicó
     */
    private void changeColor(MouseEvent me) {
        int x = me.getPoint().x/Constants.CELL_SIZE-1;
        int y = me.getPoint().y/Constants.CELL_SIZE-1;
        if(x >= 0 && x < Constants.cells_width && y >= 0 && y < Constants.cells_height) {
            if(validColors == null || validColors.isEmpty()) grid[x][y] = 0;
            else {
                byte actualColor = grid[x][y];
                if(actualColor == 0) grid[x][y] = validColors.get(0);
                else {
                    int index = validColors.indexOf(actualColor);
                    if(index == validColors.size()-1) grid[x][y] = 0;
                    else grid[x][y] = validColors.get(index+1);
                }
            }
        }
        repaint(Constants.CELL_SIZE + x * Constants.CELL_SIZE,
                Constants.CELL_SIZE + y * Constants.CELL_SIZE,
                Constants.CELL_SIZE, Constants.CELL_SIZE);
    }

    /**
     * Sirve para cambiar la posición de la hormiga si se clica en una
     * casilla donde no estaba y cambiar su dirección si se clica donde
     * ya está la hormiga
     *
     * @param me El MouseEvent con las coordenadas de donde se clicó
     */
    private void changePosition(MouseEvent me) {
        int x = me.getPoint().x/Constants.CELL_SIZE-1;
        int y = me.getPoint().y/Constants.CELL_SIZE-1;
        if(x >= 0 && x < Constants.cells_width && y >= 0 && y < Constants.cells_height) {
            if(ant[0] == x && ant[1] == y) {
                ant[2] = (ant[2]+1) % 4;
            } else {
                repaint(Constants.CELL_SIZE + ant[0] * Constants.CELL_SIZE,
                        Constants.CELL_SIZE + ant[1] * Constants.CELL_SIZE,
                        Constants.CELL_SIZE, Constants.CELL_SIZE);
                ant[0] = x;
                ant[1] = y;
            }
        }
        repaint(Constants.CELL_SIZE + x * Constants.CELL_SIZE,
                Constants.CELL_SIZE + y * Constants.CELL_SIZE,
                Constants.CELL_SIZE, Constants.CELL_SIZE);
    }

    /**
     * Asigna un nuevo tablero para la simulación y una nueva ubicación
     * para la hormiga de Langton
     *
     * @param newGrid Una matriz de números para sustituir el actual
     * @param x La coordenada x de la hormiga
     * @param y La coordenada y de arriba a abajo de la hormiga
     * @param d La dirección de la hormiga
     */
    public void setState(byte[][] newGrid, int x, int y, int d) {
        grid = newGrid;
        ant[0] = x;
        ant[1] = y;
        ant[2] = d;
        repaint();
    }

    /**
     * Obtiene cómo se ve al tablero actualmente
     *
     * @return El tablero con números que representan los colores
     *         pintados por la hormiga
     */
    public byte[][] getBoard() {
        return grid;
    }

    /**
     * Obtiene dónde está y a dónde mira la hormiga de langton en el
     * tablero
     *
     * @return La ubicación de la hormiga y su dirección en un arreglo
     *         {x,y,d}
     */
    public int[] getAnt() {
        return ant;
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if (e.getButton() == MouseEvent.BUTTON1) {
            changeColor(e);
        } else if (e.getButton() == MouseEvent.BUTTON3) {
            changePosition(e);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) { }
    @Override
    public void mousePressed(MouseEvent e) { }
    @Override
    public void mouseEntered(MouseEvent e) { }
    @Override
    public void mouseExited(MouseEvent e) { }
}
