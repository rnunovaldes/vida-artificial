package mvc.view;

/**
 * Para implementar un listener de los cuadrados desplazables
 */
public interface RuleComponentListener {

    /**
     * Indica cuando el componente se ha movido exitosamente
     *
     * @param ruleComponent
     */
    void onRuleMoved(RuleComponent ruleComponent);
}
