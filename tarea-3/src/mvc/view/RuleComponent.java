package mvc.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.border.LineBorder;

import utils.graphicInterface.ColorUtil;

/**
 * Clase que modela un componente que permite escoger su color y tamaño
 * la cual representa la dirección en la que girará una turmita y el
 * color que dejará en la celda de la simulación de las hormigas de
 * Langton
 */
public class RuleComponent extends JComponent
        implements MouseListener, MouseMotionListener {

    private volatile int lastX, lastY, screenX, screenY;
    private int thirdW, thirdH, headSize, locInitialPosX, locInitialPosY;

    private boolean started, movesAllowed = true, toRigth = true;
    private List<Point> locations = new ArrayList<>(0);
    private byte colorByte;
    private RuleComponentListener rcl;

    /**
     * Contructor de la clase del componente que genera un rectangulo de
     * las medidas indicadas y con un color de fondo indicado por los
     * números del 0 al 7. Esta tiene una flecha pintada hacia la
     * derecha inicialmente
     *
     * @param width El ancho del componente
     * @param height El largo del compoennte
     * @param color El número asociado al color:
     *              1-Gris 2-Negro 3-Rojo 4-Azul 5-Amarillo 6-Morado
     *              7-Naranja 8-Verde (Cualquier otro)-Blanco
     */
    public RuleComponent(int width, int height, byte color) {
        thirdW = width/3;
        thirdH = height/3;
        headSize = thirdW/3;
        colorByte = color;

        switch(colorByte) {
            case 1: setBackground(Color.LIGHT_GRAY);
            break;
            case 2: setBackground(Color.DARK_GRAY);
            break;
            case 3: setBackground(Color.RED);
            break;
            case 4: setBackground(Color.BLUE);
            break;
            case 5: setBackground(Color.YELLOW);
            break;
            case 6: setBackground(Color.MAGENTA);
            break;
            case 7: setBackground(Color.ORANGE);
            break;
            case 8: setBackground(Color.GREEN);
            break;
            default: setBackground(Color.WHITE);
        }

        setBorder(new LineBorder(Color.BLACK,2));
        setOpaque(true);
        setBounds(0, 0, width, height);
        setPreferredSize(new Dimension(width, height));

        lastX = locInitialPosX;
        lastY = locInitialPosY;

        addMouseListener(this);
        addMouseMotionListener(this);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(getBackground());
        g.fillRect(0, 0, getWidth(), getHeight());

        Polygon arrow = new Polygon();
        g.setColor(ColorUtil.getComplementaryColor(getBackground()));
        if(toRigth) {
            arrow.addPoint(2*thirdW, thirdH);
            arrow.addPoint(2*thirdW - headSize, thirdH + headSize);
            arrow.addPoint(2*thirdW - headSize, thirdH - headSize);
            g.fillRect(thirdW, thirdH-1, thirdW - headSize, 2);
            g.fillRect(thirdW, thirdH, 2, thirdH);
        } else {
            arrow.addPoint(thirdW, thirdH);
            arrow.addPoint(thirdW + headSize, thirdH + headSize);
            arrow.addPoint(thirdW + headSize, thirdH - headSize);
            g.fillRect(thirdW + headSize, thirdH-1, thirdW - headSize, 2);
            g.fillRect(2*thirdW, thirdH-1, 2, thirdH);
        }
        g.fillPolygon(arrow);
    }

    /**
     * Cambia el permiso de los componentes para ser movidos.
     *
     * @param allow Si True lo permite, en caso contrario no
     */
    public void allowMoves(boolean allow) {
        movesAllowed = allow;
    }

    /**
     * Asigna localizaciones en la pantalla como válidas para el
     * componente, lo que permitirá que este pueda ser arrastrado y
     * dejado en estas. Deben ser dadas como puntos globales para la
     * ventana y no de su componente padre.
     *
     * Si no se asigna ningún arreglo con puntos con este método,
     * entonces su única posición válida será la inicial
     *
     * @param locations Un arreglo con los puntos válidos
     */
    public void setValidLocations(List<Point> locations) {
        this.locations = locations;
    }

    /**
     * Obtiene un número asociado al color del componente
     *
     * @return El número asociado al color:
     *         0-Blanco 1-Negro 2-Rojo 3-Azul 4-Amarillo 5-Morado
     *         6-Naranja 7-Verde (Cualquier otro)-Gris
     */
    public byte getColor() {
        return colorByte;
    }

    /**
     * Obtiene la dirección a la que se apunta
     *
     * @return True si es a la derecha, False si es a la izquierda
     */
    public boolean getDirection() {
        return toRigth;
    }

    /**
     * Asigna la dirección indicada por el componente
     *
     * @return True si es a la derecha, False si es a la izquierda
     */
    public void setDirection(boolean direction) {
        toRigth = direction;
        repaint(thirdW, thirdH-headSize, 2*thirdH, 2*thirdW+headSize);
    }

    /**
     * Asigna una posición al componente, que revisa si el componente ya
     * se ha movido por primera vez, guarda la posisión anterior a este
     * movimiento y elimina de la lista de posisiones validas el lugar
     * en el que se ha puesto de ser posible
     *
     * @param x Posición en el eje x
     * @param y Posición en el eje y (de arriba a abajo)
     */
    public void customSetLocation(int x, int y) {
        if(!started) {
            locInitialPosX = getX();
            locInitialPosY = getY();
            started = true;
        }
        lastX = getX();
        lastY = getY();
        setLocation(x, y);
        if(lastX != locInitialPosX) locations.add(new Point(lastX,lastY));
        if(rcl != null) rcl.onRuleMoved(RuleComponent.this);
    }

    public void returnToInitialLocation() {
        if(!started) {
            locInitialPosX = getX();
            locInitialPosY = getY();
            started = true;
        }
        lastX = getX();
        lastY = getY();
        setLocation(locInitialPosX, locInitialPosY);
        if(lastX != locInitialPosX) locations.add(new Point(lastX,lastY));
        if(rcl != null) rcl.onRuleMoved(RuleComponent.this);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if(!movesAllowed) return;
        toRigth = !toRigth;
        repaint(thirdW, thirdH-headSize, 2*thirdH, 2*thirdW+headSize);
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if(!movesAllowed) return;
        if(!started) {
            locInitialPosX = getX();
            locInitialPosY = getY();
            started = true;
        }
        lastX = getX();
        lastY = getY();

        screenX = e.getXOnScreen();
        screenY = e.getYOnScreen();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if(!movesAllowed) return;
        int deltaX = getX();
        int deltaY = getY();
        if(deltaX < locInitialPosX + 20 && deltaX > locInitialPosX - 20
           && deltaY < locInitialPosY + 20 && deltaY > locInitialPosY - 20) {
            setLocation(locInitialPosX, locInitialPosY);
            if(lastX != locInitialPosX) {
                locations.add(new Point(lastX,lastY));
                if(rcl != null) rcl.onRuleMoved(RuleComponent.this);
            }
            return;
        }
        for(Point vl: locations) {
            int vlX = (int)vl.getX();
            int vlY = (int)vl.getY();
            if(deltaX < vlX + 20 && deltaX > vlX - 20
               && deltaY < vlY + 20 && deltaY > vlY - 20) {
                setLocation(vlX, vlY);
                if(lastX != locInitialPosX) locations.add(new Point(lastX,lastY));
                locations.remove(new Point(vlX, vlY));
                if(rcl != null) rcl.onRuleMoved(RuleComponent.this);
                return;
            }
        }
        setLocation(lastX, lastY);
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if(!movesAllowed) return;
        int deltaX = e.getXOnScreen() - screenX;
        int deltaY = e.getYOnScreen() - screenY;

        setLocation(lastX + deltaX, lastY + deltaY);
    }

    @Override
    public void mouseEntered(MouseEvent e) { }
    @Override
    public void mouseExited(MouseEvent e) { }
    @Override
    public void mouseMoved(MouseEvent e) { }

    public void addRuleComponentListener(RuleComponentListener rcl) {
        this.rcl = rcl;
    }
}
