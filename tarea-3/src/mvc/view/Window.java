package mvc.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JSlider;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;

import mvc.controller.Constants;
import mvc.controller.Loader;
import mvc.controller.Saver;
import mvc.controller.WindowManager;
import utils.fileManager.BoardFilter;
import utils.fileManager.RuleFilter;

/**
 * Clase para mostrar una ventana de la interfaz grafica
 */
public class Window extends JFrame
    implements ActionListener, RulesPanelListener {

    private final String[] TITLES = {
        /* 0 */"Simulación de la Hormiga de Langton",
        /* 1 */"Opciones",
        /* 2 */"Ayuda"
    };
    private final String GEN = "Paso %d";
    private final String[] OPTIONS = {
            /*  0 */"Archivo",
            /*  1 */"Guardar reglas",
            /*  2 */"Guardar tablero",
            /*  3 */"Cargar reglas",
            /*  4 */"Cargar tablero",
            /*  5 */"Guardar imagen",
            /*  6 */"Juego",
            /*  7 */"Iniciar",
            /*  8 */"Detener",
            /*  9 */"Reiniciar",
            /* 10 */"Opciones",
            /* 11 */"Mov/Seg",
            /* 12 */"Bordes",
            /* 13 */"Ayuda",
            /* 14 */"Reglas",
            /* 15 */"Celdas",
            /* 16 */"Hormiga",
            /* 17 */"Salir"
        };
    private final String[] MESSAGES = {
            /* 0 */"No hay regla que guardar",
            /* 1 */"Movimientos por segundo: %02d",
            /* 2 */"Tipo de plano:",
            /* 3 */"Las reglas están en el panel derecho, representadas como "
                    + "cuadros de colores con flechas que indican su dirección"
                    + "\n  * Para cambiar la dirección, clique una vez sobre el"
                    + " cuadro.\n  * El orden está indicado por los cuadros "
                    + "negros unidos por flechas negras. Arraste los cuadros "
                    + "sobre los cuadros negros.\n\n No podrá interactuar con "
                    + "los cuadros una vez iniciada la simulación",
            /* 4 */"Las celdas son interactuables pero solo cambiaran a un "
                    + "color que aparezca en las reglas especificadas.\n  * "
                    + "Haga clic izquierdo sobre las celdas para cambiar su "
                    + "color.\n\n Si no hay reglas establecidas, las celdas no "
                    + "serán interactuables por el clic izquierdo.",
            /* 5 */"La hormiga puede cambiar su posición y orientación "
                    + "iniciales al clicar en una celda.\n  * Haga clic derecho"
                    + " sobre una celda que no tenga a la hormiga para "
                    + "cambiarla de ubicación.\n  * Haga clic derecho sobre la "
                    + "hormiga para cambiar su orientación en dextrógiro."
        };
    private final Byte[] secondsOptions = {1,2,5,10,20,50,90};
    private final String[] topologyOptions = {
            "Bordes contrarios unidos",
            "Bordes terminan simulación",
            "Bordes reflejantes"
        };

    private int gen = 0;

    private JMenu menu1, menu2, menu3, menu4;
    private JMenuItem sm1_i1, sm1_i2, sm1_i3, sm1_i4, sm1_i5,
                      sm2_i1, sm2_i2, sm2_i3, sm3_i1, sm3_i2,
                      sm4_i1, sm4_i2, sm4_i3;
    private JButton exitButton;
    private JFileChooser fcLoad1, fcLoad2, fcSave1, fcSave2, fcSave3;
    private GridComponent simGrid;
    private JLabel genLabel;
    private JPanel sharedZone;
    private JButton playButton;
    private ImageIcon icon, pauseIcon, playIcon;
    private RulesPanel rulesPanel;
    private boolean isPaused = true;

    private final File OUTFILE = new File(Constants.OUT);

    protected WindowManager wm;

    /**
     * Constructor de la clase donde se inicializan todos los componentes
     * de la ventana principal
     *
     * @param wm El coordinador entre las ventanas y la logica
     */
    public Window(WindowManager wm) {
        this.wm = wm;
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        // setSize(480, 480);
        setTitle(TITLES[0]);
        // Centrar horizontalmente
        setUndecorated(true);
        setResizable(false);
        setLayout(new BorderLayout());
        int w = Constants.SCREEN_WIDTH-100;
        int h = Constants.SCREEN_HEIGHT-80;
        if(w < 650) w = 650;
        if(h < 550) h = 550;
        setSize(new Dimension(w, h));
        /*for ( java.awt.Window w : Window.getWindows() ) {
            GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().setFullScreenWindow( w );
        }*/
        int centerX = (Constants.SCREEN_WIDTH - w) / 2;
        setLocation(centerX, 0);
        icon = new ImageIcon(Constants.RES + Constants.SEP + "langtons-ant.png");
        setIconImage(icon.getImage());

        /* Pestaña de Archivo */
        JMenuBar mb = new JMenuBar();
        menu1 = new JMenu(OPTIONS[0]);
        sm1_i1 = new JMenuItem(OPTIONS[1]);
        sm1_i2 = new JMenuItem(OPTIONS[2]);
        sm1_i3 = new JMenuItem(OPTIONS[3]);
        sm1_i4 = new JMenuItem(OPTIONS[4]);
        sm1_i5 = new JMenuItem(OPTIONS[5]);
        menu1.add(sm1_i1);
        menu1.add(sm1_i2);
        menu1.add(new JSeparator());
        menu1.add(sm1_i3);
        menu1.add(sm1_i4);
        menu1.add(new JSeparator());
        menu1.add(sm1_i5);

        /* Pestaña de Juego */
        menu2 = new JMenu(OPTIONS[6]);
        sm2_i1 = new JMenuItem(OPTIONS[7]);
        sm2_i2 = new JMenuItem(OPTIONS[8]);
        sm2_i3 = new JMenuItem(OPTIONS[9]);
        menu2.add(sm2_i1);
        menu2.add(sm2_i2);
        menu2.add(new JSeparator());
        menu2.add(sm2_i3);
        sm2_i2.setEnabled(false);

        /* Pestaña Opciones */
        menu3 = new JMenu(OPTIONS[10]);
        sm3_i1 = new JMenuItem(OPTIONS[11]);
        sm3_i2 = new JMenuItem(OPTIONS[12]);
        menu3.add(sm3_i1);
        menu3.add(sm3_i2);

        /* Pestaña Ayuda */
        menu4 = new JMenu(OPTIONS[13]);
        sm4_i1 = new JMenuItem(OPTIONS[14]);
        sm4_i2 = new JMenuItem(OPTIONS[15]);
        sm4_i3 = new JMenuItem(OPTIONS[16]);
        menu4.add(sm4_i1);
        menu4.add(sm4_i2);
        menu4.add(sm4_i3);

        exitButton = new JButton(OPTIONS[17]);
        exitButton.setBorderPainted(false);
        exitButton.setContentAreaFilled(false);

        mb.add(menu1);
        mb.add(menu2);
        mb.add(menu3);
        mb.add(menu4);
        mb.add(exitButton);
        setJMenuBar(mb);

        fcLoad1 = new JFileChooser();
        fcLoad1.setCurrentDirectory(OUTFILE);
        fcLoad1.addChoosableFileFilter(new RuleFilter());
        fcLoad1.setAcceptAllFileFilterUsed(false);
        fcLoad2 = new JFileChooser();
        fcLoad2.setCurrentDirectory(OUTFILE);
        fcLoad2.addChoosableFileFilter(new BoardFilter());
        fcLoad2.setAcceptAllFileFilterUsed(false);
        // Add custom icons for file types.
        // fcEscoger.setFileView(new RuleFileView());
        /* File chooser para pestanya Archivo Guardar */
        fcSave1 = new JFileChooser();
        fcSave1.setCurrentDirectory(OUTFILE);
        fcSave1.setDialogType(JFileChooser.SAVE_DIALOG);
        fcSave1.setFileFilter(new RuleFilter());
        fcSave2 = new JFileChooser();
        fcSave2.setCurrentDirectory(OUTFILE);
        fcSave2.setDialogType(JFileChooser.SAVE_DIALOG);
        fcSave2.setFileFilter(new BoardFilter());
        fcSave3 = new JFileChooser();
        fcSave3.setCurrentDirectory(OUTFILE);
        fcSave3.setDialogType(JFileChooser.SAVE_DIALOG);
        fcSave3.setFileFilter(new FileNameExtensionFilter("png file","png"));
        genLabel = new JLabel(String.format(GEN, gen));

        /* Botón de pausa */
        pauseIcon = new ImageIcon(Constants.RES + Constants.SEP + "pause-icon.png");
        Image image = pauseIcon.getImage();
        Image newImg = image.getScaledInstance(24, 24,  java.awt.Image.SCALE_SMOOTH);
        pauseIcon = new ImageIcon(newImg);
        /* Botón de play */
        playIcon = new ImageIcon(Constants.RES + Constants.SEP + "play-icon.png");
        image = playIcon.getImage();
        newImg = image.getScaledInstance(24, 24,  java.awt.Image.SCALE_SMOOTH);
        playIcon = new ImageIcon(newImg);
        playButton = new JButton(playIcon);
        playButton.setVisible(true);
        playButton.setBorderPainted(false);
        playButton.setFocusPainted(false);
        playButton.setContentAreaFilled(false);

        sharedZone = new JPanel(new FlowLayout());
        sharedZone.add(playButton);
        sharedZone.add(genLabel);
        sharedZone.setPreferredSize(new Dimension(getWidth(), 35));
        add(sharedZone, BorderLayout.NORTH);

        /* Simulación */
        simGrid = new GridComponent();
        simGrid.setPreferredSize(new Dimension(getWidth()-200, getHeight()-35));
        add(simGrid, BorderLayout.CENTER);

        /* Panel de reglas */
        rulesPanel = new RulesPanel(200, getHeight()-35);
        add(rulesPanel,BorderLayout.EAST);

        pack();
        setVisible(true);

        /* Listeners */
        sm1_i1.addActionListener(this);
        sm1_i2.addActionListener(this);
        sm1_i3.addActionListener(this);
        sm1_i4.addActionListener(this);
        sm1_i5.addActionListener(this);
        sm2_i1.addActionListener(this);
        sm2_i2.addActionListener(this);
        sm2_i3.addActionListener(this);
        sm3_i1.addActionListener(this);
        sm3_i2.addActionListener(this);
        sm4_i1.addActionListener(this);
        sm4_i2.addActionListener(this);
        sm4_i3.addActionListener(this);
        exitButton.addActionListener(this);
        playButton.addActionListener(this);
        rulesPanel.addRulesPanelListener(this);
    }

    /**
     * Hace que su gridComponent pinte donde hay celulas vivas según el
     * modelo
     *
     * @param bs Dónde es que deben pintar los cuadrados del
     *           gridComponent
     */
    public void paintNextGen(byte[][] newBoard, int[] newAnt, boolean stop) {
        genLabel.setText(String.format(GEN, ++gen));
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                simGrid.setState(newBoard, newAnt[0], newAnt[1], newAnt[2]);
            }
        });
        if(stop) toggleSimulationState();
    }

    /**
     * Alterna el estado de la ventana entre pausado y corriendo,
     * activando y desactivando varias opciones del usuario
     * respectivamente, además de altenar entre los símbolos del botón
     * de inicio y pausa
     */
    private void toggleSimulationState() {
        isPaused = !isPaused;
        if(!isPaused) {
            sm2_i1.setEnabled(false);
            sm1_i2.setEnabled(false);
            sm1_i3.setEnabled(false);
            sm1_i4.setEnabled(false);
            sm1_i5.setEnabled(false);
            sm3_i2.setEnabled(false);
            sm2_i2.setEnabled(true);

            rulesPanel.allowMoves(false);
            playButton.setIcon(pauseIcon);
        } else {
            sm2_i2.setEnabled(false);
            sm2_i1.setEnabled(true);
            sm1_i2.setEnabled(true);
            sm1_i3.setEnabled(true);
            sm1_i4.setEnabled(true);
            sm1_i5.setEnabled(true);
            sm3_i2.setEnabled(true);

            rulesPanel.allowMoves(true);
            playButton.setIcon(playIcon);
        }
    }

    @Override
    public void onRuleMoved(RulesPanel rPanel) {
        simGrid.setValidColors(rPanel.getColors());
    }

    /**
     * Método que recibe las aciones realizadas en la ventana
     *
     * @param e El <code>ActionEvent</code> registrado
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == sm1_i1) {
            /* Save rule */
            String str = "";
            List<Boolean> directions = rulesPanel.getDirections();
            List<Byte> colors = rulesPanel.getColors();
            if(directions.isEmpty()) {
                JOptionPane.showMessageDialog(null, MESSAGES[0], "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
            for(boolean d: directions) str +=(d)? "R": "L";
            fcSave1.setSelectedFile(new File(str));
            if(fcSave1.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                String filename = fcSave1.getSelectedFile().getAbsolutePath();
                if(!filename.toLowerCase().endsWith(".rul")) filename += ".rul";
                Saver.saveRule(filename, directions, colors);
            }
        }
        else if(e.getSource() == sm1_i2) {
            /* Save board */
            if(fcSave2.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                String filename = fcSave2.getSelectedFile().getAbsolutePath();
                if(!filename.toLowerCase().endsWith(".bor")) filename += ".bor";
                Saver.saveBoard(filename, simGrid.getBoard(), simGrid.getAnt());
            }
        }
        else if(e.getSource() == sm1_i3) {
            /* Load rule */
            gen = 0;
            genLabel.setText(String.format(GEN, gen));
            if(fcLoad1.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                String statePath = fcLoad1.getSelectedFile().getAbsolutePath();
                List[] data = Loader.loadRule(statePath);
                List<Boolean> directions = (List<Boolean>)data[0];
                List<Byte> colors = (List<Byte>)data[1];
                rulesPanel.setRules(directions, colors);
            }
        }
        else if(e.getSource() == sm1_i4) {
            /* Load board */
            gen = 0;
            genLabel.setText(String.format(GEN, gen));
            if(fcLoad2.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                String statePath = fcLoad2.getSelectedFile().getAbsolutePath();
                byte[][] board = Loader.loadBoard(statePath);
                int[] ant = Loader.loadAnt(statePath);
                simGrid.setState(board, ant[0], ant[1], ant[2]);
            }
        }
        else if(e.getSource() == sm1_i5) {
            /* Save image */
            String str = "";
            for(boolean d: rulesPanel.getDirections()) str +=(d)? "R": "L";
            File defaultName = new File(str + "(" +String.format(GEN, gen) + ")");
            fcSave3.setSelectedFile(defaultName);
            if(fcSave3.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                String filename = fcSave3.getSelectedFile().getAbsolutePath();
                if(!filename.toLowerCase().endsWith(".png")) filename += ".png";
                Saver.saveAsImage(filename, simGrid.getBoard());
            }
        }
        else if(e.getSource() == sm2_i1 || (e.getSource() == playButton && isPaused)) {
            /* Play */
            toggleSimulationState();
            wm.sendRules(rulesPanel.getColors(), rulesPanel.getDirections());
            wm.startSimulation(simGrid.getBoard(), simGrid.getAnt());
        }
        else if(e.getSource() == sm2_i2 || (e.getSource() == playButton && !isPaused)) {
            /* Pause */
            toggleSimulationState();
            wm.stopSimulation();
        }
        else if(e.getSource() == sm2_i3) {
            /* Reset */
            isPaused = false;
            toggleSimulationState();
            wm.stopSimulation();
            gen = 0;
            genLabel.setText(String.format(GEN, gen));
            simGrid.resetBoard();
        }
        else if(e.getSource() == sm3_i1) {
            JSlider js_seconds = new JSlider(1, 90, Constants.moves_per_second);
            JLabel valueLabel = new JLabel(
                    String.format(MESSAGES[1], Constants.moves_per_second)
                );
            js_seconds.addChangeListener(new ChangeListener() {
                @Override
                public void stateChanged(ChangeEvent e) {
                    valueLabel.setText(
                            String.format(MESSAGES[1], js_seconds.getValue())
                        );
                }
            });
            JComboBox<Byte> cb_seconds = new JComboBox<>(secondsOptions);
            cb_seconds.setSelectedItem(Constants.moves_per_second);
            JPanel panel = new JPanel();
            panel.setLayout(new GridLayout(0, 1));
            panel.add(valueLabel);
            panel.add(js_seconds);

            int result = JOptionPane.showConfirmDialog(panel, panel, TITLES[1],
                    JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
            if (result == JOptionPane.OK_OPTION) {
                Constants.moves_per_second = (byte)js_seconds.getValue();
            }
        }
        else if(e.getSource() == sm3_i2) {
            JComboBox<String> cb_topology = new JComboBox<>(topologyOptions);
            cb_topology.setSelectedIndex(Constants.topology);
            JPanel panel = new JPanel();
            panel.setLayout(new GridLayout(0, 1));
            panel.add(new JLabel(MESSAGES[2]));
            panel.add(cb_topology);

            int result = JOptionPane.showConfirmDialog(null, panel, TITLES[1],
                    JOptionPane.OK_CANCEL_OPTION,JOptionPane.QUESTION_MESSAGE);
            if (result == JOptionPane.OK_OPTION) {
                Constants.topology = (byte)cb_topology.getSelectedIndex();
            }
        }
        else if(e.getSource() == sm4_i1) {
            JOptionPane.showMessageDialog(null, MESSAGES[3]);
        }
        else if(e.getSource() == sm4_i2) {
            JOptionPane.showMessageDialog(null, MESSAGES[4]);
        }
        else if(e.getSource() == sm4_i3) {
            JOptionPane.showMessageDialog(null, MESSAGES[5]);
        }
        else if(e.getSource() == exitButton) {
            System.exit(0);
        }
    }
}
