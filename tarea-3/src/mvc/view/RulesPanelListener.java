package mvc.view;

/**
 * Para implementar un listener de los 8 cuadrados desplazables
 */
public interface RulesPanelListener {

    /**
     * Indica cuando una de las reglas se ha movido exitosamente
     *
     * @param rPanel
     */
    public void onRuleMoved(RulesPanel rPanel);
}
