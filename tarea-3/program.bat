@ECHO off
@chcp 65001>nul

SET "EXEC=main.Principal"
SET "JAVA_PRIN=src\main\Principal.java"
SET "CLAS_PRIN=bin\main\Principal.class"
SET "CLAS_VIEW1=bin\mvc\view\Window.class"
SET "CLAS_VIEW2=bin\mvc\view\GridComponent.class"
SET "CLAS_VIEW3=bin\mvc\view\RuleComponent.class"
SET "CLAS_VIEW4=bin\mvc\view\RuleComponentListener.class"
SET "CLAS_VIEW5=bin\mvc\view\RulesPanel.class"
SET "CLAS_VIEW6=bin\mvc\view\RulesPanelListener.class"
SET "CLAS_CON1=bin\mvc\controller\Constants.class"
SET "CLAS_CON2=bin\mvc\controller\WindowManager.class"
SET "CLAS_CON3=bin\mvc\controller\Loader.class"
SET "CLAS_CON4=bin\mvc\controller\Saver.class"
SET "CLAS_MOD1=bin\mvc\model\Model.class"
SET "CLAS_MOD2=bin\mvc\model\Rules.class"
SET "CLAS_UTIL1=bin\utils\fileManager\BoardFilter.class"
SET "CLAS_UTIL2=bin\utils\fileManager\RuleFilter.class"
SET "CLAS_UTIL3=bin\utils\fileManager\Utils.class"
SET "CLAS_UTIL4=bin\utils\graphicInterface\ColorUtil.class"

:UseChoice
choice /c SN /n /m "¿Compilar (S/N)?"
IF ERRORLEVEL == 2 goto CON
if ERRORLEVEL == 1 goto COM

:CON
IF EXIST %CLAS_PRIN% IF EXIST %CLAS_VIEW1% IF EXIST %CLAS_VIEW2% (
IF EXIST %CLAS_VIEW3% IF EXIST %CLAS_VIEW4% IF EXIST %CLAS_VIEW5% (
IF EXIST %CLAS_VIEW6% IF EXIST %CLAS_CON1% IF EXIST %CLAS_CON2% (
IF EXIST %CLAS_CON3% IF EXIST %CLAS_CON4% IF EXIST %CLAS_MOD1% (
IF EXIST %CLAS_MOD2% IF EXIST %CLAS_UTIL1% IF EXIST %CLAS_UTIL2% (
IF EXIST %CLAS_UTIL2% IF EXIST %CLAS_UTIL3% IF EXIST %CLAS_UTIL4% (
GOTO EXE
))))))

:COM
IF NOT EXIST bin MKDIR bin
ECHO Vamos a compilar
javac -encoding UTF-8 -d bin -cp ..;src %JAVA_PRIN%

:EXE
ECHO Vamos a ejecutar el compilado
java -cp bin %EXEC%
PAUSE
ECHO Se eliminara la carpeta con los class
RMDIR /s bin
EXIT
