# El juego de la vida

## Descripción
El "Juego de la Vida" es un autómata celular bidimensional inventado por John
Conway. Se juega en una cuadrícula rectangular donde cada celda puede estar
viva (1) o muerta (0). En cada paso de tiempo, las células cambian de estado
según reglas simples basadas en la cantidad de células vecinas vivas.

Las reglas son:

* Supervivencia: Una célula viva sobrevive si tiene 2 o 3 vecinos vivos.
* Muerte por soledad: Una célula viva muere si tiene menos de 2 vecinos vivos.
* Muerte por superpoblación: Una célula viva muere si tiene más de 3 vecinos
  vivos.
* Nacimiento: Una célula muerta nace si tiene exactamente 3 vecinos vivos.

## Programa
<div style="display: flex; align-items: center;">
  <a href="https://dev.java">
    <img src="https://dev.java/assets/images/java-affinity-logo-icode-lg.png" alt="Java Affinity Logo" width="70px" />
  </a>
  <p style="margin-left: 10px;">Este trabajo se programó con Java 17.0.7</p>
</div>
Para la ejecución, use alguno de los scripts proporcionados

###### Notas
Debería funcionar con Java 8

### Ejemplo de salida
![Spceship 295P5H1V1 en la iteración 152](./out/Spaceship%20(Generación%20152).png)
