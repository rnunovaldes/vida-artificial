#!/bin/bash
# include this boilerplate
function jumpto {
  label=$1
  cmd=$(sed -n "/$label:/{:a;n;p;ba};" $0 | grep -v ':$')
  eval "$cmd"
  exit
}

function pause {
 read -s -n 1 -p "Presione alguna tecla para continuar . . ."
 echo ""
}

start=${1:-"start"}

jumpto $start

start:
set -- $(locale LC_MESSAGES)
yesexpr="$1"; noexpr="$2"; yesword="$3"; noword="$4"

EXEC=main.Principal
JAVA_PRIN=./src/main/Principal.java
CLAS_PRIN=./bin/main/Principal.class
CLAS_VIEW1=./bin/mvc/view/GridComponent.class
CLAS_VIEW2=./bin/mvc/view/Window.class
CLAS_CON1=./bin/mvc/controller/Constants.class
CLAS_CON2=./bin/mvc/controller/WindowManager.class
CLAS_MOD1=./bin/mvc/model/Model.class
CLAS_MOD2=./bin/mvc/model/Rules.class
CLAS_UTIL1=./bin/utils/fileManager/GOLFilter.class
CLAS_UTIL2=./bin/utils/fileManager/Utils.class

while true; do
  read -p "¿Compilar? (${yesword} / ${noword})? " yn
  if [[ "$yn" =~ $yesexpr ]]; then jumpto comp; fi
  if [[ "$yn" =~ $noexpr ]]; then jumpto cont; fi
  echo "Answer ${yesword} / ${noword}."
done

cont:
if [[ -e "$CLAS_PRIN" ]] && [[ -e "$CLAS_VIEW1" ]] && [[ -e "$CLAS_VIEW2" ]]; then
if [[ -e "$CLAS_CON1" ]] && [[ -e "$CLAS_CON2" ]] && [[ -e "$CLAS_MOD1" ]]; then
if [[ -e "$CLAS_MOD2" ]] && [[ -e "$CLAS_UTIL1" ]] && [[ -e "$CLAS_UTIL2" ]]; then
jumpto exe; fi fi fi

comp:
if [[ ! -d ./bin ]]; then mkdir bin; fi
echo Vamos a compilar
javac -encoding UTF-8 -d bin -cp ..:src "$JAVA_PRIN"

exe:
echo Vamos a ejecutar el compilado
java -cp bin "$EXEC"
pause
echo Se eliminará la carpeta con los class
rm -rI ./bin
exit
