package mvc.view;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.BitSet;

import javax.swing.JPanel;

import mvc.controller.Constants;

/**
 * Componente para mostrar una rejilla interactuable del juego de la
 * vida donde cada celda es una célula viva o muerta
 */
public class GridComponent extends JPanel implements MouseListener, MouseMotionListener {
    private BitSet[] grid = new BitSet[1];
    private int width, height;

    /**
     * Contructor de la clase
     */
    public GridComponent() {
        addMouseListener(this);
        addMouseMotionListener(this);
    }

    @Override
    public void setBounds(int x, int y, int width, int height) {
        super.setBounds(x, y, width, height);

        Constants.cells_width = (width/Constants.CELL_SIZE) - 2;
        Constants.cells_height = (height/Constants.CELL_SIZE) - 2;
        grid = new BitSet[Constants.cells_width];
        for(int i = 0; i < Constants.cells_width; i++) {
            grid[i] = new BitSet(Constants.cells_height);
        }
        this.width = getWidth();
        this.height = getHeight();
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        int offsetw = Constants.CELL_SIZE + (width % Constants.CELL_SIZE);
        int offseth = Constants.CELL_SIZE + (height % Constants.CELL_SIZE);

        for(int row = 0; row < Constants.cells_width; row++) {
            for(int col = 0; col < Constants.cells_height; col++) {
                if(grid[row].get(col)) {
                    g.setColor(Color.BLACK);
                } else {
                    g.setColor(Color.WHITE);
                }
                g.fillRect(Constants.CELL_SIZE + row * Constants.CELL_SIZE,
                           Constants.CELL_SIZE +  col * Constants.CELL_SIZE,
                           Constants.CELL_SIZE, Constants.CELL_SIZE);
            }
        }

        for(int i = Constants.CELL_SIZE; i <= width-Constants.CELL_SIZE; i += Constants.CELL_SIZE) {
            if((i - Constants.CELL_SIZE) % (Constants.CELL_SIZE * 10) == 0) {
                g.setColor(Color.GRAY);
            } else {
                g.setColor(Color.LIGHT_GRAY);
            }
            g.drawLine(i, Constants.CELL_SIZE, i, height - offseth);
        }
        for(int i = Constants.CELL_SIZE; i <= height-Constants.CELL_SIZE; i += Constants.CELL_SIZE) {
            if((i - Constants.CELL_SIZE) % (10 * Constants.CELL_SIZE) == 0) {
                g.setColor(Color.GRAY);
            } else {
                g.setColor(Color.LIGHT_GRAY);
            }
            g.drawLine(Constants.CELL_SIZE, i, width - offsetw, i);
        }
    }

    /**
     * Sirve para cambiar el color de un punto donde se ha clicado al
     * contrario que tenía antes de clicarlo
     *
     * @param me El MouseEvent con las coordenadas de donde se clicó
     */
    public void changePoint(MouseEvent me) {
        int x = me.getPoint().x/Constants.CELL_SIZE-1;
        int y = me.getPoint().y/Constants.CELL_SIZE-1;
        if(x >= 0 && x < Constants.cells_width && y >= 0 && y < Constants.cells_height) {
            grid[x].set(y, !grid[x].get(y));
        }
        repaint(Constants.CELL_SIZE + x * Constants.CELL_SIZE,
                Constants.CELL_SIZE +  y * Constants.CELL_SIZE,
                Constants.CELL_SIZE, Constants.CELL_SIZE);
    }

    /**
     * Asigna un nuevo tablero para la simulación
     *
     * @param newGrid Un arreglo de BitSet para sustituir el actual
     */
    public void setBoard(BitSet[] newGrid) {
        grid = newGrid;
        repaint();
    }

    /**
     * Reinicia el tablero a uno vacío
     */
    public void resetBoard() {
        grid = new BitSet[Constants.cells_width];
        for(int i = 0; i < Constants.cells_width; i++) {
            grid[i] = new BitSet(Constants.cells_height);
        }
        repaint();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        changePoint(e);
    }
    @Override
    public void mouseDragged(MouseEvent e) {
        changePoint(e);
    }

    @Override
    public void mouseClicked(MouseEvent e) {}
    @Override
    public void mousePressed(MouseEvent e) {}
    @Override
    public void mouseEntered(MouseEvent e) {}
    @Override
    public void mouseExited(MouseEvent e) {}
    @Override
    public void mouseMoved(MouseEvent e) {}

    public BitSet[] getState() {
        return grid;
    }
}
