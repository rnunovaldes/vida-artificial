package mvc.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.BitSet;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileNameExtensionFilter;

import mvc.controller.Constants;
import mvc.controller.WindowManager;
import utils.fileManager.GOLFilter;

/**
 * Clase para mostrar una ventana de la interfaz grafica
 */
public class Window extends JFrame implements ActionListener {

    private final String TITLE = "El Juego de la Vida";
    private final String GEN = "Generación %d";
    private final String[] OPTIONS = {
            /*  0 */"Archivo",
            /*  1 */"Guardar estado",
            /*  2 */"Cargar estado",
            /*  3 */"Guardar imagen",
            /*  4 */"Juego",
            /*  5 */"Iniciar",
            /*  6 */"Detener",
            /*  7 */"Reiniciar",
            /*  8 */"Opciones",
            /*  9 */"Mov/Seg",
            /* 10 */"Topología",
            /* 11 */"Salir",
            /* 12 */"Movimientos por segundo:",
            /* 13 */"Tipo de plano:"
        };
    private final Byte[] secondsOptions = {1,2,3,4,5,10,15,20};
    private final String[] topologyOptions = {
            "Bordes contrarios unidos",
            "Bordes como celulas muertas"
        };

    private int gen = 0;

    private JMenu menu1, menu2, menu3;
    private JMenuItem sm1_i1, sm1_i2, sm1_i3, sm2_i1, sm2_i2, sm2_i3, sm3_i1, sm3_i2;
    private JButton exitButton;
    private JFileChooser fcLoad, fcSave1, fcSave2;
    private GridComponent simGrid;
    private JLabel genLabel;
    private JPanel sharedZone;
    private JButton playButton;
    private ImageIcon icon, pauseIcon, playIcon;
    private boolean isPaused = true;

    private final File OUTFILE = new File(Constants.OUT);

    protected WindowManager wm;

    /**
     * Constructor de la clase donde se inicializan todos los componentes
     * de la ventana principal
     *
     * @param wm El coordinador entre las ventanas y la logica
     */
    public Window(WindowManager wm) {
        this.wm = wm;
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        // setSize(480, 480);
        setTitle(TITLE);
        // Centrar horizontalmente
        setUndecorated(true);
        setResizable(false);
        setLayout(new BorderLayout());
        setSize(new Dimension(
                Constants.SCREEN_WIDTH-100, Constants.SCREEN_HEIGHT-80
            ));
        setMinimumSize(new Dimension(650, 550));
        /*for ( java.awt.Window w : Window.getWindows() ) {
            GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().setFullScreenWindow( w );
        }*/
        int centerX = (Constants.SCREEN_WIDTH - getWidth()) / 2;
        setLocation(centerX, 0);
        icon = new ImageIcon(Constants.RES + Constants.SEP + "game-of-life.png");
        setIconImage(icon.getImage());

        /* Pestaña de Archivo */
        JMenuBar mb = new JMenuBar();
        menu1 = new JMenu(OPTIONS[0]);
        sm1_i1 = new JMenuItem(OPTIONS[1]);
        sm1_i2 = new JMenuItem(OPTIONS[2]);
        sm1_i3 = new JMenuItem(OPTIONS[3]);
        menu1.add(sm1_i1);
        menu1.add(sm1_i2);
        menu1.add(new JSeparator());
        menu1.add(sm1_i3);

        /* Pestaña de Juego */
        menu2 = new JMenu(OPTIONS[4]);
        sm2_i1 = new JMenuItem(OPTIONS[5]);
        sm2_i2 = new JMenuItem(OPTIONS[6]);
        sm2_i3 = new JMenuItem(OPTIONS[7]);
        menu2.add(sm2_i1);
        menu2.add(sm2_i2);
        menu2.add(new JSeparator());
        menu2.add(sm2_i3);
        sm2_i2.setEnabled(false);

        /* Pestaña Opciones */
        menu3 = new JMenu(OPTIONS[8]);
        sm3_i1 = new JMenuItem(OPTIONS[9]);
        sm3_i2 = new JMenuItem(OPTIONS[10]);
        menu3.add(sm3_i1);
        menu3.add(sm3_i2);

        exitButton = new JButton(OPTIONS[11]);
        exitButton.setBorderPainted(false);
        exitButton.setContentAreaFilled(false);

        mb.add(menu1);
        mb.add(menu2);
        mb.add(menu3);
        mb.add(exitButton);
        setJMenuBar(mb);

        fcLoad = new JFileChooser();
        fcLoad.setCurrentDirectory(OUTFILE);
        // Add a custom file filter and disable the default
        // (Accept All) file filter.
        fcLoad.addChoosableFileFilter(new GOLFilter());
        fcLoad.setAcceptAllFileFilterUsed(false);
        // Add custom icons for file types.
        // fcEscoger.setFileView(new RuleFileView());
        /* File chooser para pestanya Archivo Guardar */
        fcSave1 = new JFileChooser();
        fcSave1.setCurrentDirectory(OUTFILE);
        fcSave1.setDialogType(JFileChooser.SAVE_DIALOG);
        fcSave1.setFileFilter(new FileNameExtensionFilter("gol file","gol"));
        fcSave2 = new JFileChooser();
        fcSave2.setCurrentDirectory(OUTFILE);
        fcSave2.setDialogType(JFileChooser.SAVE_DIALOG);
        fcSave2.setFileFilter(new FileNameExtensionFilter("png file","png"));
        genLabel = new JLabel(String.format(GEN, gen));

        /* Botón de pausa */
        pauseIcon = new ImageIcon(Constants.RES + Constants.SEP + "pause-icon.png");
        Image image = pauseIcon.getImage();
        Image newImg = image.getScaledInstance(24, 24,  java.awt.Image.SCALE_SMOOTH);
        pauseIcon = new ImageIcon(newImg);
        /* Botón de play */
        playIcon = new ImageIcon(Constants.RES + Constants.SEP + "play-icon.png");
        image = playIcon.getImage();
        newImg = image.getScaledInstance(24, 24,  java.awt.Image.SCALE_SMOOTH);
        playIcon = new ImageIcon(newImg);
        playButton = new JButton(playIcon);
        playButton.setVisible(true);
        playButton.setBorderPainted(false);
        playButton.setFocusPainted(false);
        playButton.setContentAreaFilled(false);

        sharedZone = new JPanel(new FlowLayout());
        sharedZone.add(playButton);
        sharedZone.add(genLabel);
        sharedZone.setPreferredSize(new Dimension(getWidth(), 35));
        add(sharedZone, BorderLayout.NORTH);

        /* Simulación */
        simGrid = new GridComponent();
        simGrid.setPreferredSize(new Dimension(getWidth(), getHeight()-35));
        add(simGrid, BorderLayout.CENTER);

        pack();
        setVisible(true);

        /* Listeners */
        sm1_i1.addActionListener(this);
        sm1_i2.addActionListener(this);
        sm1_i3.addActionListener(this);
        sm2_i1.addActionListener(this);
        sm2_i2.addActionListener(this);
        sm2_i3.addActionListener(this);
        sm3_i1.addActionListener(this);
        sm3_i2.addActionListener(this);
        exitButton.addActionListener(this);
        playButton.addActionListener(this);
    }

    /**
     * Hace que su gridComponent pinte donde hay celulas vivas según el
     * modelo
     *
     * @param bs Dónde es que deben pintar los cuadrados del
     *           gridComponent
     */
    public void paintNextGen(BitSet[] bs) {
        genLabel.setText(String.format(GEN, ++gen));
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                simGrid.setBoard(bs);

            }
        });
    }


    /**
     * Alterna el estado de la ventana entre pausado y corriendo,
     * activando y desactivando varias opciones del usuario
     * respectivamente, además de altenar entre los símbolos del botón
     * de inicio y pausa
     */
    private void toggleSimulationState() {
        isPaused = !isPaused;
        if(!isPaused) {
            sm2_i1.setEnabled(false);
            sm1_i1.setEnabled(false);
            sm1_i2.setEnabled(false);
            sm1_i3.setEnabled(false);
            sm3_i2.setEnabled(false);
            sm2_i2.setEnabled(true);

            playButton.setIcon(pauseIcon);
        } else {
            sm2_i2.setEnabled(false);
            sm2_i1.setEnabled(true);
            sm1_i1.setEnabled(true);
            sm1_i2.setEnabled(true);
            sm1_i3.setEnabled(true);
            sm3_i2.setEnabled(true);

            playButton.setIcon(playIcon);
        }
    }

    /**
     * Método que recibe las aciones realizadas en la ventana
     *
     * @param e El <code>ActionEvent</code> registrado
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == sm1_i1) {
            /* Save state */
            if(fcSave1.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                String filename = fcSave1.getSelectedFile().getAbsolutePath();
                if(!filename.toLowerCase().endsWith(".gol")) filename += ".gol";
                wm.sendForSerialization(filename, simGrid.getState());
            }
        }
        else if(e.getSource() == sm1_i2) {
            /* Load state */
            gen = 0;
            genLabel.setText(String.format(GEN, gen));
            int resp = fcLoad.showOpenDialog(null);
            if(resp == JFileChooser.APPROVE_OPTION) {
                String statePath = fcLoad.getSelectedFile().getAbsolutePath();
                simGrid.setBoard(wm.sendForDeserialization(statePath));
            }
        }
        else if(e.getSource() == sm1_i3) {
            /* Save image */
            File defaultName = new File(String.format(GEN, gen));
            fcSave2.setSelectedFile(defaultName);
            if(fcSave2.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                String filename = fcSave2.getSelectedFile().getAbsolutePath();
                if(!filename.toLowerCase().endsWith(".png")) filename += ".png";
                wm.saveAsImage(filename, simGrid.getState());
            }
        }
        else if(e.getSource() == sm2_i1 || (e.getSource() == playButton && isPaused)) {
            /* Play */
            toggleSimulationState();
            wm.startSimulation(simGrid.getState());
        }
        else if(e.getSource() == sm2_i2 || (e.getSource() == playButton && !isPaused)) {
            /* Pause */
            toggleSimulationState();
            wm.stopSimulation();
        }
        else if(e.getSource() == sm2_i3) {
            /* Reset */
            isPaused = false;
            toggleSimulationState();
            wm.stopSimulation();
            gen = 0;
            genLabel.setText(String.format(GEN, gen));
            simGrid.resetBoard();
        }
        else if(e.getSource() == sm3_i1) {
            final JFrame f_options = new JFrame();
            f_options.setTitle(OPTIONS[8]);
            f_options.setUndecorated(true);
            f_options.setSize(300,40);
            f_options.setLocation((Constants.SCREEN_WIDTH - f_options.getWidth())/2,
                    (Constants.SCREEN_HEIGHT - f_options.getHeight())/2);
            f_options.setResizable(false);
            JPanel p_options = new JPanel();
            p_options.setOpaque(false);
            f_options.add(p_options);

            p_options.add(new JLabel(OPTIONS[12]));

            final JComboBox<Byte> cb_seconds = new JComboBox<>(secondsOptions);
            p_options.add(cb_seconds);
            cb_seconds.setSelectedItem(Constants.moves_per_second);
            cb_seconds.addActionListener(new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent ae) {
                    Constants.moves_per_second = (Byte)cb_seconds.getSelectedItem();
                    f_options.dispose();
                }
            });
            f_options.setVisible(true);
        }
        else if(e.getSource() == sm3_i2) {
            final JFrame f_options = new JFrame();
            f_options.setTitle(OPTIONS[8]);
            f_options.setUndecorated(true);
            f_options.setSize(300,40);
            f_options.setLocation((Constants.SCREEN_WIDTH - f_options.getWidth())/2,
                    (Constants.SCREEN_HEIGHT - f_options.getHeight())/2);
            f_options.setResizable(false);
            JPanel p_options = new JPanel();
            p_options.setOpaque(false);
            f_options.add(p_options);

            p_options.add(new JLabel(OPTIONS[13]));
            final JComboBox<String> cb_topology = new JComboBox<>(topologyOptions);
            p_options.add(cb_topology);
            cb_topology.setSelectedItem(cb_topology.getItemAt(Constants.topology));
            cb_topology.addActionListener(new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent ae) {
                    Constants.topology = (byte)cb_topology.getSelectedIndex();
                    f_options.dispose();
                }
            });
            f_options.setVisible(true);
        }
        else if(e.getSource() == exitButton) {
            System.exit(0);
        }
    }
}
