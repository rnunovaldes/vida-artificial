package mvc.model;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.BitSet;

import javax.imageio.ImageIO;

import mvc.controller.Constants;
import mvc.controller.WindowManager;

/**
 * Clase que se encarga de pedir la serialización y deserialización de
 * reglas, además de solicitar ejecuciones de las reglas
 */
public class Model {
    private WindowManager wm;
    private Rules srules;

    /**
     * Constructor de la clase
     *
     * @param wm El coordinador entre las ventanas y la lógica
     */
    public Model(WindowManager wm) {
        this.wm = wm;

    }

    /**
     * Asigna la primera generación de la simulación
     *
     * @param fgen La generación a asignar como la primera
     */
    public void setFirstGen(BitSet[] fgen) {
        srules = new Rules(fgen);
    }

    /**
     * Genera el siguiente estado correspondiente
     *
     * @return Si se completó
     */
    public boolean doSimulation() {
        srules.nextGen();
        wm.sendNextGen(srules.getState());
        try {
            Thread.sleep(1000/Constants.moves_per_second);
        } catch(InterruptedException e) {
            e.printStackTrace();
        }
        return true;
    }

    /**
     * Método para deserializar una Rules nueva.
     *
     * @param rule Son los valores de la regla que se han puesto en la IU
     * @return Si se pudo guardar la regla correctamente
     */
    public boolean serializateState(String filename, BitSet[] state) {
        try {
            File file = new File(filename);
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));

            for(int col = 0; col < Constants.cells_width; col++) {
                for(int row = 0; row < Constants.cells_height; row++) {
                    if(state[row].get(col)) {
                       writer.write("1");
                    } else {
                        writer.write(".");
                    }
                }
                if(col != Constants.cells_height-1) writer.newLine();
            }
            writer.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Método para deserializar Rules. Luego envía lo necesario para
     * mostrar qué se hizo en la GUI
     *
     * @param golPath La ruta del archivo
     * @return Los valores a mostrar en la IU para el estado
     *         correspondiente
     */
    public BitSet[] deserializateState(String golPath) {
        try {
            File file = new File(golPath);
            BufferedReader reader = new BufferedReader(new FileReader(file));

            String line;
            BitSet[] newState = new BitSet[Constants.cells_width];
            for(int i = 0; i < Constants.cells_width; i++) {
                newState[i] = new BitSet(Constants.cells_height);
            }
            int i = 0;
            while((line = reader.readLine()) != null) {
                for(int j = 0; j < line.length(); j++) {
                    if(j == Constants.cells_height) break;
                    if(line.charAt(j) == '1') newState[j].set(i);
                }
                i++;
                if(i == Constants.cells_height) break;
            }
            srules = new Rules(newState);
            reader.close();
            return srules.getState();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new BitSet[1];
    }

    /**
     * Método para crear un archivo en formato png en una ruta dada por
     * un objeto File
     *
     * @param fimg El File con la ruta y nombre de la nueva imagen
     * @param image Los datos de la imágen a guaradar
     */
    public void saveAsImage(String fimg, BitSet[] bimage) {
        try {
            File file = new File(fimg);
            BufferedImage image = generateBufferedImage(bimage);
            ImageIO.write(image, "png", file);
        } catch(IOException e) {
            String workingDir = System.getProperty("user.dir");
            System.out.println("Current working directory : " + workingDir);
            e.printStackTrace();
        }
    }

    /**
     * Método para generar un BufferedImage a partir de una matriz de Color
     *
     * @param pixeles Una matriz de booleanos donde los trues serán
     *                pintados de negro y false de blanco en el
     *                BufferedImage
     * @return La imagen en formato de BufferedImage
     */
    private static BufferedImage generateBufferedImage(BitSet[] pixeles) {
        BufferedImage nImage = new BufferedImage(Constants.cells_width * Constants.CELL_SIZE,
            Constants.cells_height * Constants.CELL_SIZE, BufferedImage.TYPE_INT_RGB);
        // Set each pixel of the BufferedImage to the color from the Color[][].
        for(int i = 0; i < Constants.cells_height; i++) {
            for(int j = 0; j < Constants.cells_width; j++) {
                int fixedi = i * Constants.CELL_SIZE;
                int fixedj = j * Constants.CELL_SIZE;
                int color = pixeles[j].get(i)? 0: 0xFFFFFF;
                for (byte y = 0; y < Constants.CELL_SIZE; y++) {
                    for (byte x = 0; x < Constants.CELL_SIZE; x++) {
                        nImage.setRGB(fixedj + x, fixedi + y, color);
                    }
                }
            }
        }
        return nImage;
    }
}
