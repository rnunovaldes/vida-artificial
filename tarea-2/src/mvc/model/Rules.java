package mvc.model;

import java.awt.Point;
import java.io.Serializable;
import java.util.BitSet;
import java.util.HashSet;
import java.util.Set;

import mvc.controller.Constants;

/**
 * Clase que modela las reglas a seguir para el juego de la vida y puede
 * obtener la generación siguiente a una indicada como actual
 */
public class Rules implements Serializable {

    private BitSet[] currentState;
    private Set<Point> cellsToUpdate = new HashSet<>();

    /**
     * Constructor de <code>Rules</code>
     *
     * @param currentState El estado de las células inicialmente
     */
    public Rules(BitSet[] currentState) {
        this.currentState = currentState;

        for (int row = 0; row < Constants.cells_width; row++) {
            for (int col = 0; col < Constants.cells_height; col++) {
                if(currentState[row].get(col)) {
                    updateSet(row, col, cellsToUpdate);
                }
            }
        }
    }

    /**
     * Dado un <code>Set</code> de puntos relevantes para el tablero,
     * agrega la ubicación de la célula y sus vecinos para ser los
     * únicos tomados en cuenta al momento de actualizar las células
     * vivas y muertas
     *
     * @param row La fila en la que está la célula relevante
     * @param col La columna donde está
     * @param updateList La lista donde se están guardando estas
     *                   ubicaciones
     */
    private void updateSet(int row, int col, Set<Point> updateList) {
        updateList.add(new Point(row,col));
        for(int i = -1; i <= 1; i++) {
            for(int j = -1; j <= 1; j++) {
                int neighborX = row + i;
                int neighborY = col + j;

                if(Constants.topology == 0) {
                    if(neighborX == -1) {
                        neighborX = Constants.cells_width - 1;
                    } else if(neighborX == Constants.cells_width) {
                        neighborX = 0;
                    }
                    if(neighborY == -1) {
                        neighborY = Constants.cells_height - 1;
                    } else if(neighborY == Constants.cells_height) {
                        neighborY = 0;
                    }
                } else {
                    if(neighborX < 0 || neighborX > Constants.cells_width-1
                        || neighborY < 0 || neighborY > Constants.cells_height-1) {
                            continue;
                        }
                }
                updateList.add(new Point(neighborX, neighborY));
            }
        }
    }

    /**
     * Cuenta la cantidad de celulas vivas en la vencindad de una célula
     * en la posición indicada
     *
     * @param row La fila en donde está la célula
     * @param col La columna en donde está la célula
     * @return
     */
    private byte countAliveNeighbors(int row, int col) {
        byte aliveNeighbors = 0;

        for(int i = -1; i <= 1; i++) {
            for(int j = -1; j <= 1; j++) {
                if (i == 0 && j == 0) continue;

                int neighborX = row + i;
                int neighborY = col + j;
                if(Constants.topology == 0) {
                    if(neighborX == -1) neighborX = Constants.cells_width - 1;
                    if(neighborX == Constants.cells_width) neighborX = 0;
                    if(neighborY == -1) neighborY = Constants.cells_height - 1;
                    if(neighborY == Constants.cells_height) neighborY = 0;
                    if(currentState[neighborX].get(neighborY)) aliveNeighbors++;
                } else {
                    if(neighborX > -1 && neighborX < Constants.cells_width
                        && neighborY > -1 && neighborY < Constants.cells_height
                        && currentState[neighborX].get(neighborY)) {
                            aliveNeighbors++;
                        }
                }
            }
        }
        return aliveNeighbors;
    }

    /**
     * Genera la siguiente generación según la el estado actual de las
     * células y actualiza este al correspondiente
     */
    public void nextGen() {
        BitSet[] nextState = new BitSet[Constants.cells_width];
        for(int i = 0; i < Constants.cells_width;i++) {
            nextState[i] = new BitSet(Constants.cells_height);
        }

        Set<Point> newUpdateList = new HashSet<>();
        for(Point p: cellsToUpdate) {
            int row = (int)p.getX();
            int col = (int)p.getY();
            byte aliveNeighbors = countAliveNeighbors(row,col);
            if(currentState[row].get(col)) {
                    if(aliveNeighbors == 2 || aliveNeighbors == 3) {
                        nextState[row].set(col);
                        updateSet(row, col, newUpdateList);
                    }
                } else {
                    if(aliveNeighbors == 3) {
                        nextState[row].set(col);
                        updateSet(row, col, newUpdateList);
                    }
                }
        }
        cellsToUpdate = newUpdateList;
        currentState = nextState;
    }

    /**
     * Obtiene la matriz con el estado actual de las células
     *
     * @return Una matriz con valores booleanos donde true es que están
     *         vivas las células y false que no
     */
    public BitSet[] getState() {
        return currentState;
    }
}
