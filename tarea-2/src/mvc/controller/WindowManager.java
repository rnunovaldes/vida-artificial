package mvc.controller;

import mvc.view.Window;

import java.util.BitSet;

import mvc.model.Model;

/**
 * Clase que coordina las ventanas y la lógica del proyecto
 */
public class WindowManager {
    private Window window;
    private Model model;
    private boolean isSimulationRunning;

    /**
     * Constructor de la clase
     */
    public WindowManager() {
        window = new Window(this);
        model = new Model(this);
    }

    /**
     * Método que envía los valores de un estado para ser guardado
     * en un archivo
     *
     * @param state Los valores de la regla a guardar
     * @return Si se guardó correctamente
     */
    public boolean sendForSerialization(String filename, BitSet[] state) {
        return model.serializateState(filename, state);
    }

    /**
     * Método que envía la ruta del archivo con el estado inicial al
     * modelo y regresa los valores a mostrar en la IU
     *
     * @param statePath La ruta del archivo con el estado
     * @return Los valores a mostrar en la IU para el estado
     *         correspondiente
     */
    public BitSet[] sendForDeserialization(String statePath) {
        return model.deserializateState(statePath);
    }

    /**
     * Método que envía los valores de la primera generación a usar
     * en la simulación al modelo y empieza la simulación
     *
     * @param fgen Los valores de la primera generación
     */
    public void startSimulation(BitSet[] fgen) {
        model.setFirstGen(fgen);
        //window.disableMenuItems();
        if(!isSimulationRunning) {
            isSimulationRunning = true;
            Thread simulationThread = new Thread(() -> {
                while(isSimulationRunning) {
                    model.doSimulation();
                //window.enableMenuItems();
                }
            });
            simulationThread.start();
        }
    }

    public void stopSimulation() {
        isSimulationRunning = false;
    }

    /**
     * Envía los valores de una generación resultante de la simulación
     * a la IU
     *
     * @param nextGen La generación nueva que debe ser mostrada
     */
    public void sendNextGen(BitSet[] nextGen) {
        window.paintNextGen(nextGen);
    }

    /**
     * Envía la ruta en la que se va a guardar la imagen del estado
     * representado en la IU
     *
     * @param path Ruta donde se guarda la imagen
     */
    public void saveAsImage(String path, BitSet[] gen) {
        model.saveAsImage(path, gen);
    }
}
