/**
 * @typedef {Object} Action
 * @property {!string} cmd - El comando a realizar
 * @property {number} [arg] - El argumento asociado al comando
 *
 * @description Un objeto que representa una acción a realizar según un
 *              símbolo
 */

/**
 * @typedef {Object.<string, Action[]>} Actions
 * @property {!string} key - El caracter asociado a las acciones
 * @property {!Action[]} actions - Valor asociado al caracter
 *           correspondiente, que es una lista con las acciones a hacer
 *
 * @description Un objeto que contiene pares clave-valor donde las
 *              claves son una carater que identifica a la variable
 *              a sustituir y los valores una lista de acciones que se
 *              deben hacer cuando se encuatre esta clave en una cadena
 *
 * @summary Las acciones a realizar según el símbolo
 */

/**
 * Espacio donde se escribe la coordenada x
 * @type {!HTMLSpanElement}
 */
const startX = document.getElementById('start-x');
/**
 * Espacio donde se escribe la coordenada y
 * @type {!HTMLSpanElement}
 */
const startY = document.getElementById('start-y');
/**
 * El contenedor del lienzo. Usado para obtenes sus medidas
 * @type {!HTMLDivElement}
 */
const leftPanel = document.getElementById('left-panel');
/**
 * El lienzo donde la tortuga va a pintar
 * @type {!HTMLCanvasElement}
 */
const canvas = document.getElementById('turtle-canvas');
/**
 * @type {!CanvasRenderingContext2D}
 */
const ctx = canvas.getContext('2d');

/**
 * Botón para iniciar la ejecución
 * @type {!HTMLButtonElement}
 */
const playButton = document.getElementById('play-button');
/**
 * Botón para pausar la ejecución
 * @type {!HTMLButtonElement}
 */
const pauseButton = document.getElementById('pause-button');
/**
 * Botón para reiniciar el lienzo
 * @type {!HTMLButtonElement}
 */
const clearButton = document.getElementById('clear-button');

let initialX = canvas.width / 2;
let initialY = canvas.height / 2;
let turtle = new Turtle(ctx, initialX, initialY);
const animationSpeed = 100; // mayor número = más rápido
let animationRunning = false;
let step = 0;
let string = '';
/**
 * @type {Actions}
 */
let actions = undefined;

/**
 * @param {Action[]} actionList - La lista de acciones a realizar
 *
 * @description Ejecuta las acciones secuencialmente en la lista
 *              proporcionada en el lienzo. Se encarga de dibujar el
 *              resultado de cada acción en el lienzo uno por uno
 */
function drawNextStep(actionList) {
    let actionStep = 0;
    /**
     * @description Se encarga de verificar si todavía hay acciones
     *              pendientes en la lista y si hay una acción
     *              disponible, ejecuta la acción correspondiente y
     *              avanza al siguiente paso.
     */
    function executeNextStep() {
        while(actionStep < actionList.length) {
            let comando = actionList[actionStep];
            turtle[comando.cmd](comando.arg);
            actionStep++;
        }
    }
    executeNextStep();
}

/**
 * @description Se encarga de iniciar la animación del sistema L,
 *              generando la cadena resultante y animando paso a paso
 *              de acuerdo a las acciones definidas para cada símbolo
 *              en la cadena. Habilita/deshabilita los botones
 *              correspondientes durante la animación y puede usarse
 *              para reanudar el dibujo si este se pausó anteriormente
 *
 * @summary Inicia la animación del sistema L
 */
function startDrawing() {
    playButton.disabled = true;
    pauseButton.disabled = false;
    clearButton.disabled = true;

    if(step === 0) {
        let lSystem = getLSystem();
        actions = getActions();
        let iterations = parseInt(
            document.getElementById('iterations').value, 10
        );
        lSystem.iterate(iterations);
        string = lSystem.getResult();

        ctx.clearRect(0, 0, canvas.width, canvas.height);
        turtle.x = initialX;
        turtle.y = initialY;
        turtle.angle = 0;
    }

    /**
     * @description Se encarga de ejecutar el siguiente paso en la
     *              animación del sistema L según una velocidad dada.
     *              Se llama recursivamente para avanzar paso a paso
     */
    function animate() {
        for(let i = 0; i < animationSpeed; i++) {
            if(step < string.length) {
                drawNextStep(actions[string.charAt(step)]);
                step++;
            } else break;
        }
        if(step < string.length && animationRunning) {
            requestAnimationFrame(animate);
        } else {
            animationRunning = false;
            pauseButton.disabled = true;
            clearButton.disabled = false;
            if(step === string.length) {
                step = 0;
            }
        }
    }

    animationRunning = true;
    animate();
}

/**
 * @description Detiene la animación del dibujo del sistema L,
 *              conservando el paso en el que se quedó, los axiomas y
 *              acciones establecidos. Habilita/deshabilita los botones
 *              correspondientes
 */
function stopDrawing() {
    animationRunning = false;

    playButton.disabled = false;
    pauseButton.disabled = true;
    clearButton.disabled = false;
}

/**
 * @description Permite o no el uso del botón de play si el axioma es
 *              correcto
 */
function allowPlayButton() {
    let inputValue = pattern.value;
    inputValue = inputValue.replace(/[+\-\[\]]/g, '');
    if(inputValue === '') playButton.disabled = true;
    for(let char of inputValue) {
        if(!usedSymbols.has(char)) {
            playButton.disabled = true;
            break;
        }
        playButton.disabled = false;
    }
}

/**
 * @description Detiene la animación del dibujo del sistema L,
 *              reiniciando el dibujo, pasos, aximas y acciones.
 *              Habilita/deshabilita los botones correspondientes
 */
function restartDrawing() {
    animationRunning = false;

    allowPlayButton();
    pauseButton.disabled = true;
    clearButton.disabled = true;

    ctx.clearRect(0, 0, canvas.width, canvas.height);
    turtle.x = initialX;
    turtle.y = initialY;
    turtle.angle = 0;
    step = 0;
}

/**
 * Hace chequeos para que la cadena inicial sea posible, y si no lo es
 * deshabilita el botón de play
 */
pattern.addEventListener('input', function() {
    pattern.value = pattern.value.toUpperCase();
    let inputValue = pattern.value;
    if(!regexWith.test(inputValue))
        pattern.value = inputValue.replace(/[^A-Z+\-\[\]]/g, '');
    if(animationRunning || !clearButton.disabled) return;
    allowPlayButton();
});

/**
 * @returns {!LSystem} Una instancia de un sistema L
 *
 * @description Genera un objeto LSystem para generar una cadena de
 *              acuerdo a las reglas puestas actualmente en el html
 */
function getLSystem() {
    /**
     * El valor del axioma escrito en el html
     * @type {string}
     */
    let axiom = document.getElementById('pattern').value;
    if(axiom === '') return;

    /**
     * Una lista con todos los elementos que muestran las reglas que
     * vienen actualmente en el html
     * @type {!NodeListOf.<HTMLDivElement>}
     */
    let rulesDiv = document.querySelectorAll('.rule');

    /**
     * Una lista con las producciones para el sistema L
     * @type {Productions}
     */
    let productions = {};
    rulesDiv.forEach((rule) => {
        /**
         * @type {!string}
         */
        let replaceText = rule.querySelector('.replace').textContent;
        /**
         * @type {!string}
         */
        let withText = rule.querySelector('.with').textContent;

        productions[replaceText] = withText;
    });
    return new LSystem(axiom, productions);
}

/**
 * @returns {!Actions} Un objeto que contiene las acciones para cada
 *          símbolo
 *
 * @description Analiza el contenido del HTML para recopilar las
 *              acciones que deben realizarse para cada símbolo.
 *              Examina los elementos HTML con la clase "action" y
 *              extrae la información necesaria para definir las
 *              acciones asociadas a cada símbolo
 *
 * @summary Obtiene las acciones a realizar para cada símbolo según el
 *          contenido del HTML.
 */
function getActions() {
    /**
     * Objeto con todos las acciones de acuerdo al símbolo
     *
     * @type {Actions}
     */
    let productions = {}

    /**
     * Una lista con todos los elementos que muestran las acciones para
     * cada símbolo que vienen actualmente en el html
     *
     * @type {!NodeListOf.<HTMLDivElement>}
     */
    let actionsDiv = document.querySelectorAll('.action');
    actionsDiv.forEach((action) => {
        /**
         * @type {!string}
         */
        let symbolText = action.querySelector('.symbol').textContent;
        /**
         * @type {!string}
         */
        let actionText = action.querySelector('.actions').value;

        /**
         * @param {string} str - La cadena que representa el comando
         * @returns {?Action} - Un objeto que representa el comando
         *          estructurado, o `null` si el comando no es válido
         *
         * @description Toma una cadena que representa un comando en
         *              formato "comando(argumento)" y la traduce a un
         *              objeto con propiedades `cmd` (comando) y,
         *              opcionalmente, `arg` (argumento). Verifica si
         *              el comando es válido comparándolo con una lista
         *              de comandos permitidos.
         *              Si el comando es válido, se extrae el comando
         *              y, si existe, el argumento numérico.
         *              Si el argumento es inválido o no está presente,
         *              se devuelve un objeto con solo el comando.
         *              Si el comando no es válido, se devuelve `null`.
         *
         * @summary Traduce un comando en formato de cadena a un objeto
         *          estructurado
         */
        function translateCommand(str) {
            let parts = str.split('(');
            let command = parts[0].trim();
            const allowedCommands = [
                'draw_forward', 'move_forward', 'left', 'right', 'push', 'pop'
            ];

            if(allowedCommands.includes(command)) {
                if(parts.length === 2) {
                    let value = parseInt(parts[1].replace(')', '').trim(), 10);
                    if(!isNaN(value)) return { cmd: command, arg: value };
                }
                return { cmd: command };
            }
            return null;
        }

        let words = actionText.split(/\s+/).filter(word => word !== '');
        let actions = []
        words.forEach((word) => {
            let objt = translateCommand(word);
            if(objt !== null) actions.push(objt);
        });
        productions[symbolText] = actions;
    });
    return productions;
}

canvas.addEventListener('click', (event) => {
    if(!animationRunning) {
        initialX = event.offsetX;
        initialY = event.offsetY;
        startX.innerText = initialX;
        startY.innerText = initialY;
        turtle = new Turtle(ctx, initialX, initialY);
    }
});

/**
 * @description Permite al lienzo cambiar su tamaño de acuerdo al
 *              espacio disponible en el navegador
 */
function resizeCanvas() {
    stopDrawing();
    restartDrawing();
    canvas.width = leftPanel.clientWidth;
    canvas.height = leftPanel.clientHeight;
    initialX = canvas.width / 2;
    initialY = canvas.height / 2;

    startX.innerText = initialX;
    startY.innerText = initialY;
}

resizeCanvas();
window.addEventListener('resize', resizeCanvas);
