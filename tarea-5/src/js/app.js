/*********************
 * Ventana principal *
 *********************/
const regexRemplace = /^[A-Z]*$/;
const regexWith = /^[A-Z+\-\[\]]*$/;
const regexInt = /^(\d)$/;
const regexActions = /^[a-z]+(\(\d+\))?$/

/**
 * Un conjunto con los caracteres usados hasta ahora para ser
 * sustituídas
 * @type {!Set.<string>}
 */
const usedSymbols = new Set();

/**
 * @typedef {Object.<string, number>} Variables
 * @property {!string} key - Cualquier clave en forma de caracter
 * @property {!number} counter - Valor asociado a la clave
 *           correspondiente, equivalente a la cantidad de veces que se
 *           ha intentado agregar este mismo elemento
 *
 * @description Un objeto que contiene pares clave-valor donde las
 *              claves son una carater que identifica a la variable
 *              a sustituir y los valores una la cantidad de veces que
 *              aparecen por regla
 *
 * @summary Conjunto de símbolos para sustituir
 */

/**
 * Un conjunto con los caracteres usados hasta ahora por las reglas
 * @type {Variables}
 */
const usedChars = {};

/**
 * El espacio donde se escribe la cadena inicial
 * @type {!HTMLInputElement}
 */
const pattern = document.getElementById('pattern');
/**
 * El espacio donde se colocan todas las reglas
 * @type {!HTMLDivElement}
 */
const rulesDiv = document.getElementById('rules');
/**
 * La entrada donde se coloca el símbolo que se reemplazará
 * @type {!HTMLInputElement}
 */
const remplaceInput = document.getElementById('replace');
/**
 * La entrada donde se coloca con qué se reemplaza el símbolo
 * @type {!HTMLInputElement}
 */
const withInput = document.getElementById('with');
/**
 * Una lista con todos los elementos que muestran las reglas que vienen
 * en el html por defecto
 * @type {!NodeListOf.<HTMLDivElement>}
 */
const existingRules = document.querySelectorAll('.rule');
/**
 * El espacio donde se colocan todos los símbolos y sus acciones
 * @type {!HTMLDivElement}
 */
const actionsDiv = document.getElementById('actions');

/**
 * @param {!HTMLInputElement} inputElement - Un elemento para escribir
 * @param {number} [i=0] - Selecciona un tipo de caracteres válidos
 *        para el elemento.
 *        0: Solo una letra y la convierte en mayúscula si no lo es;
 *        1: Letras mayúsculas, corchetes y simbolo de suma y resta.
 *           Convierte las letras en mayúsculas si no lo son;
 *        2: Solo números positivos de un dígito, cero y cadena vacía
 *
 * @description Hace que los caracteres escritos en este elemento solo
 *              solo sean caracteres válidos para la aplicación
 */
function validateRuleInput(inputElement, i) {
    let inputValue = inputElement.value;
    if(i === undefined) i = 0;
    switch(i) {
        case 0:
            inputElement.value = inputValue.toUpperCase();
            inputValue = inputElement.value;
            if(!regexRemplace.test(inputValue))
                inputElement.value = inputValue.replace(/[^A-Z]/g, '');
            break;
        case 1:
            inputElement.value = inputValue.toUpperCase();
            inputValue = inputElement.value;
            if(!regexWith.test(inputValue))
                inputElement.value = inputValue.replace(/[^A-Z+\-\[\]]/g, '');
            break;
        case 2:
            if(inputValue === '') inputElement.value = '0';

            else if(!regexInt.test(inputValue)) {
                inputElement.value = inputValue.replace(/[^\d]/g, '');
                inputValue = inputElement.value;
                if(inputValue.charAt(0) === '0' && inputValue.length === 2)
                    inputElement.value = inputValue.substring(1);
                else if(inputValue.length === 2)
                    inputElement.value = inputValue.charAt(0);
            }
            break;
    }
}

/**
 * @param {!HTMLDivElement} rule - Un elemento que muestra una regla
 *
 * @description Muestra las acciones para eliminar y editar una regla
 */
function showActions(rule) {
    rule.querySelector('.options').style.display = 'block';
}

/**
 * @param {!HTMLDivElement} rule - Un elemento que muestra una regla
 *
 * @description Oculta las acciones para eliminar y editar una regla
 */
function hideActions(rule) {
    rule.querySelector('.options').style.display = 'none';
}

/**
 * @param {string} str - Una cadena cualquiera
 * @returns {Array.<string>} Una lista con una sola instancia de cada
 *          caracter empleado en la cadena
 *
 * @description Obtiene todos los carateres úcicos de una cadena dada
 */
function getUniqueChars(str) {
    let uniqueChars = new Set();
    for(let char of str)
        if(char !== '+' && char !== '-' && char !== '[' && char !== ']')
            uniqueChars.add(char);
    return Array.from(uniqueChars);
}

/**
 * @param {!string} char - Un caracter a ser agregado a las acciones
 *        para el sistema L
 *
 * @description Agrega un caracter a las acciones si este no está
 *              incluído en esta lista del html
 */
function addChars(char) {
    if(!usedChars[char]) {
        let actionDiv = document.createElement('div');
        actionDiv.classList.add('action');
        actionDiv.id = char + '-actions';

        actionDiv.innerHTML = '<span class="symbol">'+ char + '</span>';
        actionDiv.innerHTML += '<textarea class="actions" '
            + 'title="Aciones para el símbolo ' + char
            + '"placeholder="draw_forward">draw_forward(3)</textarea>';

        // Cómo se debe ver el elemento a colocar
        /*
        <div class="action" id="F-actions">
            <span class="symbol">]</span>
            <textarea class="actions"
            title="Aciones para el símbolo F"
            placeholder="draw_foward">draw_forward(3)</textarea>
        </div>
        */

        actionsDiv.appendChild(actionDiv);

        usedChars[char] = 1;
    } else usedChars[char]++;
}

/**
 * @param {!string} char - Un caracter a ser eliminado de las acciones
 *        para el sistema L
 *
 * @description Elimina un caracter de las acciones si este no está
 *              incluído en ninguna otra regla
 */
function removeChars(char) {
    if(usedChars[char])
        if(usedChars[char] === 1) {
            let actionDiv = document.getElementById(char + '-actions');
            actionsDiv.removeChild(actionDiv);
            delete usedChars[char];
        } else usedChars[char]--;
}

/**
 * @param {!HTMLDivElement} rule - Un elemento que muestra una regla
 *
 * @description Borra este elemento de las reglas que se muestran
 */
function removeRule(rule) {
    rule.querySelector('.remove').addEventListener('click', () => {
        let replaceText = rule.querySelector('.replace').textContent;
        let withText = rule.querySelector('.with').textContent;

        getUniqueChars(replaceText + withText).forEach((char) => {
            removeChars(char);
        });
        usedSymbols.delete(replaceText);

        rulesDiv.removeChild(rule);
    });
}

/**
 * @param {!HTMLDivElement} rule - Un elemento que muestra una regla
 *
 * @description Borra este elemento de las reglas que se muestran y
 *              pone su contenido en las entradas para escribir una
 *              regla
 */
function editRule(rule) {
    rule.querySelector('.edit').addEventListener('click', () => {
        let replaceText = rule.querySelector('.replace').textContent;
        let withText = rule.querySelector('.with').textContent;

        getUniqueChars(replaceText + withText).forEach((char) => {
            removeChars(char);
        });
        usedSymbols.delete(replaceText);

        remplaceInput.value = rule.querySelector('.replace').textContent;
        withInput.value = rule.querySelector('.with').textContent;
        rulesDiv.removeChild(rule);
    });
}

/**
 * Agrega todo lo necesario para el funcionamiento de las reglas de
 * ejemplo del html
 */
existingRules.forEach((rule) => {
    rule.addEventListener('mouseover', () => showActions(rule));
    rule.addEventListener('mouseout', () => hideActions(rule));
    removeRule(rule);
    editRule(rule);
    hideActions(rule);

    let replaceText = rule.querySelector('.replace').textContent;
    let withText = rule.querySelector('.with').textContent;

    getUniqueChars(replaceText + withText).forEach((char) => {
        addChars(char);
    });
    usedSymbols.add(replaceText);
});

/**
 * @description Agrega como una regla lo que ha sido escrito en la
 *              entrada
 */
function addRule() {
    let symbol = remplaceInput.value;
    let string = withInput.value;

    if(symbol === '' || string === '' || usedSymbols.has(symbol)) return;

    let ruleDiv = document.createElement('div');
    ruleDiv.classList.add('rule');

    let options = document.createElement('div');
    options.classList.add('options');
    options.innerHTML = '<a href="#" class="edit">edit</a>';
    options.innerHTML += '<a href="#" class="remove">remove</a>';

    ruleDiv.appendChild(options);
    ruleDiv.innerHTML += '<span class="replace">' + symbol + '</span>';
    ruleDiv.innerHTML += '<span class="with">' + string + '</span>';

    // Cómo se debe ver el elemento a colocar
    /*
    <div class="rule">
        <div class="options">
            <a href="#" class="edit">edit</a>
            <a href="#" class="remove">remove</a>
        </div>
        <span class="replace">F</span>
        <span class="with">F+A++A-F--FF-A+</span>
    </div>
    */

    rulesDiv.appendChild(ruleDiv);

    ruleDiv.addEventListener('mouseover', () => showActions(ruleDiv));
    ruleDiv.addEventListener('mouseout', () => hideActions(ruleDiv));

    removeRule(ruleDiv);
    editRule(ruleDiv);

    getUniqueChars(symbol + string).forEach((char) => {
        addChars(char);
    });
    usedSymbols.add(symbol);

    remplaceInput.value = '';
    withInput.value = '';
}

/** Permite escribir contenido dentro de una cadena con un formato */
String.prototype.format = function() {
    const args = arguments;
    return this.replace(/{(\d+)}/g, function(match, number) {
        return typeof args[number] !== "undefined" ? args[number] : match;
    });
};

/*****************
 * Ventana modal *
 *****************/
/**
 * La ventana modal
 * @type {!HTMLDivElement}
 */
const modal = document.getElementById('modal-help');
/**
 * El espacio para escribir en la ventana modal
 * @type {!HTMLDivElement}
 */
const modalContent = document.getElementById('modal-content');

/**
 * Un arreglo donde se tienen los contenidos para ser cambiados en la
 * ventana modal
 * @type {!HTMLDivElement[]}
 */
let modalDivs = [];

let modalDiv = document.createElement('div');
modalDiv.innerHTML = '<p>El axioma que se usará para el sistema L.</p>';
modalDiv.innerHTML += '<p>Si esta cadena no tiene ninguna variable que de las '
 + 'reglas entonces el botón de play será inhabilitado hasta que sea posible '
 + 'hacer al menos una iteración del sistema L.</p>';
modalDivs.push(modalDiv);

modalDiv = document.createElement('div');
modalDiv.innerHTML = '<p>En el primer espacio escriba la variable a '
 + 'sustituir, la cual solo puede ser una letra.</p>';
modalDiv.innerHTML += '<p>En el segundo espacio esciba con qué se va a '
 + 'sustituir esta variable. Se pueden escribir letras o los símbolos "+", '
 + '"-", "[" o "]".</p>';
modalDiv.innerHTML += '<p>Cada variable que sea nueva al conjunto de reglas '
 + 'actual añadirá un espacio para definir las acciones a realizar por la '
 + 'tortuga gráfica.</p>';
modalDivs.push(modalDiv);

modalDiv = document.createElement('div');
let format = '<textarea class="actions" title="{0}" readonly>{1}</textarea>';
modalDiv.innerHTML = '<p>Al agregar una nueva regla se buscan las variables '
 + 'nuevas que se agregan con esta y entonces se permite definir las acciones '
 + 'que realizará la tortuga gráfica.</p>';
modalDiv.innerHTML += '<p>Las acciones que puede hacer la tortuga gráfica son '
 + 'las siguientes:</p>';
modalDiv.innerHTML += format.format('Dibuja una línea recta de "x" píxeles '
 + 'hacia la dirección que ve la tortuga. Si "x" no es número entero o no se '
 + 'agregan los paréntesis, avanza 5 píxeles', 'draw_foward(x)');
modalDiv.innerHTML += format.format('Avanza hacia el frente "x" píxeles hacia '
 + 'la dirección que ve la tortuga. Si "x" no es número entero o no se '
 + 'agregan los paréntesis, avanza 5 píxeles', 'move_foward(x)');
modalDiv.innerHTML += format.format('Gira la tortuga "x" grados hacia la '
 + 'derecha. Si "x" no es número entero o no se agregan los paréntesis, gira '
 + '90 grados', 'rigth(x)');
modalDiv.innerHTML += format.format('Gira la tortuga "x" grados hacia la '
 + 'izquierda. Si "x" no es número entero o no se agregan los paréntesis, '
 + 'gira 90 grados', 'left(x)');
modalDiv.innerHTML += format.format('Guarda el estado (la posición y ángulo '
 + 'actual) de la tortuga en una pila', 'push');
modalDiv.innerHTML += format.format('Recupera el último estado (la posición y '
 + 'ángulo) guardado de la tortuga y se los asigna. Si no hay un estado '
 + 'guardado, no hace nada', 'pop');
modalDiv.innerHTML += format.format('No se hace nada, si es que se necesita '
 + 'como un control de crecimiento. Puede escribirse cualquiercosa (o no '
 + 'escribir nada), pero puede poner "none para ser consistente"', 'none');
modalDiv.innerHTML += '<p>Si se quiere que alguna variable o constante haga '
 + 'más de una acción, es posible al escribir todas las acciones que se '
 + 'deseen separadas por un espacio o salto de línea.</p>';
modalDiv.innerHTML += '<p>Si se escribe alguna acción no válida, se ignorará '
 + 'toda esta cadena hasta el próximo salto de línea on interpretará como '
 + '"none".</p>';
modalDivs.push(modalDiv);

let currentModalDiv = -1;

/**
 * @param {number} index - Un número que indica que contenido se
 *        mostrará en la ventana modal
 * @description Permite cambiar el contenido de la ventana modal y lo
 *              muestra
 */
function changeModalContent(index) {
    modal.style.display = 'block';
    currentModalDiv = index;
    modalContent.appendChild(modalDivs[currentModalDiv]);
}

/**
 * Una especie de "botón" para cerrar la ventana modal
 * @type {!HTMLSpanElement}
 */
const closeModal = document.getElementById('close-modal');
closeModal.addEventListener('click', function() {
    modal.style.display = 'none';
    modalContent.removeChild(modalDivs[currentModalDiv]);
    currentModalDiv = -1;
});

// Cerrar la ventana emergente al hacer clic fuera de ella
window.addEventListener('click', function(event) {
    if(event.target === modal) {
        modal.style.display = 'none';
        modalContent.removeChild(modalDivs[currentModalDiv]);
        currentModalDiv = -1;
    }
});
