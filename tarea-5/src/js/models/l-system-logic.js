/**
 * @class LSystem
 * @classdesc Representa un objeto con axiomas y reglas para el sistema
 *            L y cuenta con una forma de iterarlas
 */
class LSystem {
    /**
     * @typedef {Object.<string, string>} Productions
     * @property {!string} key - Cualquier clave en forma de cadena
     * @property {!string} value - Valor asociado a la clave
     *           correspondiente, también una cadena
     *
     * @description Un objeto que contiene pares clave-valor donde las
     *              claves son una cadena que identifica a la variable
     *              a sustituir y los valores una cadena que será lo
     *              que se sustituirá en el axioma
     *
     * @summary Conjunto de reglas para sustituir en el axioma
     */

    /**
     * @constructor Crea una instancia que tiene un axioma (cadena
     *              inicial) y un conjunto de reglas
     *
     * @param {!string} axiom - Una cadena que será usada como la
     *        inicial
     * @param {!Productions} rules - Un objeto que tiene una cadena
     *        como clave y una cadena como valor
     * @example
     * new LSystem('A', {'A': 'AB', 'B': 'A'})
     */
    constructor(axiom, rules) {
        /**
         * El axioma (cadena inicial) del sistema.
         * @type {!string}
         */
        this.axiom = axiom;

        /**
         * Las reglas del sistema L.
         * @type {!Productions}
         */
        this.rules = rules;

        /**
         * El resultado después de aplicar las reglas.
         * @type {!string}
         */
        this.result = axiom;
    }

    /**
     * @description Aplica las reglas en los axiomas una sola vez y
     *              actualiza el resultado
     */
    applyRules() {
        let newResult = '';
        for(let char of this.result) {
            let rule = this.rules[char];
            if(rule) newResult += rule;
            else newResult += char;
        }
        this.result = newResult;
    }

    /**
     * @param {number} [iterations=1] - La cantidad de veces que deben
     *        aplicarse las reglas. Un valor positivo si se quiere que
     *        se realicen iteraciones o 1 si es negativo o cero
     *
     * @description Aplica las reglas una cantidad de veces dada
     */
    iterate(iterations) {
        if(iterations === undefined || iterations < 1) iterations = 1;
        for(let i = 0; i < iterations; i++) this.applyRules();
    }

    /**
     * @returns {!string} - El resultado actual como una cadena
     *
     * @description Obtiene el resultado da haber aplicado las reglas
     *              para en el axioma
     */
    getResult() {
        return this.result;
    }
}
