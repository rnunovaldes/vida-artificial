/**
 * @class Turtle
 * @classdesc Representa una tortuga que puede moverse y pintar en un
 *            espacio de dos dimensiones
 */
class Turtle {
    /**
     * @typedef {Object} Memory
     * @property {!number} x - La posición x guardada
     * @property {!number} y - La posición y guardada
     * @property {!number} angle - El ángulo guardado
     *
     * @description Un objeto que representa el estado de la tortuga
     *              guardado
     */

    /**
     * @constructor Crea una instancia que tiene la ubicación de la
     *              tortuga
     *
     * @param {!CanvasRenderingContext2D} ctx -
     * @param {!number} x - Las coordenadas en el eje
     *        de las ordenadas de la tortuga
     * @param {!number} y - Las coordenadas en el eje
     *        de las absisas de la tortuga
     */
    constructor(ctx, x, y) {
        /**
         * @type {!CanvasRenderingContext2D}
         */
        this.ctx = ctx;

        /**
         * Ubicación en las ordenadas
         * @type {!number}
         */
        this.x = x;

        /**
         * Ubicación en las absisas
         * @type {!number}
         */
        this.y = y;

        /**
         * Ángulo al que mira la tortuga
         * @type {!number}
         */
        this.angle = 0;

        /**
         * Una pila para almacenar ubicaciones y direcciones si se
         * requieren
         * @type {!Memory[]}
         */
        this.memory = [];
    }

    /**
     * @param {number} [distance=100] - La distancia a recorrer
     *
     * @description Hace que la tortuga avance una distancia en la
     *              dirección que mira y pinta una línea entre el punto
     *              inicial y final
     */
    draw_forward(distance) {
        if(distance === undefined) distance = 5;
        let radians = this.angle * (Math.PI / 180);
        let newX = this.x + Math.cos(radians) * distance;
        let newY = this.y + Math.sin(radians) * distance;

        ctx.beginPath();
        ctx.moveTo(this.x, this.y);
        ctx.lineTo(newX, newY);
        ctx.stroke();

        this.x = newX;
        this.y = newY;
    }

    /**
     * @param {number} [distance=100] - La distancia a recorrer
     *
     * @description Hace que la tortuga avance una distancia en la
     *              dirección que mira
     */
    move_forward(distance) {
        if(distance === undefined) distance = 5;
        let radians = this.angle * (Math.PI / 180);

        this.x =  this.x + Math.cos(radians) * distance;
        this.y = this.y + Math.sin(radians) * distance;
    }

    /**
     * @param {number} [angle=90] - Los grados a girar
     *
     * @description Hace que la tortuga gire a la izquierda una
     *              cantidad de grados dada
     */
    left(angle) {
        if(angle === undefined) angle = 90;
        this.angle -= angle;
    }

    /**
     * @param {number} [angle=90] - Los grados a girar
     *
     * @description Hace que la tortuga gire a la derecha una
     *              cantidad de grados dada
     */
    right(angle) {
        if(angle === undefined) angle = 90;
        this.angle += angle;
    }

    /**
     * @description Guarda en la memoria el estado actual de la tortuga
     */
    push() {
        this.memory.push({ x: this.x, y: this.y, angle: this.angle });
    }

    /**
     * @description Repone a la tortuga el último estado guardado en la
     *              memoria
     */
    pop() {
        let objt = this.memory.pop();
        if(objt !== undefined) {
            this.x = objt.x;
            this.y = objt.y;
            this.angle = objt.angle;
        }
    }
}
