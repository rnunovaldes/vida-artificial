# Sistema de Lindenmayer

## Descripción
El "Sistema L" es un sistema matemático y formal utilizado en la generación de
estructuras fractales y patrones en la computación gráfica y la modelización de
la naturaleza. Se define una serie de símbolos y reglas de producción que
describen cómo evoluciona una cadena de símbolos a lo largo del tiempo (como
una GLC). Esta cadena de símbolos se interpreta gráficamente para crear
patrones complejos.

Los elementos clave de un sistema L son:

* Inicio: Es la cadena inicial de símbolos a partir de la cual se inicia la
  generación.
* Reglas de Producción: Son un conjunto de reglas que especifican cómo se
  transforman los símbolos en cada iteración. Cada símbolo se reemplaza por una
  cadena de símbolos según las reglas definidas.
* Iteración: El proceso de generación se realiza en iteraciones sucesivas. En
  cada iteración, se aplican las reglas de producción a la cadena actual para
  generar una nueva cadena.
* Interpretación Gráfica: La cadena resultante se interpreta gráficamente para
  crear patrones visuales.

## Programa
<div style="display: flex; align-items: center;">
  <a href="http://www.w3.org/html/logo/">
  <img src="https://www.w3.org/html/logo/badge/html5-badge-h-css3.png" width="133" height="64" alt="HTML5 Powered with CSS3 / Styling" title="HTML5 Powered with CSS3 / Styling">
  </a>
  <p style="margin-left: 10px;">Se trabajó con HTML5 y CSS3</p>
</div>

<div style="display: flex; align-items: center;">
  <a href="https://github.com/voodootikigod/logo.js">
  <img src="https://raw.githubusercontent.com/voodootikigod/logo.js/master/js.png" alt="The Unnoficial JavaScript Logo" width="70" />
  </a>
  <p style="margin-left: 10px;">Y se programó con JavaScript. Esta imágen tiene los siguientes <a href="https://github.com/voodootikigod/logo.js/blob/master/LICENSE" target="_blank">Términos de la Licencia</a></p>
</div>

Para la ejecución, use alguno de los scripts proporcionados o haga doble clic
en el archivo  `index.html` ubicado en el `src` de esta carpeta.

###### Notas
Si bien el trabajo no es una copia del proyecto
<a href="https://github.com/NolanDC/fractals" target="_blank">FRACTALS</a> de
<a href="http://www.nolandc.com" target="_blank">Nolan Carroll</a>, por la
vista toma parte de este.

### Ejemplo de salida
![Ejemplo 7 de la wikipedia en inglés](./out/fractal-plant.png)
