# Redes Azarosas de Kauffman

## Descripción
Las "Redes Azarosas de Kauffman", son una clase de modelos matemáticos utilizados en la teoría de redes y la biología para estudiar la dinámica de sistemas complejos, especialmente en el contexto de la genética. Se utilizan para explorar la idea de cómo emergen las propiedades y el comportamiento colectivo en sistemas biológicos y otros sistemas complejos.

El funcionamiento de esta implementación es como sigue:

* Genes y Estados: En una red de Kauffman, los componentes individuales del
  sistema se representan como "genes". Cada gen puede estar en uno de dos
  estados (1 o 0) y está interconectado con otros tres genes.
* Reglas de Actualización: En cada paso de tiempo discreto, los nodos
  actualizan su estado de acuerdo con ciertas reglas. La actualización de un
  gen depende de los estados actuales de los genes con los que se conecta.

## Programa
<div style="display: flex; align-items: center;">
  <a href="https://www.python.org/">
  <img src="https://s3.dualstack.us-east-2.amazonaws.com/pythondotorg-assets/media/community/logos/python-logo-only.png" alt="The Python Logo" width="70px" />
</a>
  <p style="margin-left: 10px;">Este trabajo se programó con Python 3.11.3</p>
</div>
Para la ejecución, use alguno de los scripts proporcionados

###### Requisitos
* Pillow==10.0.1

### Ejemplo de salida
![Red con 20 genes generada al azar con regla 88](./out/image-20-gene.png)
