@ECHO off
@chcp 65001>nul

SET "EXEC=src/main.py"
SET "PY_PRIN=src\main.py"
SET "PY_CONT1=src\mvc\controller\manager.py"
SET "PY_CONT2=src\mvc\controller\observer.py"
SET "PY_CONT3=src\mvc\controller\saver_loader.py"
SET "PY_MODE1=src\mvc\model\model.py"
SET "PY_VIEW1=src\mvc\view\automaton_ui.py"
SET "PY_VIEW2=src\mvc\view\window.py"
SET "FILES_EXIST=YES"

IF NOT EXIST %PY_PRIN% (
    SET "FILES_EXIST=NO"
) ELSE (
    SET "PY_FILES=%PY_CONT1% %PY_CONT2% %PY_CONT3% %PY_MODE1% %PY_VIEW1% %PY_VIEW2%"
    FOR %%F IN (%PY_FILES%) DO (
        IF NOT EXIST %%F (
            SET "FILES_EXIST=NO"
            EXIT /B
        )
    )
)

IF "%FILES_EXIST%"=="YES" (
    ECHO Vamos a ejecutar el compilado
    python %EXEC%
) ELSE (
    ECHO Faltan archivos necesarios para ejecutar el programa.
)

EXIT /B
