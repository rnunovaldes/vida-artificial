import threading
from typing import List, Tuple

from mvc.model.model import Model
from mvc.controller.observer import Observer, Observable

"""
Módulo que tiene una clase para servir de puente entre la vista y el
modelo del programa
"""
class Manager(Observer, Observable):
    _is_simulation_running: bool = False
    _end: int = 0

    def __init__(self, window):
        """
        Crea una ventana y una lógica para la simulación de las redes
        azarosas de Kauffman
        """
        super().__init__()  # Llama al constructor de la clase base
        self.__model: Model = Model()
        window.add_observer(self)
        self.__model.add_observer(window)

    def start_simulation(
            self, network: List[Tuple[int,bool,Tuple[int,int,int]]]
        ) -> None:
        """
        Inicia la simulación del autómata

        Args:
          network (List[Tuple[int, bool, Tuple[int, int, int]]]): Una
            representación de los genes en una lista.
        """
        if Manager._end == 0: self.__model.set_network(network)
        if not Manager._is_simulation_running:
            Manager._is_simulation_running = True
        def simulation_thread():
            while Manager._is_simulation_running and Manager._end < 100:
                self.__model.do_simulation()
                Manager._end += 1
            if Manager._end == 100:
                self.__model.stop()
                Manager._is_simulation_running = False
                Manager._end = 0
        simulation_thread = threading.Thread(target=simulation_thread)
        simulation_thread.start()

    def update(self, event: str, data=None) -> None:
        """
        Método que realiza acciones al recibir una notificación

        Args:
          event (str): Banderas que identifican al evento
            "set_rules" Indica que se deben asignar reglas para la
                        simulación
            "start_sim" Indica que se debe iniciar la simulación
            "stop_sim" Indica que se debe detener la simulación
            "reset_sim" Indica que se debe reiniciar la simulación
          data (Any): Un dato que es pasado al reallizarse la
            notificación
            "set_rules" Tiene una lista de booleanos
            "start_sim" Tiene una lista de genes
            "stop_sim" No recibe datos
            "reset_sim" No recibe datos
        """
        if event == 'set_rules':
            if Manager._end == 0: self.__model.set_rules(data)
        elif event == 'start_sim':
            self.start_simulation(data)
        elif event == 'stop_sim':
            Manager._is_simulation_running = False
        elif event == 'reset_sim':
            Manager._end = 0
            Manager._is_simulation_running = False
