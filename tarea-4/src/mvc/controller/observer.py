"""
Módulo con una interfaz para observadores y observados
"""
from typing import List


class Observer:
    """
    Una clase con el propósito de ser implementada por los observadores
    """
    def update(self, event: str, data=None) -> None:
        """
        Método que realiza acciones al recibir una notificación

        Args:
          event (str): Banderas que identifican al evento
          data (Any): Un dato que es pasado al reallizarse la
            notificación
        """
        pass

class Observable:
    """
    Una clase con el propósito de ser implementada por los observados
    """
    def __init__(self):
        """
        Inicia una lista vacía de observadores
        """
        self.observers: List[Observer] = []

    def add_observer(self, observer: Observer):
        """
        Agrega un observador a su lista

        Args:
          observer (Observer): La calse que va recibir notificaciones
        """
        self.observers.append(observer)

    def remove_observer(self, observer: Observer):
        """
        Elimina a un observador de su lista

        Args:
          observer (Observer): La clase que ya no va a recibir
            notificaciones
        """
        self.observers.remove(observer)

    def notify_observers(self, event: str, data=None) -> None:
        """
        Notifica a los observadores que ha habido un cambio

        Args:
          event (str): La forma de identificar el evento
        """
        for observer in self.observers:
            observer.update(event, data)
