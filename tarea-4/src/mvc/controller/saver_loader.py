import re

from PIL import Image

def save_table(filename, booleans):
    file = open(filename, 'w')
    for b in booleans: file.write(str(int(b)))

def save_network(filename, genes):
    line = '{:02d} = {:b} -> {:02d}, {:02d}, {:02d}\n'
    file = open(filename, 'w')
    for i in range(len(genes)):
        gen = genes[i]
        gen1, gen2, gen3 = gen[2]
        file.write(line.format(i, gen[1], gen1, gen2, gen3))

def save_as_image(filename, matrix, cell_size):
    height = len(matrix[0]) * cell_size
    width = len(matrix) * cell_size

    image = Image.new('RGB', (width, height))

    color_map = {
        0: (255, 255, 255),
        1: (0, 0, 0)
    }
    for y in range(len(matrix)):
        for x in range(len(matrix[0])):
            pixel_color = color_map.get(matrix[y][x], (255, 255, 255))
            for dy in range(cell_size):
                for dx in range(cell_size):
                    image.putpixel((y * cell_size + dx, x * cell_size + dy), pixel_color)
    image.save(filename, 'PNG')

def load_table(filename):
    with open(filename, 'r') as file:
        content = file.read()
    booleans = []
    for i in range(len(content)):
        if len(booleans) == 8: return booleans
        if content[i] == '0': booleans.append(False)
        elif content[i] == '1': booleans.append(True)
    if len(booleans) < 8:
        for i in range(8 - len(booleans)): booleans.append(False)
    return booleans

def load_network(filename):
    file = open(filename, 'r')
    lines = file.readlines()
    data = []
    patron = r'^\d+\s*=\s*\d+\s*->\s*\d+(?:\s*,\s*\d+){2}$'
    for i in range(len(lines)):
        line = lines[i]
        if re.match(patron, line): data.append(line)
    return data
