import time
from typing import List, Tuple

from mvc.controller.observer import Observable

"""
Este módulo contiene:
Métodos para solicitar ciertas acciones para empezar a calcular la
simulación de las redes azarosas de Kauffman

La lógica de las reglas para la simulación de las redes azarosas de
Kauffman y calcular las generaciones siguientes a una inicial
"""
class Model(Observable):
    """
    Clase que sirve al Manager para pedir ejecuciones de la simulación
    """

    def set_rules(self, bool_list: List[bool]):
        """
        Asigan un cojunto de booleanos como las reglas de la simulación
        """
        self.__rules: Rules = Rules(bool_list)

    def set_network(self, network: List[Tuple[int,bool,Tuple[int,int,int]]]):
        """
        Asigna un conjunto de genes con su valor booleano y a que otros
        tres genes apunta como una red para la simulación

        Args:
          network (List[Tuple[int, bool, Tuple[int, int, int]]]): Una
            representación de los genes en una lista
        """
        self.__rules.set_network(network)

    def do_simulation(self) -> None:
        """
        Obtiene la siguiente generación de valores paa los genes
        """
        self.__rules.next_gen()
        self.notify_observers('next_gen', self.__rules.get_gen())
        try: time.sleep(0.01)
        except KeyboardInterrupt: pass

    def stop(self) -> None:
        """
        Notifica a los observadores que ya no va a entregar más datos
        de la simulación
        """
        self.notify_observers('stop')

class Rules():
    """
    Clase que calcula la simulación del autómata
    """
    __matrix = []

    def __init__(self, bool_list: List[bool]):
        """
        Asigna una combinación de 8 booleanos como las reglas a usar

        Args:
          bool_list (List[bool]): Una lista de booleanos donde el
              primero  representa al 000, el segundo al 001,... y el
              último al 111
        """
        self.__bool_list: List[bool] = bool_list
        self.__matrix: List[bool] = []

    def set_network(
            self, network: List[Tuple[int,bool,Tuple[int,int,int]]]
        ) -> None:
        """
        Asigna una red booleana para calcular las generaciones de la
        simulación

        Args:
          network (List[Tuple[int, bool, Tuple[int, int, int]]]): Una
            representación de los genes en una lista. Esta lista de
            genes tiene el identificador del gen (aunque no se por el
            programa, más bien es para el usuario), el valor booleano
            actual del gen, y una tupla con los genes con los que se
            vincula
        """
        self.__network: List[Tuple[int, bool, Tuple[int, int, int]]] = network
        list: List[bool] = []
        for gene in network:
            list.append(gene[1])
        self.__matrix.append(list)

    def next_gen(self) -> None:
        """
        Calcula la siguiete generación a la actual
        """
        def triple_to_int(bit1: bool, bit2: bool, bit3: bool) -> int:
            return bit1 * 2**2 + bit2 * 2**1 + bit3 * 2**0
        list: List[bool] = []
        for i in range(len(self.__network)):
            gene1, gene2, gene3 = self.__network[i][2]
            index = triple_to_int(
                self.__matrix[0][gene1],
                self.__matrix[0][gene2],
                self.__matrix[0][gene3]
            )
            list.append(self.__bool_list[index])
        self.__matrix.append(list)

    def get_gen(self) -> List[bool]:
        """
        Obtiene la generación anterior a la última que se se generó al
        hacer next_gen. Este método debe ser usado después de next_gen
        pues convierte la generación calculada por esta a la actual

        Returns:
          List[bool]: Una lista de booleanos donde cada True es donde
            hay un gen activo y False donde no
        """
        row: List[bool] = self.__matrix[0]
        self.__matrix[0] = self.__matrix[1]
        self.__matrix.pop()
        return row
