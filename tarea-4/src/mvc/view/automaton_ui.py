import random
import re
import tkinter as tk

from typing import List, Tuple

"""
Módulo con 3 clases que representan el estado de la simulación, la red
de booleanos a tomar en cuenta y las reglas del autómata
"""
class BinaryVisualization:
    """
    Clase que tiene lo necesario para visualizar la ejecución de la
    simulación
    """
    __x: int = 0
    __y: int = 0
    square_size: float = 10

    def __init__(self, parent: tk.Frame):
        """
        Permite pasar una lista de numeros "1" y "0" que van a ser
        representados como cuadrados negros o blancos, respectivamente
        de arriba a abajo. Cada vez que se pase una nueva lista, se
        recorrerá una columna a la derecha

        Args
          parent (tk.Frame): El elemento que va a ser pintado para
            hacer la representación
        """
        self.__canvas: tk.Canvas = tk.Canvas(parent, bg='black')

        scrollbar: tk.Scrollbar = tk.Scrollbar(
            parent, orient='horizontal',command=self.__canvas.xview
        )
        scrollbar.pack(side='bottom', fill='x')
        self.__canvas.pack(side='left', fill='both', expand=True)

        self.__canvas.xview_moveto(0)
        self.__canvas.config(xscrollcommand=scrollbar.set)

    def paint(self, binary: List[bool]) -> None:
        """
        Pinta los cuadros de acuerdo a la lista que se le pase y lleva
        la contabilidad de qué columna es la siguiente que debe pintarse

        Args:
          binary (List[int]): Una lista con los valores a representar
        """
        self.__y = len(binary)
        BinaryVisualization.square_size = (
            self.__canvas.winfo_height() / len(binary)
        )
        y: int = 0

        canvas_width: float = self.__x + BinaryVisualization.square_size
        self.__canvas.config(scrollregion=(0, 0, canvas_width, 0))

        for bit in binary:
            color: str = 'red' if bit == 1 else 'white'
            self.__canvas.create_rectangle(
                self.__x, y,
                self.__x + BinaryVisualization.square_size,
                y + BinaryVisualization.square_size,
                fill=color
            )
            y += BinaryVisualization.square_size
        self.__x += BinaryVisualization.square_size

    def reset(self) -> None:
        """
        Reinicia el canvas, eliminando todos los cuadros pintados
        """
        self.__canvas.delete('all')
        self.__x = 0

    def get_values(self) -> List[List[int]]:
        """
        Obtiene una matriz de números que representa donde están los
        genes activos o no activos

        Returns:
          List[List[int]] Una matriz de enteros que solo ocupan al 1 y
            al 0 para representar dónde el gen está activo y dónde no
        """
        matrix: List[List[bool]] = []
        squares: Tuple[int,...] = self.__canvas.find_all()
        row: List[bool] = []
        for i in range(len(squares)):
            if i % self.__y == 0 and i != 0:
                matrix.append(row)
                row = []
            if self.__canvas.itemcget(squares[i], "fill") == 'red':
                row.append(1)
            else: row.append(0)
        matrix.append(row)
        return matrix

class BooleanNetworkUI:
    """
    Clase que tiene lo necesario para visualizar cómo se ven las redes
    de booleanos que se desean usar en la ejecución de la simulación
    """
    __RANDOMIZE_LABEL: str = 'Generar al azar'
    __MIN: int = 10
    __MAX: int = 100

    def __init__(self, parent: tk.Frame):
        """
        Se crea un espacio para escribir. Este debe tener un cierto
        formato para poder ser reconocido como una red de booleanos.

        Primero se escribe el número del gen seguido de un "=",
        luego el valor booleano que tendrá seguido de "->"
        y finalmente los números de los tres genes que conecta separados
        por espacios.
        Nótese que el primer número no es necesario para el programa,
        pero sirve como referencia para el usuario
        Si el siguiente gen se pondrá con un salto de línea

        Ejemplo:
          0 = 1 -> 20, 19, 3
        """
        input_frame: tk.Frame = tk.Frame(parent)
        input_frame.pack(side='top', padx=10, pady=10, fill='x')

        spinbox: tk.Spinbox = tk.Spinbox(
            input_frame, from_=self.__MIN, to=self.__MAX, increment=1, width=10
        )
        spinbox.delete(0, 'end')
        spinbox.insert(0, 50)
        spinbox.pack(side='left', padx=10)
        button: tk.Button = tk.Button(
            input_frame, text=self.__RANDOMIZE_LABEL,
            command=lambda: self.__randomize_network(spinbox.get())
        )
        button.pack()

        text_frame: tk.Frame = tk.Frame(parent)
        text_frame.pack(side='top', padx=10, fill='both', expand=True)

        self.__text_area: tk.Text = tk.Text(text_frame, wrap=tk.NONE)
        scrollbar: tk.Scrollbar = tk.Scrollbar(
            text_frame, orient='vertical', command=self.__text_area.yview
        )
        scrollbar.pack(side='right', fill='y')
        self.__text_area.pack(fill='both', expand=True)
        self.__text_area.config(yscrollcommand=scrollbar.set)

        self.__randomize_network(20)

    def __randomize_network(self, num: [str|int]) -> None:
        """
        Crea una red con valores aleatorios de un tamaño dado

        Args
          num: La cantidad de genes en la red
        """
        if isinstance(num, str):
            try: num = int(num)
            except ValueError: return
        elif not(isinstance(num, int)):
            raise TypeError(f'{num} is not int or str')
        self.__text_area.delete('1.0', 'end')
        if num < BooleanNetworkUI.__MIN: num = 10
        elif num > BooleanNetworkUI.__MAX: num = 100
        line: str = '{:02d} = {:b} -> {:02d}, {:02d}, {:02d}\n'
        for i in range(num):
            self.__text_area.insert('end', line.format(
                i, random.choice([True, False]), random.randint(0, num-1),
                random.randint(0, num-1), random.randint(0, num-1)
            ))

    def get_values(self) -> List:
        """
        Obtiene lo escrito en el area. Si alguna línea no tiene el
        formato adecuado, se salta hasta la siguiete que sí lo tenga
        (si no hay ninguna, de vuelve una lista vacía)

        Returns
          (List[int,bool,Tuple[int,int,int]) Una lista con una tripla
          con los distintos elementos de la red
        """
        str: str = self.__text_area.get('1.0', 'end-1c')
        lines: List[str] = str.splitlines()
        data: List[int,bool,Tuple[int,int,int]] = []

        def str_to_int(num: str, default: int) -> int:
            try: return int(num.strip())
            except ValueError: return default
        def str_to_bool(boolean: bool) -> bool:
            try: return False if boolean.strip() == '0' else True
            except ValueError: return False

        patron: str = r'^\d+\s*=\s*\d+\s*->\s*\d+(?:\s*,\s*\d+){2}$'
        for i in range(len(lines)):
            line: str = lines[i]
            if re.match(patron, line):
                parts: List[str] = lines[i].split('=')
                gene: int = str_to_int(parts[0], i)

                parts = parts[1].split('->')
                boolean: bool = str_to_bool(parts[0])

                parts = parts[1].strip().split(',')
                net: Tuple[int,int,int] = (
                    str_to_int(parts[0], gene),
                    str_to_int(parts[1], gene),
                    str_to_int(parts[2], gene)
                )
                data.append((gene, boolean, net))
        return data

    def set_values(self, values: str) -> None:
        """
        Asigna valores a la red de genes para sustituír los actuales

        Args:
          values (str): Una cadena que ya tiene el formato para ser
            puesta en el area de representación
        """
        self.__text_area.delete('1.0', 'end')
        for v in values: self.__text_area.insert('end', v)

class TruthTableUI:
    """
    Clase que tiene lo necesario para visualizar cómo se ven las reglas
    que se desean usar en la ejecución de la simulación
    """
    _checkboxes: List[tk.BooleanVar] = []
    __RANDOMIZE_LABEL: str = 'Escoger al azar'

    def __init__(self, parent: tk.Frame):
        """
        Hace una tabla de verdad del 0 al 7 en binario y se le puede
        asignar un valor verdadero o falso a cada número

        Args
          parent (tk.Frame): El componente que contendrá a este elemento
        """
        for i in range(8):
            label: tk.Label = tk.Label(parent, text=f'({bin(i)[2:].zfill(3)})')
            label.grid(row=i, column=0, sticky='e')

            output: tk.BooleanVar = tk.BooleanVar()
            checkbutton: tk.Checkbutton = tk.Checkbutton(
                parent, variable=output
            )
            checkbutton.grid(padx=10, pady=5, row=i, column=1, sticky='w')
            self._checkboxes.append(output)

        button: tk.Button = tk.Button(
            parent, text=self.__RANDOMIZE_LABEL,
            command=self.__randomize_values
        )
        button.grid(padx=5, row=3, column=2, sticky='e')

    def __randomize_values(self) -> None:
        """
        Sirve para cambiar los valores booleanos de los elementos de la
        tabla de verdad de forma aleatoria
        """
        for i in range(8):
            self._checkboxes[i].set(random.choice([True, False]))

    def get_values(self) -> List[bool]:
        """
        Obtiene una lista con los valores de las 8 casillas desde el 0
        hasta el 7

        Returns:
          (List[bool]) Una lista con los valores booleanos de las
            reglas representadas
        """
        return [checkbox.get() for checkbox in self._checkboxes]

    def set_values(self, booleans: List[bool]) -> None:
        """
        De una lista de booleanos, asigna los valores de las 8 casillas
        desde el 0 hasta el 7

        Args:
          booleans (List[bool]): Son los 7 valores a asignar dell
            número 0 al 7 en ese orden
        """
        for i in range(8):
            self._checkboxes[i].set(booleans[i])
