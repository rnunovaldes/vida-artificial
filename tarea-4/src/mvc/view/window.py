import tkinter as tk

from tkinter import filedialog
from tkinter import messagebox
from tkinter import ttk

from typing import Dict, Tuple

from mvc.controller.observer import Observer, Observable
from mvc.controller.manager import Manager
from mvc.controller.saver_loader import *

from mvc.view.automaton_ui import BinaryVisualization, BooleanNetworkUI, TruthTableUI

"""
Módulo con la clase que sive como vista y notifica al controlador
"""
RES: str = './res/'
OUT: str = './out/'

class Window(Observer, Observable):
    """
    Clase que crea una ventana y todos los elementos visuales para la
    simulación
    """
    __TITLES = [
        'Redes azarozas de Kauffman', #0
        'Opciones', #1
        'Ayuda' #2
    ]
    __OPTIONS = [
        'Archivo', #0
        'Guardar tabla', #1
        'Guardar red', #2
        'Cargar tabla', #3
        'Cargar red', #4
        'Guardar imagen', #5
        'Juego', #6
        'Iniciar', #7
        'Detener', #8
        'Reiniciar', #9
        'Ayuda', #10
        'Tablas', #11
        'Redes', #12
        'Salir' #13
    ]
    __LABELS = [
        'Red de Kauffman',
        'Tabla de Verdad'
    ]
    __MESSAGES = [
        'En la pestaña "Tabla de Verdad" se encuentran 8 casillas que puden \
ser clicadas y sirven para representar las reglas de las redes de Kauffman.\n\
\n  * Si se clica en "Escoger al azar" se asignan valores al azar', #0
        'En la pestaña "Red de Kauffman" se encuentra un espacio para escribir\
. Se espera que cada gen venga en un format específico como sigue:\n\n\
{#gen} = {booleano del gen} -> {#1er gen al que apunta}, {#2do gen}, \
{#3er gen}\n\n Ejemplo: 0 = 1 -> 20, 19, 3\n\n\
  * Al dar un salto de línea, se considera como otro gen.\n  * Si se clica en \
"Generar al azar" se asigna una red al azar del tamaño especificado por el \
número a su lado izquierdo' #1
    ]
    __TYPE_MAP = {
        0: {
            'title': '{} tabla de verdad',
            'filter': ('Archivos con la tabla de verdad', '*.tab'),
            'suffix': '.tab',
        },
        1: {
            'title': '{} red booleana',
            'filter': ('Archivos con la red booleana', '*.net'),
            'suffix': '.net'
        },
        2: {
            'title': '{} resultado como imagen',
            'filter': ('Archivos de imagen PNG', '*.png'),
            'suffix': '.png'
        },
        3: ('Todos los archivos', '*.*')
    }

    __is_paused: bool = True
    __clear_on_next: bool = False

    def __init__(self):
        """
        Se inicia la ventana y se ponen todos los elementos visuales
        """
        super().__init__()
        Manager(self)
        self.__window: tk.Tk = tk.Tk()

        SCREEN_WIDTH: int = self.__window.winfo_screenwidth()
        SCREEN_HEIGHT: int = self.__window.winfo_screenheight()

        WINDOW_WIDTH: int = SCREEN_WIDTH-100 if SCREEN_WIDTH > 749 else 650
        WINDOW_HEIGHT: int = SCREEN_HEIGHT-100 if SCREEN_HEIGHT > 629 else 550
        CENTER_X: int = int((SCREEN_WIDTH - WINDOW_WIDTH) / 2)

        self.__menu_bar: tk.Menu = tk.Menu(self.__window)
        self.__file_menu_1: tk.Menu = tk.Menu(self.__menu_bar, tearoff=0)
        self.__file_menu_2: tk.Menu = tk.Menu(self.__menu_bar, tearoff=0)
        self.__file_menu_3: tk.Menu = tk.Menu(self.__menu_bar, tearoff=0)

        self.__tab_control: ttk.Notebook = ttk.Notebook(
            self.__window, width=200, height=WINDOW_HEIGHT
        )
        self.__tab_network: tk.Frame = tk.Frame(self.__tab_control)
        self.__tab_table: tk.Frame = tk.Frame(self.__tab_control)

        self.__tab_binary: tk.Frame = tk.Frame(self.__window)

        self.__window.geometry(f'{WINDOW_WIDTH}x{WINDOW_HEIGHT}+{CENTER_X}+0')

        self.__window.title(self.__TITLES[0])
        #self.__window.iconbitmap(RES+'random-boolean-network.png')
        self.__window.tk.call(
            'wm', 'iconphoto', self.__window._w, tk.PhotoImage(file=RES+'random-boolean-network.png')
        )
        #self.__window.overrideredirect(True)

        # Archivo
        self.__file_menu_1.add_command(
            label=self.__OPTIONS[1], command=lambda: self.__save_file(0)
        )
        self.__file_menu_1.add_command(
            label=self.__OPTIONS[2], command=lambda: self.__save_file(1)
        )
        self.__file_menu_1.add_separator()
        self.__file_menu_1.add_command(
            label=self.__OPTIONS[3], command=lambda: self.__load_file(0)
        )
        self.__file_menu_1.add_command(
            label=self.__OPTIONS[4], command=lambda: self.__load_file(1)
        )
        self.__file_menu_1.add_separator()
        self.__file_menu_1.add_command(
            label=self.__OPTIONS[5], command=lambda: self.__save_file(2)
        )
        self.__menu_bar.add_cascade(
            label=self.__OPTIONS[0], menu=self.__file_menu_1
        )

        # Juego
        self.__file_menu_2.add_command(
            label=self.__OPTIONS[7], command=self.__start_simulation
        )
        self.__file_menu_2.add_command(
            label=self.__OPTIONS[8], state='disabled',
            command=self.__stop_simulation
        )
        self.__file_menu_2.add_separator()
        self.__file_menu_2.add_command(
            label=self.__OPTIONS[9], command=self.__restart_simulation
        )
        self.__menu_bar.add_cascade(
            label=self.__OPTIONS[6], menu=self.__file_menu_2
        )

        # Ayuda
        self.__file_menu_3.add_command(
            label=self.__OPTIONS[11], command=lambda: self.__show_message(0)
        )
        self.__file_menu_3.add_command(
            label=self.__OPTIONS[12], command=lambda: self.__show_message(1)
        )
        self.__menu_bar.add_cascade(
            label=self.__OPTIONS[10], menu=self.__file_menu_3
        )

        # Salir
        self.__menu_bar.add_command(
            label=self.__OPTIONS[13], command=self.__quit
        )

        self.__window.config(menu=self.__menu_bar)

        self.__tab_control.add(self.__tab_network, text=self.__LABELS[0])
        self.__tab_control.add(self.__tab_table, text=self.__LABELS[1])

        # Agregar contenido a las pestañas
        self.__boolean_network = BooleanNetworkUI(self.__tab_network)
        self.__truth_table = TruthTableUI(self.__tab_table)

        # Pack del Tabbed Pane
        self.__tab_control.pack(expand=0, side='left', fill='y')

        self.__tab_binary.pack(fill='both', expand=True)
        self.__binary_visualization = BinaryVisualization(self.__tab_binary)

        self.__window.mainloop()

    def __toggle_simulation_state(self) -> None:
        """
        Enciende y apaga opciones según sean necesarias o no
        """
        if not self.__is_paused:
            self.__file_menu_1.entryconfig(self.__OPTIONS[1], state='disabled')
            self.__file_menu_1.entryconfig(self.__OPTIONS[2], state='disabled')
            self.__file_menu_1.entryconfig(self.__OPTIONS[3], state='disabled')
            self.__file_menu_1.entryconfig(self.__OPTIONS[4], state='disabled')
            self.__file_menu_1.entryconfig(self.__OPTIONS[5], state='disabled')

            self.__file_menu_2.entryconfig(self.__OPTIONS[7], state='disabled')
            self.__file_menu_2.entryconfig(self.__OPTIONS[9], state='disabled')
            self.__file_menu_2.entryconfig(self.__OPTIONS[8], state='normal')
        else:
            self.__file_menu_1.entryconfig(self.__OPTIONS[1], state='normal')
            self.__file_menu_1.entryconfig(self.__OPTIONS[2], state='normal')
            self.__file_menu_1.entryconfig(self.__OPTIONS[3], state='normal')
            self.__file_menu_1.entryconfig(self.__OPTIONS[4], state='normal')
            self.__file_menu_1.entryconfig(self.__OPTIONS[5], state='normal')

            self.__file_menu_2.entryconfig(self.__OPTIONS[7], state='normal')
            self.__file_menu_2.entryconfig(self.__OPTIONS[9], state='normal')
            self.__file_menu_2.entryconfig(self.__OPTIONS[8], state='disabled')

    def __save_file(self, type: int) -> None:
        """
        Define lo necesario para guardar un archivo correspondiente a
        un tipo de extención

        Args
          type: Si es "0" se trata de los valores de la tabla de verdad
                Si es "1" se trata de la red de genes
                Si es "2" se trata de una imagen
        """
        type_getted: Dict[str,Tuple[str,str],str] = Window.__TYPE_MAP.get(type)
        filename: str = filedialog.asksaveasfilename(
            title=type_getted['title'].format('Guardar'),
            initialdir=OUT,
            filetypes=(type_getted['filter'], Window.__TYPE_MAP.get(3))
        )
        if filename:
            if not filename.endswith(type_getted['suffix']):
                filename += type_getted['suffix']
            if type == 0:
                save_table(filename, self.__truth_table.get_values())
            elif type == 1:
                save_network(filename, self.__boolean_network.get_values())
            elif type == 2:
                save_as_image(
                    filename,
                    self.__binary_visualization.get_values(),
                    int(BinaryVisualization.square_size)+1
                )

    def __load_file(self, type: int) -> None:
        """
        Define lo necesario para encontrar un archivo correspondiente a
        un tipo de extención

        Args
          type: Si es "0" se trata de los valores de la tabla de verdad
                Si es "1" se trata de la red de genes
        """
        type_getted: Dict[str,Tuple[str,str],str] = Window.__TYPE_MAP.get(type)
        filename: str = filedialog.askopenfilename(
            title=type_getted['title'].format('Cargar'),
            initialdir=OUT,
            filetypes=(type_getted['filter'], Window.__TYPE_MAP.get(3))
        )
        if filename:
            if type == 0:
                self.__truth_table.set_values(load_table(filename))
            elif type == 1:
                self.__boolean_network.set_values(load_network(filename))

    def __start_simulation(self) -> None:
        """
        Notifica a los observadores que se inició la simulación
        """
        self.__is_paused = False
        self.__toggle_simulation_state()
        if self.__clear_on_next:
            self.__binary_visualization.reset()
            self.__clear_on_next = False
        self.notify_observers('set_rules', self.__truth_table.get_values())
        self.notify_observers('start_sim', self.__boolean_network.get_values())

    def __stop_simulation(self) -> None:
        """
        Notifica a los observadores que la simulación debe ser detenida
        """
        self.__is_paused = True
        self.__toggle_simulation_state()
        self.notify_observers('stop_sim')

    def __restart_simulation(self) -> None:
        """
        Notifica a los observadores que se reinició la simulación y
        borra la vista anterior
        """
        self.__is_paused = True
        self.__toggle_simulation_state()
        self.notify_observers('reset_sim')
        self.__binary_visualization.reset()

    def __show_message(self, type: int) -> None:
        messagebox.showinfo(title=self.__TITLES[2], message=self.__MESSAGES[type])

    def __quit(self) -> None:
        """
        Termina el programa
        """
        if not self.__is_paused: self.__stop_simulation()
        self.__window.quit()

    def update(self, event: str, data=None) -> None:
        """
        Método que realiza acciones al recibir una notificación

        Args:
          event (str): Banderas que identifican al evento
            "next_gen" Indica que se obtuvo una nueva generación para
              y debe ser pintada
          data (Any): Un dato que es pasado al reallizarse la
            notificación
            "next_gen" Tiene una lista de booleanos
        """
        if event == 'next_gen':
            self.__binary_visualization.paint(data)
        elif event == 'stop':
            self.__is_paused = True
            self.__clear_on_next = True
            self.__toggle_simulation_state()
