import os
import sys
from mvc.view.window import Window

# Agrega el directorio raíz del proyecto a sys.path
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))

OUT = './out'
if not os.path.exists(OUT):
    os.makedirs(OUT)

manager = Window()
