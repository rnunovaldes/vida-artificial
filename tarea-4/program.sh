#!/bin/bash

EXEC="src/main.py"
PY_PRIN="src/main.py"
PY_CONT1="src/mvc/controller/manager.py"
PY_CONT2="src/mvc/controller/observer.py"
PY_CONT3="src/mvc/controller/saver_loader.py"
PY_MODE1="src/mvc/model/model.py"
PY_VIEW1="src/mvc/view/automaton_ui.py"
PY_VIEW2="src/mvc/view/window.py"
FILES_EXIST="YES"

if [ ! -f "$PY_PRIN" ]; then
    FILES_EXIST="NO"
else
    PY_FILES=("$PY_CONT1" "$PY_CONT2" "$PY_CONT3" "$PY_MODE1" "$PY_VIEW1" "$PY_VIEW2")
    for file in "${PY_FILES[@]}"; do
        if [ ! -f "$file" ]; then
            FILES_EXIST="NO"
            exit 1
        fi
    done
fi

if [ "$FILES_EXIST" == "YES" ]; then
    echo "Vamos a ejecutar el compilado"
    python "$EXEC"
else
    echo "Faltan archivos necesarios para ejecutar el programa."
fi

exit 0
