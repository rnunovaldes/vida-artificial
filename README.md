# Vida Artificial - Repositorio

Repositorio que tiene programas en java encargados como prácticas durante el
curso de Vida Artificial de la Facultad de Ciencias, UNAM

## Uso del los progrmas
Cada práctica encargada tienen dos scripts llamados `program`, uno es `.sh` y
el otro `.bat` para que pueda ejecutarse el programa tanto en Unix como en
Windows. Solo hay que ejecutar el script correspondiente.

## Organización de directorios
```
/
├── programa01/
│   ├── src/
│   │   ├── paquete/...
│   │   └── ...
│   ├── res/...
│   ├── out/...
│   ├── program.sh
│   ├── program.bat
│   └── README.md
├── programa0X/
│   └── ...
└── utilS/
    └── ...
```

* En "utils" están varias clases útilies para los programas que se hicieron con
java son de uso general.

* En "programa0X" está cada una de las prácticas encargadas,
  * junto a un README que explica en cierto grado de que trata la misma,
  * además están los scripts para ejecutar el programa.
  * En "src" está lo necesario para los programas 😎️.
  * En "res" hay recursos como imágenes que son usados por los programas.
  * En "out" se guardan los resultados de los programas, de así quererlo.
    Por defecto cuentan con algunas salidas previamente generadas.

### Descripción rápida de las prácticas
| # | Trabajo                                                | Lenguaje   |
| - | ------------------------------------------------------ | ---------- |
| 1 | [Autómata celular de una dimensión](tarea-1/README.md) | Java       |
| 2 | [El Juego de la vida](tarea-2/README.md)               | Java       |
| 3 | [La hormiga de Langton](tarea-3/README.md)             | Java       |
| 4 | [Redes azarosas de Kauffman](tarea-4/README.md)        | Python     |
| 5 | [Sistema L](tarea-5/README.md)                         | JavaScript |

## Autor
- Raúl N. Valdés
