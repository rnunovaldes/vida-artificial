package utils;

import java.text.Normalizer;
import java.text.Normalizer.Form;
//import java.util.regex.Pattern;

/**
 * Clase para validar cadenas bajo ciertas condiciones
 *
 * @version 1.0.1
 */
public final class StringValidator {

    /**
     * Cambia caracteres especiales por caracteres "normales"
     *
     * @param str una cadena
     * @return una cadena
     * @see <a href="https://loquemeinteresadelared.wordpress.com/2015/10/01/eliminar-acentos-y-diacriticos-de-un-string-en-java/">URL</a>
     */
    public static String clean(String str) {
        if(str == null) return null;
        String normalizedStr = Normalizer.normalize(str, Normalizer.Form.NFD);
        //normalizedStr.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
        return normalizedStr.replaceAll("[\\p{M}]", "");
    }

    /**
     * Regresa si una cadena esta formada por caracteres alfabeticos, con
     * posibilidad de aceptar numeros
     *
     * @param str Una cadena a comprobar
     * @param acceptNum Indica si acepta números
     * @return boleano que dice si se trata de una cadena con sólo letras o no
     */
    public static boolean isAlphabetic(String str, boolean acceptNum) {
        //Esta valida acentos y otros diacríticos
        if(str == null) return false;
        String newStr = str.replaceAll("\\s","");
        if(newStr.isEmpty()) return false;
        if(acceptNum) {
            for (char c : newStr.toCharArray()) {
                if(!Character.isAlphabetic(c)) return false;
            }
        } else {
            for (char c : newStr.toCharArray()) {
                if(!Character.isAlphabetic(c) && !Character.isDigit(c)) {
                    return false;
                }
            }
        }
        return true;
        // Esta no
        /*if(str == null) return false;
        String newStr = str.replaceAll("\\s","");
        if(newStr.isEmpty()) return false;
        String pattern = acceptNumbers ? "^[a-zA-Z0-9]+$" : "^[a-zA-Z]+$";
        return str.matches(pattern);*/
    }

    /**
     * Regresa si una cadena está formada por solo caracteres numéricos enteros
     *
     * @param str La cadena a comprobar
     * @param acceptNeg Si se aceptan números negativos
     * @return boleano que indica si se trata de una cadena con solo enteros
     */
    public static boolean isInteger(String str, boolean acceptNeg) {
        if(str==null) return false;
        int number = 0;
        try {
            number = Integer.parseInt(str);
        } catch (NumberFormatException e) {
            return false;
        }
        return (!acceptNeg && number < 0)? false: true;
    }

    /**
     * Regresa si una cadena está formada por solo caracteres numéricos reales
     *
     * @param str La cadana a comprobar
     * @param accept_neg Si se aceptaran números negativos
     * @return boleano que indica si se trata de una cadena con solo reales
     */
    public static boolean isDouble(String str, boolean acceptNeg) {
        if(str==null) return false;
        double number = 0;
        try {
            number = Double.parseDouble(str);
        } catch (NumberFormatException e) {
            return false;
        }
        return (!acceptNeg && number < 0)? false: true;
     }
}
