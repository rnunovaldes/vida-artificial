package utils;

/**
 * Clase que tendrá algunas herramientas para comprobar cosas sobre la
 * ejecución de los programas
 *
 * @version 1.0.0
 */
public final class Tools {
    private static long timeStart;
    private static long timeStop;

    public static void startTimer() {
        timeStart = System.currentTimeMillis();
    }

    public static void stopTimer() {
        timeStop = System.currentTimeMillis();
        String form = "Se completó en %dms";
        System.out.println(String.format(form, timeStop - timeStart));
    }

}
