package utils.fileManager;

import java.io.File;

import javax.swing.filechooser.FileFilter;

/**
 * Un filtro para encontrar los archivos con las reglas (.rul)
 */
public class BoardFilter extends FileFilter {

    private final String description = "Tableros de la simulación";

    /**
     * Determina si el archivo es aceptable con el criterio de extensión
     * "rul"
     */
    @Override
    public boolean accept(File f) {
        if(f.isDirectory()) {
            return true;
        }

        String extension = Utils.getExtension(f);
        if(extension != null) {
            if(extension.equals(Utils.bor)) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    /**
     * La descripción que mostrará de los archivos buscados
     *
     * @return Una descripción
     */
    public String getDescription() {
        return description;
    }
}
