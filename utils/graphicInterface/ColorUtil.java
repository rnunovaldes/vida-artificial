package utils.graphicInterface;

import java.awt.Color;

/**
 * Clase que tendrá métodos complementarios a java.awt.Color
 *
 * @version 1.0.0
 */
public class ColorUtil {
    public static Color getComplementaryColor(Color originalColor) {
        int red = originalColor.getRed();
        int green = originalColor.getGreen();
        int blue = originalColor.getBlue();

        int complementaryRed = 255 - red;
        int complementaryGreen = 255 - green;
        int complementaryBlue = 255 - blue;

        return new Color(complementaryRed, complementaryGreen, complementaryBlue);
    }
}
