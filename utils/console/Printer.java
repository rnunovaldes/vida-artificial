package utils.console;

/**
* Clase para imprimir cadenas con colores
* @version 1.2
*/
public final class Printer {

    /**
     * Imprime en consola, de color rojo las letras y fondo negro. Da un salto
     * de linea al terminar la cadena
     * @param str Una cadena a ser impresa
     */
    public static void printError(String str) {
        System.out.println(""+ Color.ROJO + Color.NEGRO_FONDO + str + Color.RESET);
    }

    /**
     * Imprime en consola, de color rojo las letras y fondo negro. Da un salto
     * de linea al terminar la cadena
     * @param str Una cadena a ser impresa
     */
    public static void printWarning(String str) {
        System.out.println(""+ Color.AMARILLO + Color.NEGRO_FONDO + str + Color.RESET);
    }

    /**
     * Imprime en consola, de color verde las letras y fondo negro. Da un salto
     * de linea al terminar la cadena
     * @param str Una cadena a ser impresa
     */
    public static void printOk(String str) {
        System.out.println(""+ Color.VERDE + Color.NEGRO_FONDO + str + Color.RESET);
    }

    /**
     * Imprime en consola, de color azul las letras y fondo negro. Da un salto
     * de linea al terminar la cadena
     * @param str Una cadena a ser impresa
     */
    public static void printInfo(String str) {
        System.out.println(""+ Color.CIAN + Color.NEGRO_FONDO + str + Color.RESET);
    }

    /**
     * Imprime en consola, de color magenta las letras y fondo negro. Da un
     * salto de linea al terminar la cadena
     * @param str Una cadena a ser impresa
     */
    public static void printExtra(String str) {
        System.out.println(""+ Color.MAGENTA + Color.NEGRO_FONDO + str + Color.RESET);
    }

    /**
     * Imprime "> " con color azul para ser usado como indicador que se espera
     * una entrada de carateres
     */
    public static void startConsole() {
        System.out.print(""+ Color.CIAN_NEGRITAS + Color.NEGRO_FONDO
                         + "> " + Color.RESET);
    }

    /**
     * Imprime en consola. Para ahorrar escribir SYstem.out.print(String)
     * @param str Una cadena a ser impresa
     */
    public static void print(String str) {
        System.out.print(str);
    }

    /**
     * Imprime en consola y da salto de línea.
     * Para ahorrar escribir System.out.print(String)
     * @param str Una cadena a ser impresa
     */
    public static void println(String str) {
        System.out.println(str);
    }

    public static void println() {
        System.out.println();
    }
}
