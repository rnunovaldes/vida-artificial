package utils.console;

import java.util.Scanner;

/**
 * Clase para leer cadenas o validarlas bajo ciertas condiciones
 *
 * @version 2.1
 */
public final class Reader {

    private static Scanner sc = new Scanner(System.in);

    /**
     * Lee una cadena de caracteres y, si se le indicó, borra lo escrito en la
     * consola desde donde comenzó la escritura en cuanto obtiene la cadena
     *
     * @param eraseLine Si debe borrar lo escrito en la línea o no
     * @return str que se ha sido leída
     */
    public static String readString(boolean eraseLine) {
        if(eraseLine) {
            String s = sc.nextLine();
            System.out.print(SpecialCharacter.PREV_LINE.getCode(1)
			                 + SpecialCharacter.CLEAR_FROM_CURSOR);
            return s;
        }
        return sc.nextLine();
    }
}
