package utils.console;

/**
 * Clase con caracteres especiales que pueden ser usados en la consola con ANSI
 * @version 2.0
 *
 * @see <a href="https://www.lihaoyi.com/post/BuildyourownCommandLinewithANSIescapecodes.html">Haoyi's Programming Blog</a>
 */
public enum SpecialCharacter {

    /**
     * Caracteres especiales que deben ser usados con
     * String.format(SpecialCharacter,int)
     */
    UP("\u001B[%dA"),
    DOWN("\u001B[%dB"),
    RIGHT("\u001B[%dC"),
    LEFT("\u001B[%dD"),
    NEXT_LINE("\u001B[%dE"),
    PREV_LINE("\u001B[%dF"),

    /** Carateres que pueden ser usados con solo su toString() */
    START_LINE("\r"),
    CLEAR("\u001B[2J"),
    CLEAR_FROM_CURSOR("\u001B[0J"),
    CLEAR_CURRENT_LINE("\u001B[2K"),
    SAVE_CURSOR("\u001b[s"),
    REST_CURSOR("\u001b[u"),

    /**
     * Caracteres que pueden ser usados con su toString pero se espera puedan
     * ser repetidos al concatenarlos usando repeat(int)
     */
    NEW_LINE("\n"),
    BACKSPACE("\b");

    private final String code;

    SpecialCharacter(String code) {
        this.code = code;
    }

    /**
     * De vuelve una cadena para ser usada como caracter especial que desplaza
     * una cantidad de veces indicada si el elemento lo permite
     *
     * @param n La cantidad de desplazamiento según el caracter especial
     * @return Una cadena para ser usada
     */
    public String getCode(int n) {
        if(this.ordinal() <= SpecialCharacter.PREV_LINE.ordinal()) {
            return String.format(code, n);
        }
        else if (this.ordinal() <= SpecialCharacter.REST_CURSOR.ordinal()) {
            return this.toString();
        }
        else return this.toString().repeat(n);
    }

    @Override
    public String toString() {
        return code;
    }
}
