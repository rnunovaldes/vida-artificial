package utils.console;

import utils.StringValidator;

/**
 * Clase para manejar una aplicación que se corre en consola. Tiene métodos
 * para interactuar con el usuario muy sencillos y prácticos.
 *
 * @version 1.1.2
 */
public class MenuConsole {

    protected static String INV = "¡¡¡Lo que ingresó es inválido!!!";
    protected static String ENTER = "<ENTER para continuar>";
    protected static String OPTION = "Seleccione una opción";
    protected static String RANGE = "Seleccione una opción en el rango de opciones";
    protected static String ACCEPT = "Seleccionó: ";
    protected static String YES = "Sí";
    protected static String NO = "No";

    /**
     * Método para generar una pausa en el programa y solicitar al usuario que
     * presione enter para continuar.
     */
    public void pause() {
        boolean avanzar = false;
        String validacion;
        int cont = 0;
        do {
            Printer.printInfo(ENTER);
            validacion = Reader.readString(true);
            if (validacion.isEmpty()) {
                avanzar = true;
            } else {
                cont++;
                avanzar = false;
            }
        } while(!avanzar);
        Printer.print(SpecialCharacter.PREV_LINE.getCode(cont)
                      + SpecialCharacter.CLEAR_FROM_CURSOR);
    }

    /**
     * Método para que el usuario ingrese un número (enteros). No permite que
     * el usuario ingrese decimales. Se usa para preguntar por opciones
     *
     * @param negativos Indica si se aceptan o no números negativos
     * @return El número que ingresó el usuario
     */
    public int askInteger(boolean negativos) {
        boolean loop = true, firstError = false;
        String validacion = "";
        do {
            Printer.startConsole();
            validacion = Reader.readString(true).trim();
            loop = !StringValidator.isInteger(validacion, negativos);
            if(loop && !firstError) {
                firstError = true;
                Printer.printError(INV);
            }
        } while(loop);
        if(firstError)
            Printer.print(SpecialCharacter.PREV_LINE.getCode(1)
                          + SpecialCharacter.CLEAR_FROM_CURSOR);
        return Integer.parseInt(validacion);
    }

    /**
     * Método para que el usuario ingrese un número (reales)
     *
     * @param negativos Indica si se aceptan o no números negativos
     * @return El número que ingresó el usuario
     */
    public double askDouble(boolean negativos) {
        boolean loop = true, firstError = false;
        String validacion;
        do {
            Printer.startConsole();
            validacion = Reader.readString(true).trim();
            loop = !StringValidator.isDouble(validacion, negativos);
            if(loop && !firstError) {
                firstError = true;
                Printer.printError(INV);
            }
        } while(loop);
        if(firstError)
            Printer.print(SpecialCharacter.PREV_LINE.getCode(1)
                          + SpecialCharacter.CLEAR_FROM_CURSOR);
        return Double.parseDouble(validacion);
    }

    /**
     * Método para que el usuario ingrese una palabra (o sea, solo letras)
     *
     * @param aceptaDigitos Indica si se aceptan dígitos en la cadena
     * @return La cadena que ingreso el usuario.
     */
    public String askWord(boolean aceptaDigitos) {
        boolean loop = true, firstError = false;
        String cadena;
        do {
            Printer.startConsole();
            cadena = Reader.readString(true).trim();
            loop = !StringValidator.isAlphabetic(cadena, aceptaDigitos);
            if(loop && !firstError) {
                firstError = true;
                Printer.printError(INV);
            }
        } while (loop);
        if(firstError)
            Printer.print(SpecialCharacter.PREV_LINE.getCode(1)
                          + SpecialCharacter.CLEAR_FROM_CURSOR);
        return cadena;
    }

    /**
     * Método para que el usuario ingrese una cadena
     *
     * @param clear Indica si debe de preprosesarse la cadena para que se
     *              eliminen diacrítos
     * @return La cadena que ingresó el usuario.
     */
    public String askString(boolean clear) {
        Printer.startConsole();
        String str = Reader.readString(true).trim();
        if(clear) {
            str = StringValidator.clean(str);
        }
        return str;
    }

    /**
     * Método que hace una pregunta de sí o no. Modificar las variables
     * estáticas YES y NO si se espera alguna forma distinta de sí o no
     *
     * @param question La pregunta que se hace al usuario
     * @return Regresa un boleano que indica si la respuesta es afirmativa
     */
    public boolean askYesNo(String question) {
        String yesTemp = StringValidator.clean(YES).toLowerCase();
        String noTemp = StringValidator.clean(NO).toLowerCase();
        String form = String.format("%s [%c/%c]", question,
                                    yesTemp.charAt(0), noTemp.charAt(0));
        int rightOffset = question.length() + 7;
        Printer.printWarning(form + " " + SpecialCharacter.SAVE_CURSOR);
        boolean loop = true, firstError = false, bool = false;
        String str = "e";
        do {
            str = askWord(false);
            String strTemp = StringValidator.clean(str.toLowerCase());
            if(strTemp.equals(yesTemp) || (strTemp.length() == 1
               && strTemp.charAt(0) == yesTemp.charAt(0))) {
                loop = false;
                bool = true;
            } else if(strTemp.equals(noTemp) || (strTemp.length() == 1
                      && strTemp.charAt(0) == noTemp.charAt(0))) {
                loop = false;
                bool = false;
            } else if(!firstError) {
                firstError = true;
                Printer.printError(INV);
            }
        } while (loop);
        if(firstError) {
            Printer.print(SpecialCharacter.PREV_LINE.getCode(1)
                          + SpecialCharacter.CLEAR_FROM_CURSOR);
            Printer.print(SpecialCharacter.RIGHT.getCode(rightOffset)
                      + SpecialCharacter.UP.getCode(2));
        } else {
            Printer.print(SpecialCharacter.RIGHT.getCode(rightOffset)
                      + SpecialCharacter.UP.getCode(1));
        }
        Printer.printOk(str);
        return bool;
    }

    /**
     * Método que para hacer una pregunta con solo dos respuestas
     *
     * @param pregunta Cadena que contiene lo que se va a preguntar al usuario
     * @param opcion1 Una opción posble del usuario
     * @param opcion2 La otra opción posible del usuario
     * @return <code>true</code> si se escoge la opcion1 y <code>false</code> si
     * se escoge la opcion2.
     */
    public boolean askDual(String pregunta, String opcion1, String opcion2) {
        Printer.printInfo(pregunta);
        Printer.println("1.- " + opcion1 + "\t2.- " + opcion2);
        int opcion = 0;
        boolean loop = true, firstError = false;
        Printer.printWarning(OPTION);
        while(loop) {
            opcion = askInteger(false);
            if (opcion == 1 || opcion == 2) {
                loop = false;
            } else if (!firstError){
                firstError = true;
                Printer.printError(RANGE);
            }
        }
        if(firstError) {
            Printer.print(SpecialCharacter.PREV_LINE.getCode(3)
                          + SpecialCharacter.CLEAR_FROM_CURSOR);
        } else {
            Printer.print(SpecialCharacter.PREV_LINE.getCode(2)
                          + SpecialCharacter.CLEAR_FROM_CURSOR);
        }
        if (opcion == 1) {
            Printer.printOk(ACCEPT + opcion1);
            return true;
        }
        Printer.printOk(ACCEPT + opcion2);
        return false;
    }

    /**
     * Método que regresa un entero. Este imprime en consola un menú; este menú
     * se obtiene recorriendo un arreglo dado de cadenas que contienen las
     * opciones y los índices de donde comienzan y donde terminan las opciones
     * (Es necesario que estos sean contiguas). Les asigna a cada elemento del
     * arreglo un número de opción que va desde el 1 hasta n.
     * Luego el interactua con el usuario para que seleccione una opción del
     * menú; solo acepta valores válidos del menú.
     *
     * @param options El arreglo con las opciones
     * @param start Indice de la posición inicial del arreglo, desde donde
     * empezará a recorrelo
     * @param end Indice donde se finaliza de recorrer el arreglo
     * @return La opción seleccionada por el usuario. Un entero
     */
    public int imprimirMenu(String[] array, int start, int end) {
        Printer.println();
        boolean loop = true, firstError = false;
        int opcion = 0;
        for (int i = start; i <= end; i++) {
            Printer.println((i - start + 1)+".- " + array[i]);
        }
        Printer.printWarning(OPTION);
        while(loop) {
            opcion = askInteger(false);
            if (opcion <= (end - start + 1) && opcion >= 1) {
                loop = false;
            } else if(!firstError) {
                firstError = true;
                Printer.printError(RANGE);
            }
        }
        if(firstError) {
            Printer.print(SpecialCharacter.PREV_LINE.getCode(2)
                          + SpecialCharacter.CLEAR_FROM_CURSOR);
        } else {
            Printer.print(SpecialCharacter.PREV_LINE.getCode(1)
                          + SpecialCharacter.CLEAR_FROM_CURSOR);
        }
        Printer.printOk(ACCEPT + array[opcion + start -1]);
        return opcion;
    }
}
